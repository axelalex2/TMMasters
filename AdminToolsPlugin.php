<?php

namespace TMMasters;

use ManiaControl\Admin\AuthenticationManager;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallbackManager;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Logger;
use ManiaControl\ManiaControl;
use ManiaControl\Manialinks\ManialinkPageAnswerListener;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerActions;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Plugins\Plugin;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;
use TMMasters\TMMUtils;

/**
 * AdminToolsPlugin provides a simple for administrators to have additional actions over their servers.
 *
 * @author axelalex2
 */
class AdminToolsPlugin implements CallbackListener, CommandListener, ManialinkPageAnswerListener, Plugin
{
    /**
     * Constants
     */
    const PLUGIN_ID      = 133;
    const PLUGIN_VERSION = 20.04;
    const PLUGIN_NAME    = 'AdminToolsPlugin';
    const PLUGIN_AUTHOR  = 'axelalex2';

    const SETTING_ADMINTOOLS_PERMISSION_LOGINOF    = 'Permission for //loginof';
    const SETTING_ADMINTOOLS_PREFIXADMINCHAT       = 'Prefix Admin-Chat';
    const SETTING_ADMINTOOLS_PREFIXMASTERADMINCHAT = 'Prefix MasterAdmin-Chat';
    const SETTING_ADMINTOOLS_PREFIXMODERATORCHAT   = 'Prefix Moderator-Chat';
    const SETTING_ADMINTOOLS_PREFIXSUPERADMINCHAT  = 'Prefix SuperAdmin-Chat';

    /**
     * Private properties
     */
    /** @var    ManiaControl $maniaControl */
    private $maniaControl = null;

    private $forceAuthLevel  = AuthenticationManager::AUTH_LEVEL_PLAYER;
    private $muteAuthLevel   = AuthenticationManager::AUTH_LEVEL_PLAYER;
    private $nicksAndPlayers = array();
    private $specMode        = PlayerActions::SPECTATOR_USER_SELECTABLE;

    /**
     * @see \ManiaControl\Plugins\Plugin::prepare()
     * @param ManiaControl $maniaControl
     */
    public static function prepare(ManiaControl $maniaControl)
    { }

    public static function getAuthor     () { return self::PLUGIN_AUTHOR ; }
    public static function getId         () { return self::PLUGIN_ID     ; }
    public static function getName       () { return self::PLUGIN_NAME   ; }
    public static function getVersion    () { return self::PLUGIN_VERSION; }
    public static function getDescription() { return 'Plugin offers additional Commands for Administrators'; }

    public function getPermissionLoginOf    () { return AuthenticationManager::getAuthLevel($this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_ADMINTOOLS_PERMISSION_LOGINOF)); }
    public function getPrefixAdminChat      () { return (string) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_ADMINTOOLS_PREFIXADMINCHAT      ); }
    public function getPrefixMasterAdminChat() { return (string) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_ADMINTOOLS_PREFIXMASTERADMINCHAT); }
    public function getPrefixModeratorChat  () { return (string) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_ADMINTOOLS_PREFIXMODERATORCHAT  ); }
    public function getPrefixSuperAdminChat () { return (string) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_ADMINTOOLS_PREFIXSUPERADMINCHAT ); }

    /**
     * Load the plugin
     *
     * @param \ManiaControl\ManiaControl $maniaControl
     */
    public function load(ManiaControl $maniaControl)
    {
        $this->maniaControl = $maniaControl;

        // Settings
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_ADMINTOOLS_PERMISSION_LOGINOF   , AuthenticationManager::getPermissionLevelNameArray(AuthenticationManager::AUTH_LEVEL_MODERATOR), 'Finds logins to parts of nicknames');
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_ADMINTOOLS_PREFIXADMINCHAT      , '$4dd -> Admins', 'Use //achat <text> to contact other Admins');
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_ADMINTOOLS_PREFIXMASTERADMINCHAT, '$d44 -> MasterAdmins', 'Use //machat <text> to contact other MasterAdmins');
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_ADMINTOOLS_PREFIXMODERATORCHAT  , '$4d4 -> Moderators', 'Use //modchat <text> to contact other Moderators');
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_ADMINTOOLS_PREFIXSUPERADMINCHAT , '$d4d -> SuperAdmins', 'Use //sachat <text> to contact other SuperAdmins');

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(PlayerManager::CB_PLAYERCONNECT, $this, 'handlePlayerConnectCallback');
        $this->maniaControl->getCallbackManager()->registerCallbackListener(PlayerManager::CB_PLAYERDISCONNECT, $this, 'handlePlayerDisonnectCallback');

        // Commands
        // Chat
        $this->maniaControl->getCommandManager()->registerCommandListener(array(  'achat',       'adminchat'), $this, 'onCommandAdminChat'      , true, 'Sends a message to all Admins.');
        $this->maniaControl->getCommandManager()->registerCommandListener(array( 'machat', 'masteradminchat'), $this, 'onCommandMasterAdminChat', true, 'Sends a message to all MasterAdmins.');
        $this->maniaControl->getCommandManager()->registerCommandListener(array('modchat',   'moderatorchat'), $this, 'onCommandModeratorChat'  , true, 'Sends a message to all Moderators.');
        $this->maniaControl->getCommandManager()->registerCommandListener(array( 'sachat',  'superadminchat'), $this, 'onCommandSuperAdminChat' , true, 'Sends a message to all SuperAdmins.');

        // Force
        $this->maniaControl->getCommandManager()->registerCommandListener('forceplayall', $this, 'onCommandForcePlayAll', true, 'Forces all players below your Authentication Level into Player Mode.');
        $this->maniaControl->getCommandManager()->registerCommandListener('forcespecall', $this, 'onCommandForceSpecAll', true, 'Forces all players below your Authentication Level into Spectator Mode.');
        $this->maniaControl->getCommandManager()->registerCommandListener('freeall'     , $this, 'onCommandFreeAll'     , true, 'Frees all players below your Authentication Level.');
        $this->maniaControl->getCommandManager()->registerCommandListener('specall'     , $this, 'onCommandSpecAll'     , true, 'Sets all players below your Authentication Level into Spectator Mode.');

        // Logins
        $this->maniaControl->getCommandManager()->registerCommandListener('loginof', $this, 'onCommandLoginOf', true, 'Displays all nicknames and logins of players which match your simplified nick search.');

        // Mute
        $this->maniaControl->getCommandManager()->registerCommandListener('muteall'  , $this, 'onCommandMuteAll'  , true, 'Mutes all players below your Authentication Level.');
        $this->maniaControl->getCommandManager()->registerCommandListener('unmuteall', $this, 'onCommandUnmuteAll', true, 'Unmutes all players below your Authentication Level.');

        $players = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($players as $player)
        {
            $normalizedNick = TMMUtils::replaceSpecialCharacters($player->rawNickname);
            array_push($this->nicksAndPlayers, array($normalizedNick, $player));
        }
    }

    public function handlePlayerConnectCallback(Player $player)
    {
        // Force $player if $this->forceAuthLevel is higher
        if (!$this->maniaControl->getAuthenticationManager()->checkRight($player, $this->forceAuthLevel))
        {
            try
            {
                $this->maniaControl->getClient()->forceSpectator($player->login, $this->specMode);
            }
            catch (Exception $e)
            {
                TMMUtils::chat(
                    $this->maniaControl,
                    true,
                    ChatMode::ADMIN_ERROR,
                    $player->login.' did not get forced!',
                    $this->forceAuthLevel
                );
            }
        }

        // Mute $player if $this->muteAuthLevel is higher
        if (!$this->maniaControl->getAuthenticationManager()->checkRight($player, $this->muteAuthLevel))
        {
            try
            {
                $this->maniaControl->getClient()->ignore($player->login);
            }
            catch (Exception $e)
            {
                TMMUtils::chat(
                    $this->maniaControl,
                    true,
                    ChatMode::ADMIN_ERROR,
                    $player->login.' did not get muted!',
                    $this->muteAuthLevel
                );
            }
        }

        $normalizedNick = TMMUtils::replaceSpecialCharacters($player->rawNickname);
        array_push($this->nicksAndPlayers, array($normalizedNick, $player));
    }

    public function handlePlayerDisonnectCallback(Player $player)
    {
        foreach ($this->nicksAndPlayers as $key => $nickAndPlayer)
        {
            if ($nickAndPlayer[1] === $player)
            {
                unset($this->nicksAndPlayers[$key]);
                return;
            }
        }
    }

    public function onCommandAdminChat(array $chatCallback, Player $admin)
    {
        $this->onCommandChat(
            $chatCallback,
            $admin,
            AuthenticationManager::AUTH_LEVEL_ADMIN,
            $this->getPrefixAdminChat()
        );
    }

    public function onCommandMasterAdminChat(array $chatCallback, Player $admin)
    {
        $this->onCommandChat(
            $chatCallback,
            $admin,
            AuthenticationManager::AUTH_LEVEL_MASTERADMIN,
            $this->getPrefixMasterAdminChat()
        );
    }

    public function onCommandModeratorChat(array $chatCallback, Player $admin)
    {
        $this->onCommandChat(
            $chatCallback,
            $admin,
            AuthenticationManager::AUTH_LEVEL_MODERATOR,
            $this->getPrefixModeratorChat()
        );
    }

    public function onCommandSuperAdminChat(array $chatCallback, Player $admin)
    {
        $this->onCommandChat(
            $chatCallback,
            $admin,
            AuthenticationManager::AUTH_LEVEL_SUPERADMIN,
            $this->getPrefixSuperAdminChat()
        );
    }

    public function onCommandForcePlayAll(array $chatCallback, Player $admin)
    {
        $this->onCommandForceAll(
            $admin,
            PlayerActions::SETTING_PERMISSION_FORCE_PLAYER_PLAY,
            PlayerActions::SPECTATOR_PLAYER,
            'forced everyone into Player Mode!'
        );
    }

    public function onCommandForceSpecAll(array $chatCallback, Player $admin)
    {
        $this->onCommandForceAll(
            $admin,
            PlayerActions::SETTING_PERMISSION_FORCE_PLAYER_SPEC,
            PlayerActions::SPECTATOR_SPECTATOR,
            'forced everyone into Spectator Mode!'
        );
    }

    public function onCommandFreeAll(array $chatCallback, Player $admin)
    {
        $this->onCommandForceAll(
            $admin,
            PlayerActions::SETTING_PERMISSION_FORCE_PLAYER_PLAY,
            PlayerActions::SPECTATOR_USER_SELECTABLE,
            'freed everyone!'
        );
    }

    public function onCommandSpecAll(array $chatCallback, Player $admin)
    {
        $this->onCommandForceAll(
            $admin,
            PlayerActions::SETTING_PERMISSION_FORCE_PLAYER_SPEC,
            PlayerActions::SPECTATOR_BUT_KEEP_SELECTABLE,
            'set everyone into Spectator Mode!'
        );
    }

    public function onCommandLoginOf(array $chatCallback, Player $admin)
    {
        if (!$this->maniaControl->getAuthenticationManager()->checkRight($admin, $this->getPermissionLoginOf()))
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($admin);
            return false;
        }
        
        $commandParts = explode(' ', $chatCallback[1][2]);
        $command = array_shift($commandParts);
        assert($command === '//loginof');

        foreach ($commandParts as $origSearchTerm)
        {
            // uppercase input is the only change we allow
            $origSearchTerm = strtolower($origSearchTerm);
            $searchTerm = TMMUtils::replaceSpecialCharacters($origSearchTerm);
            if ($searchTerm != $origSearchTerm)
            {
			    $this->maniaControl->getChat()->sendUsageInfo(
                    "'{$origSearchTerm}' not allowed, you can only search for simplified nick names (ASCII)!",
                    $admin
                );
                continue;
            }

            $playersFound = array();
            foreach ($this->nicksAndPlayers as $nickAndPlayer)
            {
                // both completely unreadable
                if ($searchTerm === '' && $nickAndPlayer[0] === '')
                    array_push($playersFound, TMMUtils::chatFormatPlayer($nickAndPlayer[1]));
                elseif ($searchTerm !== '' && strpos($nickAndPlayer[0], $searchTerm) !== false)
                    array_push($playersFound, TMMUtils::chatFormatPlayer($nickAndPlayer[1]));
            }

            if (empty($playersFound))
            {
                TMMUtils::chat(
                    $this->maniaControl,
                    true,
                    ChatMode::ERROR,
                    TMMUtils::formatMessage(
                        "No players found for '%s'!",
                        $origSearchTerm
                    ),
                    $admin
                );
            }
            else
            {
                TMMUtils::chat(
                    $this->maniaControl,
                    true,
                    ChatMode::INFORMATION,
                    TMMUtils::formatMessage(
                        "Players found for '%s': %s",
                        $origSearchTerm,
                        implode(', ', $playersFound)
                    ),
                    $admin
                );
            }
        }
    }

    public function onCommandMuteAll(array $chatCallback, Player $admin)
    {
        if (!$this->maniaControl->getAuthenticationManager()->checkPermission($admin, PlayerActions::SETTING_PERMISSION_MUTE_PLAYER))
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($admin);
            return false;
        }

        if (!$this->maniaControl->getAuthenticationManager()->checkRight($admin, $this->muteAuthLevel))
        {
            TMMUtils::chat(
                $this->maniaControl,
                true,
                ChatMode::ERROR,
                'Cannot mute all players, currently muted by higher privileged player!',
                $admin->login
            );

            return false;
        }

        $this->muteAuthLevel = $admin->authLevel;

        $targets = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($targets as $target)
        {
            // Mute $target if $admin is higher privileged
            if (!$this->maniaControl->getAuthenticationManager()->checkRight($target, $admin->authLevel))
            {
                try
                {
                    $this->maniaControl->getClient()->ignore($target->login);
                }
                catch (Exception $e)
                {
                    TMMUtils::chat(
                        $this->maniaControl,
                        true,
                        ChatMode::ADMIN_ERROR,
                        $target->login.' did not get muted!',
                        $this->muteAuthLevel
                    );
                }
            }
        }

        TMMUtils::chat(
            $this->maniaControl,
            true,
            ChatMode::INFORMATION,
            $admin->getEscapedNickname().' muted everyone!'
        );
    }

    public function onCommandUnmuteAll(array $chatCallback, Player $admin)
    {
        if (!$this->maniaControl->getAuthenticationManager()->checkPermission($admin, PlayerActions::SETTING_PERMISSION_MUTE_PLAYER))
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($admin);
            return false;
        }

        if (!$this->maniaControl->getAuthenticationManager()->checkRight($admin, $this->muteAuthLevel))
        {
            TMMUtils::chat(
                $this->maniaControl,
                true,
                ChatMode::ERROR,
                'Cannot unmute all players, currently muted by higher privileged player!',
                $admin->login
            );

            return false;
        }

        $this->muteAuthLevel = AuthenticationManager::AUTH_LEVEL_PLAYER;

        $targets = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($targets as $target)
        {
            // Unmute $target if $admin is higher privileged
            if (!$this->maniaControl->getAuthenticationManager()->checkRight($target, $admin->authLevel))
            {
                try
                {
                    $this->maniaControl->getClient()->unIgnore($target->login);
                }
                catch (Exception $e)
                {
                    TMMUtils::chat(
                        $this->maniaControl,
                        true,
                        ChatMode::ADMIN_ERROR,
                        $target->login.' did not get unmuted!',
                        $this->muteAuthLevel
                    );
                }
            }
        }

        TMMUtils::chat(
            $this->maniaControl,
            true,
            ChatMode::INFORMATION,
            $admin->getEscapedNickname().' unmuted everyone!'
        );
    }

    private function onCommandChat(array $chatCallback, Player $admin, $authLevel, $prefix)
    {
        if (!$this->maniaControl->getAuthenticationManager()->checkRight($admin, $authLevel))
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($admin);
            return false;
        }

        $message = (explode(' ', $chatCallback[1][2], 2))[1];
        TMMUtils::chat(
            $this->maniaControl,
            true,
            ChatMode::ADMIN_MESSAGE,
            $admin->getEscapedNickname().$prefix.': '.$message,
            $authLevel
        );
    }

    private function onCommandForceAll(Player $admin, $permission, $specMode, string $message)
    {
        if (!$this->maniaControl->getAuthenticationManager()->checkPermission($admin, $permission))
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($admin);
            return false;
        }

        if (!$this->maniaControl->getAuthenticationManager()->checkRight($admin, $this->forceAuthLevel))
        {
            TMMUtils::chat(
                $this->maniaControl,
                true,
                ChatMode::ERROR,
                'Cannot force mode of all players, currently forced by higher privileged player!',
                $admin->login
            );
            return false;
        }

        $this->specMode = $specMode;
        if ($this->specMode == PlayerActions::SPECTATOR_USER_SELECTABLE ||
            $this->specMode == PlayerActions::SPECTATOR_BUT_KEEP_SELECTABLE)
            $this->forceAuthLevel = AuthenticationManager::AUTH_LEVEL_PLAYER;
        else
            $this->forceAuthLevel = $admin->authLevel;

        $targets = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($targets as $target)
        {
            // Force $target if $admin is higher privileged
            if (!$this->maniaControl->getAuthenticationManager()->checkRight($target, $admin->authLevel))
            {
                try
                {
                    $this->maniaControl->getClient()->forceSpectator($target->login, $this->specMode);
                }
                catch (Exception $e)
                {
                    TMMUtils::chat(
                        $this->maniaControl,
                        true,
                        ChatMode::ADMIN_ERROR,
                        $target->login.' did not get forced!',
                        $this->forceAuthLevel
                    );
                }
            }
        }

        TMMUtils::chat(
            $this->maniaControl,
            true,
            ChatMode::INFORMATION,
            $admin->getEscapedNickname().' '.$message
        );
    }

    /**
     * Unload the plugin and its resources
     * @see \ManiaControl\Plugins\Plugin::unload()
     */
    public function unload()
    {
        $this->maniaControl = null;
    }
}
