<?php

namespace TMMasters;

final class ChatMode
{
    const ADMIN_SUCCESS     = 0;
    const ADMIN_INFORMATION = 1;
    const ADMIN_MESSAGE     = 2;
    const ADMIN_ERROR       = 3;
    const ADMIN_EXCEPTION   = 4;
    const SUCCESS           = 5;
    const INFORMATION       = 6;
    const ERROR             = 7;
    const EXCEPTION         = 8;
}
