<?php

namespace TMMasters;

use FML\Controls\Frame;
use FML\Controls\Labels\Label_Text;
use FML\Controls\Quad;
use FML\ManiaLink;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\Callbacks;
use ManiaControl\Callbacks\TimerListener;
use ManiaControl\Logger;
use ManiaControl\ManiaControl;
use ManiaControl\Manialinks\IconManager;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Plugins\Plugin;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use ManiaControl\Utils\Formatter;
use Maniaplanet\DedicatedServer\Xmlrpc\FaultException;

/**
 * ChatTimerWidgetPlugin provides a widget for the ChatTimer.
 *
 * @author axelalex2
 */
class ChatTimerWidgetPlugin implements CallbackListener, Plugin, TimerListener
{
    /*
     * Constants
     */
    const PLUGIN_ID      = 134;
    const PLUGIN_VERSION = 20.04;
    const PLUGIN_NAME    = 'ChatTimerWidgetPlugin';
    const PLUGIN_AUTHOR  = 'axelalex2';

    const MLID_CHATTIMER_WIDGET = 'ChatTimerWidgetPlugin.ChatTimerWidget';

    const SETTING_CHATTIMER_WIDGET_HEIGHT          = 'Widget Height';
    const SETTING_CHATTIMER_WIDGET_MINCHATTIME     = 'Minimum S_ChatTime to display Widget';
    const SETTING_CHATTIMER_WIDGET_POSX            = 'Widget Position X';
    const SETTING_CHATTIMER_WIDGET_POSY            = 'Widget Position Y';
    const SETTING_CHATTIMER_WIDGET_PRECHATTIMETEXT = 'Widget Pre-ChatTime Text';
    const SETTING_CHATTIMER_WIDGET_TEXTPREFIX      = 'Widget Text Prefix';
    const SETTING_CHATTIMER_WIDGET_TEXTSIZE        = 'Widget Text Size';
    const SETTING_CHATTIMER_WIDGET_WIDTH           = 'Widget Width';

    const S_CHATTIME = 'S_ChatTime';

    /*
     * Private properties
     */
    /** @var ManiaControl $maniaControl */
    private $maniaControl       = null;
    private $podiumEndTimestamp = null;

    /**
     * @see \ManiaControl\Plugins\Plugin::prepare()
     */
    public static function prepare(ManiaControl $maniaControl)
    { }

    public static function getId         () { return self::PLUGIN_ID     ; }
    public static function getName       () { return self::PLUGIN_NAME   ; }
    public static function getVersion    () { return self::PLUGIN_VERSION; }
    public static function getAuthor     () { return self::PLUGIN_AUTHOR ; }
    public static function getDescription() { return 'Plugin offers a widget for a ChatTimer'; }

    public function getHeight         () { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_CHATTIMER_WIDGET_HEIGHT         ); }
    public function getMinChatTime    () { return (int)    $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_CHATTIMER_WIDGET_MINCHATTIME    ); }
    public function getPosX           () { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_CHATTIMER_WIDGET_POSX           ); }
    public function getPosY           () { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_CHATTIMER_WIDGET_POSY           ); }
    public function getPreChatTimeText() { return (string) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_CHATTIMER_WIDGET_PRECHATTIMETEXT); }
    public function getTextPrefix     () { return (string) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_CHATTIMER_WIDGET_TEXTPREFIX     ); }
    public function getTextSize       () { return (int)    $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_CHATTIMER_WIDGET_TEXTSIZE       ); }
    public function getWidth          () { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_CHATTIMER_WIDGET_WIDTH          ); }

    /**
     * @see \ManiaControl\Plugins\Plugin::load()
     */
    public function load(ManiaControl $maniaControl)
    {
        $this->maniaControl = $maniaControl;

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(Callbacks::BEGINMAP, $this, 'handleOnBeginMap');
        $this->maniaControl->getCallbackManager()->registerCallbackListener(Callbacks::MP_PODIUMSTART, $this, 'handleOnStartPodium');

        // Settings
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_CHATTIMER_WIDGET_HEIGHT, 5.5);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_CHATTIMER_WIDGET_MINCHATTIME, 30);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_CHATTIMER_WIDGET_POSX, 160 - 25);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_CHATTIMER_WIDGET_POSY, 90 - 11);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_CHATTIMER_WIDGET_PRECHATTIMETEXT, '');
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_CHATTIMER_WIDGET_TEXTPREFIX, 'Skip in: ');
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_CHATTIMER_WIDGET_TEXTSIZE, 1);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_CHATTIMER_WIDGET_WIDTH, 30);

        $this->maniaControl->getTimerManager()->registerTimerListening($this, 'display', 1000);

        $this->active = false;

        return true;
    }

    private function getChatTime()
    {
        return $this->maniaControl->getServer()->getScriptManager()->isScriptMode()
             ? $this->maniaControl->getClient()->getModeScriptSettings()[self::S_CHATTIME]
             : $this->maniaControl->getClient()->getChatTime()['CurrentValue'] / 1000;
    }

    /**
     * Display the Widgets
     */
    public function display()
    {
        $chattime = $this->getChatTime();
        if ($chattime < $this->getMinChatTime())
        {
            $this->closeWidget();
            return;
        }

        $text = null;
        if ($this->podiumEndTimestamp === null)
        {
            // not in podium sequence
            $text = $this->getPreChatTimeText();
            if ($text === '')
            {
                $this->closeWidget();
                return;
            }
        }
        else
        {    
            $currentTimestamp = time();
            $diffTimestamp = $this->podiumEndTimestamp - $currentTimestamp;
            $text = $this->getTextPrefix() . $this->formatTime((int) $diffTimestamp);
        }
        assert(is_string($text));

        $maniaLink = new ManiaLink(self::MLID_CHATTIMER_WIDGET);

        $posX         = $this->getPosX();
        $posY         = $this->getPosY();
        $height       = $this->getHeight();
        $width        = $this->getWidth();
        $quadStyle    = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadStyle();
        $quadSubstyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadSubstyle();

        // mainframe
        $frame = new Frame();
        $maniaLink->addChild($frame);
        $frame->setPosition($posX, $posY);
        $frame->setSize($width, $height);

        // Background Quad
        $backgroundQuad = new Quad();
        $frame->addChild($backgroundQuad);
        $backgroundQuad->setSize($width, $height);
        $backgroundQuad->setStyles($quadStyle, $quadSubstyle);

        $label = new Label_Text();
        $frame->addChild($label);
        $label->setPosition(0, 0);
        $label->setText($text);
        $label->setTextColor('fff');
        $label->setTextEmboss(true);
        $label->setTextSize($this->getTextSize());

        // Send manialink
        $this->maniaControl->getManialinkManager()->sendManialink($maniaLink);
    }

    public function handleOnBeginMap()
    {
        $this->podiumEndTimestamp = null;
    }

    public function handleOnStartPodium()
    {
        $chattime = $this->getChatTime();
        $this->podiumEndTimestamp = time() + $chattime;
    }

    /**
     * @see \ManiaControl\Plugins\Plugin::unload()
     */
    public function unload()
    {
        $this->closeWidget();
    }

    /**
     * Close a Widget
     *
     * @param string $widgetId
     */
    private function closeWidget()
    {
        $this->maniaControl->getManialinkManager()->hideManialink(self::MLID_CHATTIMER_WIDGET);
    }

    private function formatTime(int $time)
    {
        if ($time < 0)
            return '0:00';

        $minutes = (int) ($time / 60);
        $seconds = (int) ($time % 60);

        return "" . $minutes . ":" . str_pad($seconds, 2, '0', STR_PAD_LEFT);
    }
}
