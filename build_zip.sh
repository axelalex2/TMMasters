#!/bin/bash
PLUGIN_FILE=$1
shift
PLUGIN_NAME=`echo ${PLUGIN_FILE} | cut -d'.' -f 1`
PWD=`pwd`
DIRECTORY=`basename ${PWD}`

echo "Getting Plugin-ID ..."
PLUGIN_ID=`grep "const PLUGIN_ID" ${PLUGIN_FILE} | sed 's/  */ /g' | sed 's/;//g' | cut -d' ' -f 5`
echo ${PLUGIN_ID}
echo ""

echo "Getting Plugin-Version ..."
PLUGIN_VERSION=`grep "const PLUGIN_VERSION" ${PLUGIN_FILE} | sed 's/  */ /g' | sed 's/;//g' | cut -d' ' -f 5`
echo ${PLUGIN_VERSION}
echo ""

echo "Building Zip-Name ..."
ZIP_NAME=${PLUGIN_ID}_${PLUGIN_NAME}_v${PLUGIN_VERSION}.zip
echo ${ZIP_NAME}
echo ""

echo "Creating Zip ..."
mkdir ${DIRECTORY}
cp ${PLUGIN_FILE} ${DIRECTORY}/
cp $@ ${DIRECTORY}/
zip -r ${ZIP_NAME} ${DIRECTORY}/
rm -rf ${DIRECTORY}/
mv -f ${ZIP_NAME} ..
echo "Done."
echo ""
