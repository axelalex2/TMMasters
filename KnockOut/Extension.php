<?php

namespace TMMasters\KnockOut;

use ManiaControl\Callbacks\CallQueueListener;
use TMMasters\KnockOutPlugin;

abstract class Extension implements CallQueueListener
{
    abstract public function __construct(KnockOutPlugin $plugin);

    // To other extensions
    public function getOptionalDependencies() { return array(); }
    
    public function getPresets() { return array(); }
    
    public function registerCallsOnKoLoad() { }
    public function registerCallsOnKoUnload() { }

    public function test() { }

    abstract public function unload();
}
