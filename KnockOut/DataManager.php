<?php

namespace TMMasters\KnockOut;

use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\Callbacks;
use ManiaControl\Callbacks\CallQueueListener;
use ManiaControl\Callbacks\Structures\TrackMania\OnScoresStructure;
use ManiaControl\Logger;
use ManiaControl\ManiaControl;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerManager;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extensions\FormatExtension;
use TMMasters\KnockOutPlugin;
use TMMasters\TMMUtils;

class KnockOutPlayerList
{
    private $playerList = array(); // $login => Player

    public function __construct(ManiaControl $maniaControl)
    {
        $this->setPlayers($maniaControl);
    }

    public function addPlayer(Player $player)
    {
        if ($this->isAlive($player))
            return false;

        $this->playerList[$player->login] = $player;
        return true;
    }

    public function detectZombies(ManiaControl $maniaControl)
    {
        // zombies are either players in the wrong mode, or players in the player list already disconnected
        $zombies = array();
        
        // no spectators
        foreach ($maniaControl->getPlayerManager()->getPlayers() as $player)
        {
            // zombie if spectator and alive, or driver and dead
            if ($player->isSpectator == $this->isAlive($player))
                $zombies[$player->login] = $player;
        }

        foreach ($this->playerList as $playerLogin => $playerInstance)
        {
            // make sure to get the latest available data
            $player = $maniaControl->getPlayerManager()->getPlayer($playerLogin);
            if (!$player->isConnected)
                $zombies[$player->login] = $player;
        }

        return $zombies;
    }

    public function getNbPlayers()
    {
        return count($this->playerList);
    }

    public function getPlayers()
    {
        return array_values($this->playerList);
    }

    public function isAlive(Player $player)
    {
        return array_key_exists($player->login, $this->playerList);
    }

    public function removePlayer(Player $player)
    {
        if (!$this->isAlive($player))
            return false;

        unset($this->playerList[$player->login]);
        return true;
    }

    public function reset()
    {
        $this->playerList = array();
    }

    public function setPlayers(ManiaControl $maniaControl)
    {
        $this->reset();
        foreach ($maniaControl->getPlayerManager()->getPlayers(true) as $player)
        {
            assert($player instanceof Player);
            if ($player->isSpectator)
                continue;
            $this->playerList[$player->login] = $player;
        }
    }
}

class KnockOutRoundScore
{
    public const DC  = PHP_INT_MAX;
    public const DNF = PHP_INT_MAX - 1;

    private $login = '';
    private $name  = '';
    private $time  = null;

    public function __construct(Player $player, int $time)
    {
        $this->login = $player->login;
        $this->name  = $player->getEscapedNickname();
        $this->time  = $time;
    }

    public function __toString()
    {
        return "{$this->login}: {$this->time}";
    }

    public function finished()
    {
        return $this->time >= 0 && !$this->isDC() && !$this->isDNF();
    }

    public function getPlayerLogin() { return $this->login; }
    public function getPlayerName () { return $this->name ; }
    public function getTime       () { return $this->time ; }

    public function isDC () { return $this->time === self::DC ; }
    public function isDNF() { return $this->time === self::DNF; }
}

class DataManager implements CallbackListener, CallQueueListener
{
    /**
     * Other constants
     */
    public const KO_LEGACY_POINTS      = 'KnockOut.DataManager.LegacyPoints';
    public const MAX_PLAYERS           = 255;
    public const SCRIPT_S_ROUNDSPERMAP = 'S_RoundsPerMap';
    public const SCRIPT_S_WARMUPNB     = 'S_WarmUpNb';

    /**
     * Private properties
     */
    private $knockOutPlugin;
    private $maniaControl;

    private $isScriptMode = null;
    private $koMatchLive = false;
    private $koRoundLive;
    private $manualSkip;
    private $nbKnockoutsRoundStart;
    private $nbMapsFinished;
    private $nbPlayersPrevRoundStart;
    private $nbPlayersRoundDC;
    private $nbPlayersRoundStart;
    private $nbRoundsMapFinished;
    private $nbRoundsRemainingMax;
    private $nbRoundsTotalFinished;
    private $nbWarmUpRoundsLeft;
    private $paused;
    private $playerList;
    private $preventKO;
    private $roundScores;

    public function getNbKnockouts($nbPlayers = null)
    {
        if ($nbPlayers === null)
            $nbPlayers = $this->getNbPlayersCurrent();

        $formatExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(FormatExtension::class);
        if ($formatExtension != null)
            return min($nbPlayers-1, $formatExtension->getNextMultiKOPlayerLimit($nbPlayers)[0]);

        return ($nbPlayers >= 2) ? 1 : 0;
    }

    public function getNbKnockoutsRoundStart()
    {
        return $this->nbKnockoutsRoundStart;
    }

    public function getNbKnockoutsToApply()
    {
        return max(0, $this->getNbKnockoutsRoundStart() - $this->getNbPlayersRoundDC());
    }

    public function getNbMapsFinished()
    {
        return $this->nbMapsFinished;
    }

    public function getNbMapsMax()
    {
        return 1 + intval(($this->getNbRoundsTotalMax()-1) / $this->getNbRoundsMapMax());
    }

    public function getNbPlayersCurrent()
    {
        return $this->playerList->getNbPlayers();
    }

    public function getNbPlayersKoPrevRound()
    {
        return $this->nbPlayersPrevRoundStart - $this->getNbPlayersCurrent();
    }

    public function getNbPlayersRoundDC()
    {
        return $this->nbPlayersRoundDC;
    }

    public function getNbPlayersRoundStart()
    {
        return $this->nbPlayersRoundStart;
    }

    public function getNbRoundScores()
    {
        return count($this->roundScores);
    }

    public function getNbRoundsMapCurrent()
    {
        return $this->nbRoundsMapFinished + $this->koRoundLive;
    }

    public function getNbRoundsMapFinished()
    {
        return $this->nbRoundsMapFinished;
    }

    public function getNbRoundsMapMax()
    {
        if ($this->isScriptMode)
            return $this->maniaControl->getClient()->getModeScriptSettings()[self::SCRIPT_S_ROUNDSPERMAP];
            
        $formatExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(FormatExtension::class);
        if ($formatExtension === null)
            return 0;
        
        $nbFinalRounds = $this->nbRoundsRemainingMax + $this->nbRoundsMapFinished;
        return min($nbFinalRounds, $formatExtension->getScriptSRoundsPerMap());
    }

    public function getNbRoundsRemainingMax()
    {
        return $this->nbRoundsRemainingMax;
    }

    public function getNbRoundsTotalCurrent()
    {
        return $this->nbRoundsTotalFinished + $this->koRoundLive;
    }

    public function getNbRoundsTotalFinished()
    {
        return $this->nbRoundsTotalFinished;
    }

    public function getNbRoundsTotalMax()
    {
        return $this->nbRoundsTotalFinished + $this->nbRoundsRemainingMax;
    }

    public function getNbWarmUpRoundCurrent()
    {
        return $this->getNbWarmUpRoundsTotal() - $this->nbWarmUpRoundsLeft;
    }

    public function getNbWarmUpRoundsLeft()
    {
        return $this->nbWarmUpRoundsLeft;
    }

    public function getNbWarmUpRoundsTotal()
    {
        return $this->isScriptMode
             ? $this->maniaControl->getClient()->getModeScriptSettings()[self::SCRIPT_S_WARMUPNB]
             : $this->maniaControl->getClient()->getAllWarmUpDuration()['CurrentValue'];
    }

    public function getPlayerList()
    {
        return $this->playerList;
    }

    public function getRoundScore(int $index)
    {
        if ($index < 0 || $index >= $this->getNbRoundScores())
            return null;
        
        return $this->roundScores[$index];
    }

    public function getRoundScores()
    {
        return $this->roundScores;
    }

    public function getWinner()
    {
        if ($this->getNbPlayersCurrent() == 1)
            return $this->playerList->getPlayers()[0];

        return null;
    }

    public function isManualSkip()
    {
        return $this->manualSkip === true;
    }

    public function isMatchLive()
    {
        return $this->koMatchLive === true;
    }

    public function isPaused()
    {
        return $this->paused;
    }

    public function isPreventKO()
    {
        return $this->preventKO;
    }

    public function isRoundLive()
    {
        return $this->koRoundLive === true;
    }

    public function isScriptMode()
    {
        return $this->isScriptMode;
    }

    public function pause()
    {
        $this->paused = true;
    }

    public function preventKO()
    {
        $this->preventKO = true;
    }

    public function resetRoundScores()
    {
        $this->roundScores = array();
    }

    public function unpause()
    {
        $this->paused = false;
    }

    /**
     * Main Functionality
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        $this->isScriptMode = $this->maniaControl->getServer()->getScriptManager()->isScriptMode();

        // only register KnockOutCallbacks, which are not handled by the KnockOutCallbackManager
        // he will update the DataManager first always

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_BEGIN,
            $this,
            'handleBeginKoCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_MANUAL_SKIP,
            $this,
            'handleManualSkipCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_KO,
            $this,
            'handlePlayerKoCallback'
        );
    }

    private function addRoundScore(Player $player, int $time)
    {
        if (!$this->koMatchLive)
            return;

        // Don't report finishes/DNFs if no round is live
        if ($time != KnockOutRoundScore::DC && !$this->koRoundLive)
            return;

        assert(!$player->isSpectator);
        if ($player->isSpectator)
            return false;

        foreach ($this->roundScores as $index => $roundScore)
        {
            if ($roundScore->getPlayerLogin() != $player->login)
                continue;
            
            // player is in round scores
            if ($this->koRoundLive && $time === KnockOutRoundScore::DC)
            {
                // player DCed after finishing -> remove him from array to update round scores
                array_splice($this->roundScores, $index, 1);
                break;
            }
            else
            {
                // no update needed
                return false;
            }
        }

        // insert new result from back, because most times later reported are slower, therefor further back
        $finished = ($time != KnockOutRoundScore::DNF && $time != KnockOutRoundScore::DC);
        $i = count($this->roundScores);
        while ($i > 0)
        {
            // if finished, only insert before, if truly faster
            // if not finished, insert before, as the player in question failed later

            $comparisonTime = $this->roundScores[$i-1]->getTime();
            if ($finished && $time < $comparisonTime)
                $i--;
            elseif ($time <= $comparisonTime)
                $i--;
            else
                break;
        }

        array_splice($this->roundScores, $i, 0, array(new KnockOutRoundScore($player, $time)));
        return true;
    }

    public function calcNbRoundsRemainingMax()
    {
        $this->nbRoundsRemainingMax = 0;

        $nbPlayers = $this->getNbPlayersCurrent();
        $formatExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(FormatExtension::class);
        if ($formatExtension === null)
        {
            $this->nbRoundsRemainingMax = $nbPlayers-1;
            return;
        }

        do
        {
            list($nbKnockouts, $playerLimit) = $formatExtension->getNextMultiKOPlayerLimit($nbPlayers);
            assert($nbPlayers >= $playerLimit);

            $nbPlayersForLowerLimit = $nbPlayers - $playerLimit;
            $nbPlayersOverLimit = $nbPlayersForLowerLimit % $nbKnockouts;
            assert($nbPlayersOverLimit >= 0);
            if ($nbPlayersOverLimit > 0)
                $nbPlayersForLowerLimit += ($nbKnockouts - $nbPlayersOverLimit);

            $this->nbRoundsRemainingMax += $nbPlayersForLowerLimit / $nbKnockouts;
            $nbPlayers -= $nbPlayersForLowerLimit;
        } while ($playerLimit > 1);
    }

    public function handleBeginKoCallback()
    {
        // calculate those later, as the FormatExtension might change their values
        $this->nbKnockoutsRoundStart = $this->getNbKnockouts();
        $this->calcNbRoundsRemainingMax();
    }

    public function handleBeginMapCallback()
    {
        $this->nbRoundsMapFinished = 0;
        $this->nbWarmUpRoundsLeft = $this->getNbWarmUpRoundsTotal();
        $this->resetRoundScores();
    }

    public function handleBeginRoundCallback()
    {
        $zombies = $this->playerList->detectZombies($this->maniaControl);
        $nbZombies = count($zombies);
        if ($nbZombies > 0)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                TMMUtils::formatMessage(
                    'Zombies (%s): %s',
                    $nbZombies,
                    implode(', ', array_keys($zombies))
                )
            );

            foreach ($zombies as $zombieLogin)
            {
                $player = $this->maniaControl->getPlayerManager()->getPlayer($zombieLogin);
                if (!$player)
                    continue;
                
                // retrigger DC-Callback for unconnected zombies, because it wasn't picked up on initially
                if (!$player->isConnected)
                    $this->maniaControl->getCallbackManager()->triggerCallback(KnockOutCallbacks::KO_PLAYER_DC, $player);
            }
        }

        $this->koRoundLive = true;
        $this->manualSkip = false;
        $this->nbPlayersRoundDC = 0;
        $this->nbPlayersRoundStart = $this->getNbPlayersCurrent();
        $this->nbKnockoutsRoundStart = $this->getNbKnockouts();
        $this->preventKO = false;
        $this->resetRoundScores();
    }

    public function handleBeginWarmUpCallback()
    { }

    public function handleBeginWarmUpRoundCallback()
    {
        $this->nbWarmUpRoundsLeft--;
    }

    public function handleEndMapCallback()
    {
        $this->nbMapsFinished++;
        $this->resetPlayerPoints();
    }

    public function handlePreEndRoundCallback()
    {
        $this->koRoundLive = false;
    }

    public function handleEndRoundCallback()
    {
        $this->nbPlayersPrevRoundStart = $this->nbPlayersRoundStart;
        $this->nbRoundsMapFinished++;
        $this->nbRoundsTotalFinished++;
        $this->calcNbRoundsRemainingMax();
    }

    public function handleEndWarmUpCallback()
    { }

    public function handleEndWarmUpRoundCallback()
    { }

    public function handleManualSkipCallback()
    {
        $this->manualSkip = true;
    }

    public function handlePlayerDisconnectCallback(Player $player)
    {
        $this->nbPlayersRoundDC++;
        $this->addRoundScore($player, KnockOutRoundScore::DC);
        $player->destroyCache($this, self::KO_LEGACY_POINTS);
    }
    
    public function handlePlayerFinishCallback(Player $player, int $time)
    {
        if ($this->addRoundScore($player, $time) === false)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                'Internal Error, check Logs.'
            );
            Logger::logError("Internal Error: WidgetRound::handlePlayerFinishCallback({$player->login}, {$time})");
        }
    }

    public function handlePlayerKoCallback(Player $player, int $position, int $nbRoundsSurvived)
    {
        $this->playerList->removePlayer($player);
        $this->addRoundScore($player, KnockOutRoundScore::DNF);
    }

    public function handlePlayerRetireCallback(Player $player)
    {
        $this->addRoundScore($player, KnockOutRoundScore::DNF);
    }

    public function loadPlayerList()
    {
        $this->playerList = new KnockOutPlayerList($this->maniaControl);
    }

    public function loadVariables()
    {
        $this->koMatchLive             = true;
        $this->koRoundLive             = false;
        $this->manualSkip              = true;
        $this->nbKnockoutsRoundStart   = $this->getNbKnockouts();
        $this->nbMapsFinished          = 0;
        $this->nbPlayersPrevRoundStart = $this->playerList->getNbPlayers();
        $this->nbPlayersRoundDC        = 0;
        $this->nbPlayersRoundStart     = $this->playerList->getNbPlayers();
        $this->nbRoundsMapFinished     = 0;
        $this->nbRoundsTotalFinished   = 0;
        $this->nbWarmUpRoundsLeft      = 0;
        $this->paused                  = false;
        $this->preventKO               = true;
        $this->roundScores             = array();
    }

    public function registerCallsOnKoLoad()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'loadPlayerList',
                'resetPlayerPoints',
                'loadVariables',
                'calcNbRoundsRemainingMax',
            )
        );
    }

    public function registerCallsOnKoUnload()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'unsetPlayerPoints',
                'unloadVariables',
            )
        );
    }

    public function resetPlayerPoints()
    {
        $players = $this->playerList->getPlayers();
        foreach ($players as $player)
        {
            $player->setCache(
                $this->knockOutPlugin,
                self::KO_LEGACY_POINTS,
                0
            );
        }
    }

    public function unload()
    {
        $this->unloadVariables();
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
    }

    public function unloadVariables()
    {
        $this->koMatchLive             = false;
        $this->koRoundLive             = null;
        $this->manualSkip              = null;
        $this->nbKnockoutsRoundStart   = null;
        $this->nbMapsFinished          = null;
        $this->nbPlayersPrevRoundStart = null;
        $this->nbPlayersRoundDC        = null;
        $this->nbPlayersRoundStart     = null;
        $this->nbRoundsMapFinished     = null;
        $this->nbRoundsTotalFinished   = null;
        $this->nbRoundsRemainingMax    = null;
        $this->nbWarmUpRoundsLeft      = null;
        $this->paused                  = null;
        if ($this->playerList instanceof KnockOutPlayerList)
            $this->playerList->reset();
        $this->playerList              = null;
        $this->preventKO               = null;
        $this->roundScores             = null;
    }

    public function unsetPlayerPoints()
    {
        $players = $this->playerList->getPlayers();
        foreach ($players as $player)
        {
            $player->destroyCache(
                $this->knockOutPlugin,
                self::KO_LEGACY_POINTS
            );
        }
    }
}
