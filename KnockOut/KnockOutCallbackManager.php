<?php

namespace TMMasters\KnockOut;

use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallbackManager;
use ManiaControl\Callbacks\Callbacks;
use ManiaControl\Callbacks\Structures\Common\BasePlayerTimeStructure;
use ManiaControl\Callbacks\Structures\TrackMania\OnWayPointEventStructure;
use ManiaControl\Callbacks\Structures\TrackMania\OnScoresStructure;
use ManiaControl\Logger;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerManager;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOutPlugin;
use TMMasters\TMMUtils;

class KnockOutCallbackManager implements CallbackListener
{
    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::BEGINMAP,
            $this,
            'handleBeginMapCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            CallbackManager::CB_MP_BEGINROUND,
            $this,
            'handleBeginRoundLegacyCallback'
        );
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::MP_STARTROUNDSTART,
            $this,
            'handleBeginRoundScriptCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::TM_WARMUPSTART,
            $this,
            'handleBeginWarmUpScriptCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::TM_WARMUPSTARTROUND,
            $this,
            'handleBeginWarmUpRoundScriptCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::ENDMAP,
            $this,
            'handleEndMapCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            CallbackManager::CB_MP_ENDROUND,
            $this,
            'handleEndRoundLegacyCallback'
        );
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::TM_SCORES,
            $this,
            'handleEndRoundScriptCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::TM_WARMUPEND,
            $this,
            'handleEndWarmUpScriptCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::TM_WARMUPENDROUND,
            $this,
            'handleEndWarmUpRoundScriptCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            PlayerManager::CB_PLAYERCONNECT,
            $this,
            'handlePlayerConnectCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            PlayerManager::CB_PLAYERDISCONNECT,
            $this,
            'handlePlayerDisconnectCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            CallbackManager::CB_TM_PLAYERFINISH,
            $this,
            'handlePlayerFinishLegacyCallback'
        );
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::TM_ONFINISHLINE,
            $this,
            'handlePlayerFinishScriptCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::TM_ONGIVEUP,
            $this,
            'handlePlayerGiveUpCallback'
        );
    }

    public function handleBeginMapCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;

        $this->knockOutPlugin->getDataManager()->handleBeginMapCallback();
        $this->triggerCallback(
            KnockOutCallbacks::KO_MAP_BEGIN,
            $this->knockOutPlugin->getDataManager()->getNbMapsFinished()+1
        );
    }

    public function handleBeginRoundLegacyCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            return;

        $nbWarmUpRoundsLeft = $this->knockOutPlugin->getDataManager()->getNbWarmUpRoundsLeft();
        if ($nbWarmUpRoundsLeft > 0)
        {
            if ($nbWarmUpRoundsLeft === $this->knockOutPlugin->getDataManager()->getNbWarmUpRoundsTotal())
            {
                $this->knockOutPlugin->getDataManager()->handleBeginWarmUpCallback();
                $this->triggerCallback(
                    KnockOutCallbacks::KO_WARMUP_BEGIN
                );
            }

            $this->knockOutPlugin->getDataManager()->handleBeginWarmUpRoundCallback();
            $this->triggerCallback(
                KnockOutCallbacks::KO_WARMUP_ROUND_BEGIN,
                $this->knockOutPlugin->getDataManager()->getNbWarmUpRoundCurrent()
            );
            return;
        }

        $this->knockOutPlugin->getDataManager()->handleBeginRoundCallback();
        $this->triggerCallback(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this->knockOutPlugin->getDataManager()->getNbRoundsTotalFinished()+1,
            $this->knockOutPlugin->getDataManager()->getNbRoundsMapFinished()+1
        );
    }

    public function handleBeginRoundScriptCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode())
            return;

        $this->knockOutPlugin->getDataManager()->handleBeginRoundCallback();
        $this->triggerCallback(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this->knockOutPlugin->getDataManager()->getNbRoundsTotalFinished()+1,
            $this->knockOutPlugin->getDataManager()->getNbRoundsMapFinished()+1
        );
    }

    public function handleBeginWarmUpScriptCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode())
            return;

        $this->knockOutPlugin->getDataManager()->handleBeginWarmUpCallback();
        $this->triggerCallback(
            KnockOutCallbacks::KO_WARMUP_BEGIN
        );
    }

    public function handleBeginWarmUpRoundScriptCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode())
            return;

        $this->knockOutPlugin->getDataManager()->handleBeginWarmUpRoundCallback();
        $this->triggerCallback(
            KnockOutCallbacks::KO_WARMUP_ROUND_BEGIN,
            $this->knockOutPlugin->getDataManager()->getNbWarmUpRoundCurrent()
        );
    }

    public function handleEndMapCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if ($this->knockOutPlugin->getDataManager()->isManualSkip())
            return;

        $this->knockOutPlugin->getDataManager()->handleEndMapCallback();
        $this->triggerCallback(
            KnockOutCallbacks::KO_MAP_END,
            $this->knockOutPlugin->getDataManager()->getNbMapsFinished()
        );
    }

    public function handleEndRoundLegacyCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            return;
        
        if ($this->knockOutPlugin->getDataManager()->isManualSkip())
            return;
        
        if ($this->knockOutPlugin->getDataManager()->isPreventKO())
            return;

        if ($this->knockOutPlugin->getDataManager()->getNbWarmUpRoundsLeft() > 0)
        {
            $this->knockOutPlugin->getDataManager()->handleEndWarmUpRoundCallback();
            $this->triggerCallback(
                KnockOutCallbacks::KO_WARMUP_ROUND_END,
                $this->knockOutPlugin->getDataManager()->getNbWarmUpRoundCurrent()
            );

            if ($this->knockOutPlugin->getDataManager()->getNbWarmUpRoundsLeft() == 0)
            {
                $this->knockOutPlugin->getDataManager()->handleEndWarmUpCallback();
                $this->triggerCallback(
                    KnockOutCallbacks::KO_WARMUP_END
                );
            }

            return;
        }

        $lastPlayerRoundPoints = 1 + (DataManager::MAX_PLAYERS - $this->knockOutPlugin->getDataManager()->getNbPlayersCurrent());
        $targetRoundPoints = $lastPlayerRoundPoints + $this->knockOutPlugin->getDataManager()->getNbKnockoutsToApply();
        $this->triggerCallback(
            KnockOutCallbacks::KO_ROUND_KO_LEGACY,
            $targetRoundPoints
        );
        
        $this->knockOutPlugin->getDataManager()->handleEndRoundCallback();
        $this->triggerCallback(
            KnockOutCallbacks::KO_ROUND_END,
            $this->knockOutPlugin->getDataManager()->getNbRoundsTotalFinished(),
            $this->knockOutPlugin->getDataManager()->getNbRoundsMapFinished()
        );
    }

    public function handleEndRoundScriptCallback(OnScoresStructure $structure)
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode())
            return;
        
        if ($this->knockOutPlugin->getDataManager()->isManualSkip())
            return;
        
        $section = $structure->getSection();
        if ($section === 'PreEndRound')
        {
            $this->knockOutPlugin->getDataManager()->handlePreEndRoundCallback();
            $this->triggerCallback(
                KnockOutCallbacks::KO_ROUND_KO_SCRIPT,
                $structure
            );
        }
        elseif ($section === 'EndRound')
        {
            $this->knockOutPlugin->getDataManager()->handleEndRoundCallback();
            $this->triggerCallback(
                KnockOutCallbacks::KO_ROUND_END,
                $this->knockOutPlugin->getDataManager()->getNbRoundsTotalFinished(),
                $this->knockOutPlugin->getDataManager()->getNbRoundsMapFinished()
            );
        }
    }

    public function handleEndWarmUpScriptCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode())
            return;
        
        $this->knockOutPlugin->getDataManager()->handleEndWarmUpCallback();
        $this->triggerCallback(
            KnockOutCallbacks::KO_WARMUP_END
        );
    }

    public function handleEndWarmUpRoundScriptCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode())
            return;

        $this->knockOutPlugin->getDataManager()->handleEndWarmUpRoundCallback();
        $this->triggerCallback(
            KnockOutCallbacks::KO_WARMUP_ROUND_END,
            $this->knockOutPlugin->getDataManager()->getNbWarmUpRoundCurrent()
        );
    }

    public function handlePlayerConnectCallback(Player $player)
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive())
            return;
        
        $this->triggerCallback(
            KnockOutCallbacks::KO_PLAYER_JOIN,
            $player
        );
    }

    public function handlePlayerDisconnectCallback(Player $player)
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive())
            return;

        if ($player->isSpectator)
            return;
        
        $this->knockOutPlugin->getDataManager()->handlePlayerDisconnectCallback($player);
        $this->triggerCallback(
            KnockOutCallbacks::KO_PLAYER_DC,
            $player
        );
    }

    /** Doesn't work, still in for completion */
    public function handlePlayerFinishLegacyCallback(array $data)
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isRoundLive())
            return;
        
        list($playerUid, $login, $time) = $data[1];
        $player = $this->maniaControl->getPlayerManager()->getPlayer($login);
        if ($player === null)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                'Internal Error, check Logs.',
                $login
            );
            Logger::logError("Internal Error: WidgetRound::handlePlayerFinishCallback({$playerUid}, {$login}, {$time})");
            return;
        }

        $this->knockOutPlugin->getDataManager()->handlePlayerFinishCallback(
            $player,
            $time
        );
        $this->triggerCallback(
            KnockOutCallbacks::KO_PLAYER_FINISH,
            $player,
            $time
        );
    }

    public function handlePlayerFinishScriptCallback(OnWayPointEventStructure $structure)
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isRoundLive())
            return;
        
        $player = $structure->getPlayer();
        $time = $structure->getRaceTime();

        $this->knockOutPlugin->getDataManager()->handlePlayerFinishCallback(
            $player,
            $time
        );
        $this->triggerCallback(
            KnockOutCallbacks::KO_PLAYER_FINISH,
            $player,
            $time
        );
    }

    public function handlePlayerGiveUpCallback(BasePlayerTimeStructure $structure)
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
          || $this->knockOutPlugin->getDataManager()->isPaused())
            return;

        if (!$this->knockOutPlugin->getDataManager()->isRoundLive())
            return;
        
        $player = $structure->getPlayer();

        $this->knockOutPlugin->getDataManager()->handlePlayerRetireCallback($player);
        $this->triggerCallback(
            KnockOutCallbacks::KO_PLAYER_RETIRE,
            $player
        );
    }

    private function triggerCallback($callback, ...$args)
    {
        $debugMessage = array_fill(0, 1 + count($args), '%s');
        $debugMessage = 'Callback: ' . implode(', ', $debugMessage);

        call_user_func_array(
            array($this->knockOutPlugin, 'debugFormatMessage'),
            array_merge(array(false, $debugMessage, $callback), $args)
        );
        call_user_func_array(
            array($this->maniaControl->getCallbackManager(), 'triggerCallback'),
            array_merge(array($callback), $args)
        );
    }

    public function unload()
    {
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
    }
}
