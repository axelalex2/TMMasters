<?php

namespace TMMasters\KnockOut;

use Maniaplanet\DedicatedServer\Xmlrpc\Exception;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\Extensions\BreakExtension;
use TMMasters\KnockOut\Extensions\ChangeLogExtension;
use TMMasters\KnockOut\Extensions\EditionExtension;
use TMMasters\KnockOut\Extensions\FormatExtension;
use TMMasters\KnockOut\Extensions\InfoExtension;
use TMMasters\KnockOut\Extensions\PlayerExtension;
use TMMasters\KnockOut\Extensions\ResultsExtension;
use TMMasters\KnockOut\Extensions\ServerExtension;
use TMMasters\KnockOut\Extensions\TeamExtension;
use TMMasters\KnockOut\Extensions\WarmUpExtension;
use TMMasters\KnockOut\Extensions\WidgetInfo;
use TMMasters\KnockOut\Extensions\WidgetRound;
use TMMasters\KnockOutPlugin;

class ExtensionManager
{
    private const EXTENSIONS = array(
        BreakExtension::class,
        ChangeLogExtension::class,
        EditionExtension::class,
        FormatExtension::class,
        InfoExtension::class,
        PlayerExtension::class,
        ResultsExtension::class,
        ServerExtension::class,
        TeamExtension::class,
        WarmUpExtension::class,
        WidgetInfo::class,
        WidgetRound::class,
    );

    private $extensions = array();

    public function __construct(KnockOutPlugin $plugin)
    {
        foreach (self::EXTENSIONS as $extension)
            $this->loadExtension($extension, $plugin);
    }

    public function getExtension(string $extensionName)
    {
        if (!array_key_exists($extensionName, $this->extensions))
            return null;

        return $this->extensions[$extensionName];
    }

    public function getExtensions()
    {
        return $this->extensions;
    }

    public function isLoaded(string $extensionName)
    {
        if (!array_key_exists($extensionName, $this->extensions))
            return false;

        return $this->extensions[$extensionName] != null;
    }

    public function loadExtension(string $extensionName, KnockOutPlugin $plugin)
    {
        $extensionInstance = new $extensionName($plugin);
        if (!($extensionInstance instanceof Extension))
            throw new Exception("Class {$extensionName} does not implement the interface KnockOutExtension!");

        $this->extensions[$extensionName] = $extensionInstance;
        return true;
    }

    public function registerCallsOnKoLoad()
    {
        foreach ($this->extensions as $extension)
            $extension->registerCallsOnKoLoad();
    }

    public function registerCallsOnKoUnload()
    {
        foreach ($this->extensions as $extension)
            $extension->registerCallsOnKoUnload();
    }

    public function unloadExtension(string $extensionName)
    {
        if (!array_key_exists($extensionName, $this->extensions))
            return false;

        assert($this->extensions[$extensionName] != null);
        $this->extensions[$extensionName]->unload();
        $this->extensions[$extensionName] = null;
        return true;
    }

    public function unloadExtensions()
    {
        foreach ($this->extensions as $extensionName => &$extensionInstance)
        {
            $extensionInstance->unload();
            $extensionInstance = null;
        }
    }
}
