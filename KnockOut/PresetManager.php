<?php

namespace TMMasters\KnockOut;

use ManiaControl\Admin\AuthenticationManager;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Players\Player;
use TMMasters\ChatMode;
use TMMasters\KnockOutPlugin;
use TMMasters\TMMUtils;

class PresetManager implements CommandListener
{
    /**
     * Settings
     */
    public const PRESET_DEFAULT   = 'default';
    public const PRESET_DEVELOP   = 'develop';
    public const PRESET_SPECIAL   = 'special';
    public const PRESET_TC        = 'tc';
    public const PRESET_THROWBACK = 'throwback';
    public const PRESET_TTC       = 'ttc';
    public const PRESET_TTC3      = 'ttc3';
    public const PRESET_TTC4      = 'ttc4';
    public const PRESET_TTC5      = 'ttc5';

    public const ALL_PRESETS = array(
        self::PRESET_DEFAULT,
        self::PRESET_DEVELOP,
        self::PRESET_SPECIAL,
        self::PRESET_TC,
        self::PRESET_THROWBACK,
        self::PRESET_TTC,
        self::PRESET_TTC3,
        self::PRESET_TTC4,
        self::PRESET_TTC5,
    );

    private const DEPRECATED_PRESETS = array(
        self::PRESET_DEFAULT => array(
            KnockOutPlugin::SETTING_ADMINS_AUTHENTICATIONLEVEL => AuthenticationManager::AUTH_NAME_SUPERADMIN,
            KnockOutPlugin::SETTING_ADMINS_DEBUG               => false,

            KnockOutPlugin::SETTING_MESSAGES_CHATPREFIX => '',
            KnockOutPlugin::SETTING_MESSAGES_FORMATDEBUGMESSAGES => '$n$555',
        ),
        self::PRESET_DEVELOP => array(
            KnockOutPlugin::SETTING_ADMINS_DEBUG => true,
        ),
        self::PRESET_TC => array(
            KnockOutPlugin::SETTING_MESSAGES_CHATPREFIX => '$<$n$o$e22TC$> » ',
        ),
        self::PRESET_TTC => array(
            KnockOutPlugin::SETTING_MESSAGES_CHATPREFIX => '$<$n$o$e22TTC$> » ',
        ),
    );

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    /**
     * Main Functionality
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        $this->maniaControl->getCommandManager()->registerCommandListener(
            'kopreset',
            $this,
            'onCommandKOPreset',
            true,
            'Loads a specified preset configuration.'
        );

        // ManiaControl has a feature to cleanup unused settings, so we need to load the default preset to prevent that
        $this->loadPreset(self::PRESET_DEFAULT, false);
    }

    private function loadPreset(string $preset, bool $onCommand)
    {
        assert(in_array($preset, self::ALL_PRESETS));

        foreach ($this->knockOutPlugin->getExtensionManager()->getExtensions() as $extension)
        {
            $extensionPresets = $extension->getPresets();
            if (!array_key_exists($preset, $extensionPresets))
                continue;

            foreach ($extensionPresets[$preset] as $setting => $value)
            {
                $this->knockOutPlugin->debugFormatMessage(
                    $onCommand,
                    'Loading setting: %s => %s',
                    $setting,
                    $value
                );
                $this->maniaControl->getSettingManager()->setSetting(
                    $this->knockOutPlugin,
                    $setting,
                    $value
                );
            }
        }

        // Deprecated old style preset
        if (array_key_exists($preset, self::DEPRECATED_PRESETS))
        {
            foreach (self::DEPRECATED_PRESETS[$preset] as $setting => $value)
            {
                $this->knockOutPlugin->debugFormatMessage(
                    $onCommand,
                    'Loading setting: %s => %s',
                    $setting,
                    $value
                );
                $this->maniaControl->getSettingManager()->setSetting(
                    $this->knockOutPlugin,
                    $setting,
                    $value
                );
            }
        }
    }

    private function loadPresetCommand(Player $player, array $params)
    {
        assert( $this->maniaControl->getAuthenticationManager()->checkRight(
            $player,
            $this->knockOutPlugin->getAdminsAuthenticationLevel()
        ) );

        if ($this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot load Preset while KO is live!',
                $player
            );
            return;
        }

        $nbParams = count($params);
        if ($nbParams > 1)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Too many parameters specified!',
                $player
            );
            return;
        }

        if ($nbParams == 0 || strlen($params[0]) == 0)
        {
            $presets = implode(', ', self::ALL_PRESETS);
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                TMMUtils::formatMessage(
                    'Available Presets: %s',
                    $presets
                ),
                $player
            );
            return;
        }

        $preset = $params[0];
        if (!in_array($preset, self::ALL_PRESETS))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                TMMUtils::formatMessage(
                    'Preset %s does not exist, cannot load!',
                    $preset
                ),
                $player
            );
            return;
        }

        $this->loadPreset($preset, true);

        $this->knockOutPlugin->chat(
            ChatMode::ADMIN_SUCCESS,
            TMMUtils::formatMessage(
                '%s loaded preset %s!', 
                $player,
                $preset
            )
        );
    }

    public function onCommandKOPreset(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkRight(
            $player,
            $this->knockOutPlugin->getAdminsAuthenticationLevel()
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }
        
        $chatCommand = explode(' ', $chatCallback[1][2]);
        $command = array_shift($chatCommand);
        $this->loadPresetCommand($player, $chatCommand);
    }

    public function unload()
    {
        $this->maniaControl->getCommandManager()->unregisterCommandListener($this);
        $this->maniaControl = null;
        
        $this->knockOutPlugin = null;
    }
}
