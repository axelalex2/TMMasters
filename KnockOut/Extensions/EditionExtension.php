<?php

namespace TMMasters\KnockOut\Extensions;

use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Files\FileUtil;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;
use TMMasters\TMMUtils;

class EditionExtension extends Extension implements CallbackListener
{
    /**
     * Custom Callbacks
     */
    public const KOCB_EDITION_UPDATE = 'KnockOut.Extension.Edition.Update';

    /**
     * Settings
     */
    private const SETTING_AUTHOR_ALLOWEDTOPLAY = 'Edition/Author/Allowed to play';
    private const SETTING_AUTHOR_NAME          = 'Edition/Author/Name';
    private const SETTING_MAPS_FOLDERORLIST    = 'Edition/Maps/Folder or List';
    private const SETTING_MAPS_SHUFFLE         = 'Edition/Maps/Shuffle';
    private const SETTING_NUMBER               = 'Edition/Number';
    private const SETTING_STYLE                = 'Edition/Style';
    private const SETTING_WILDCARD_NUMBER      = 'Edition/Wildcard/Number';
    private const SETTING_WILDCARD_STYLE       = 'Edition/Wildcard/Style';

    /**
     * Other Constants
     */
    public const SETTING_EDITION_UPDATE = array(
        self::SETTING_NUMBER,
        self::SETTING_STYLE,
        self::SETTING_WILDCARD_NUMBER,
        self::SETTING_WILDCARD_STYLE,
    );

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    private $maplist = null;

    /**
     * Setting Functions
     */
    public function getAuthorAllowedToPlay()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_AUTHOR_ALLOWEDTOPLAY
        );
    }
    public function getAuthorName()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_AUTHOR_NAME
        );
    }
    public function getMapsFolderOrList()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MAPS_FOLDERORLIST
        );
    }
    public function getMapsShuffle()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MAPS_SHUFFLE
        );
    }
    public function getNumber()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_NUMBER
        );
    }
    public function getStyle()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_STYLE
        );
    }
    public function getWildcardNumber()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_WILDCARD_NUMBER
        );
    }
    public function getWildcardStyle()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_WILDCARD_STYLE
        );
    }

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            PlayerManager::CB_PLAYERCONNECT,
            $this,
            'handlePlayerConnectCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            self::KOCB_EDITION_UPDATE,
            $this,
            'handleEditionUpdateCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            SettingManager::CB_SETTING_CHANGED,
            $this,
            'handleUpdateSettingsCallback'
        );
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                // TODO self::SETTING_AUTHOR_ALLOWEDTOPLAY => false,
                // TODO self::SETTING_AUTHOR_NAME          => '',
                self::SETTING_MAPS_FOLDERORLIST    => '',
                self::SETTING_MAPS_SHUFFLE         => false,
                self::SETTING_NUMBER               => 0,
                self::SETTING_STYLE                => '',
                self::SETTING_WILDCARD_NUMBER      => '<number>',
                self::SETTING_WILDCARD_STYLE       => '<style>',
            ),
            PresetManager::PRESET_THROWBACK => array(
                // TODO self::SETTING_AUTHOR_ALLOWEDTOPLAY => true,
                self::SETTING_MAPS_FOLDERORLIST => 'TTC_Throwback/**',
                self::SETTING_MAPS_SHUFFLE      => true,
                self::SETTING_STYLE             => 'Throwback',
            ),
            PresetManager::PRESET_TTC => array(
                self::SETTING_MAPS_FOLDERORLIST => 'TTC/<style>/<number>/*',
            ),
        );
        return $presets;
    }

    public function registerCallsOnKoLoad()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'loadMaps'
            )
        );
    }

    public function unload()
    {
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;

        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    public function announceMaps()
    {
        if ($this->maplist === null)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_INFORMATION,
                'Automatic map loader deactivated!'
            );
        }
        elseif ($this->isValidMapsList($this->maplist))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_INFORMATION,
                TMMUtils::formatMessage(
                    'Maplist %s found!',
                    $this->maplist
                )
            );
        }
        elseif (is_array($this->maplist) && !empty($this->maplist))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_INFORMATION,
                TMMUtils::formatMessage(
                    '%s map(s) found!',
                    count($this->maplist)
                )
            );
        }
        else
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                TMMUtils::formatMessage(
                    'Maps loader could not find any maps for setting %s, please check!',
                    $this->getMapsFolderOrList()
                )
            );
        }
    }
    
    public function checkForMaps()
    {
        $this->maplist = null;

        $maplist = $this->getMapsFolderOrList();
        if ($this->isValidMapsList($maplist))
            $this->maplist = $maplist;
        elseif (strlen($maplist) > 0)
            $this->maplist = $this->parseMapFolderPath($maplist, true);

        $this->announceMaps();
    }

    public function handleEditionUpdateCallback()
    {
        if (!$this->maniaControl->getCallQueueManager()->hasListening($this, 'checkForMaps'))
            $this->maniaControl->getCallQueueManager()->registerListening($this, 'checkForMaps');
    }

    public function handlePlayerConnectCallback(Player $player)
    {
        if ( $this->maniaControl->getAuthenticationManager()->checkRight(
            $player,
            $this->knockOutPlugin->getAdminsAuthenticationLevel()
        ) )
        {
            $this->checkForMaps();
        }
    }
    
    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (in_array($setting->setting, self::SETTING_EDITION_UPDATE))
        {
            // will call handleEditionUpdateCallback already
            $this->maniaControl->getCallbackManager()->triggerCallback(self::KOCB_EDITION_UPDATE);
            return;
        }
        
        if (!$setting->belongsToClass($this->knockOutPlugin))
            return;

        if ($setting->setting != self::SETTING_MAPS_FOLDERORLIST)
            return;

        $this->handleEditionUpdateCallback();
    }

    private function isChallengeFileName($filename)
    {
        $challengeFileNameEnding = '.challenge.gbx';
        $fileNameEnding = strtolower(substr($filename, -strlen($challengeFileNameEnding)));
        return ($fileNameEnding === $challengeFileNameEnding);
    }

    private function isMapFileName($filename)
    {
        $mapFileNameEnding = '.map.gbx';
        $fileNameEnding = strtolower(substr($filename, -strlen($mapFileNameEnding)));
        return ($fileNameEnding === $mapFileNameEnding);
    }

    private function isValidMapsList($maps)
    {
        if (is_string($maps) && substr($maps, -4) === '.txt')
            return is_readable($maps);
        
        return false;
    }

    private function loadMapFolder()
    {
        // unload the current Maps
        $currentMaps = $this->maniaControl->getMapManager()->getMaps();
        foreach ($currentMaps as $i => $currentMap)
        {
            if ($currentMap->fileName)
                $currentMaps[$i] = $currentMap->fileName;
            else
            {
                $this->maniaControl->getMapManager()->removeMap(
                    null,
                    $currentMap->uid,
                    false,
                    false
                );
            }
        }

        try
        {
            $this->maniaControl->getClient()->removeMapList($currentMaps);
        }
        catch (Exception $e)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
        }

        // load the new ones
        if ($this->getMapsShuffle())
            shuffle($this->maplist);

        try
        {
            $this->maniaControl->getClient()->addMapList($this->maplist);
        }
        catch (Exception $e)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
        }
    }

    private function loadMapList()
    {
        try
        {
            $this->maniaControl->getClient()->loadMatchSettings($this->maplist);
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_INFORMATION,
                TMMUtils::formatMessage(
                    'Maplist %s loaded!',
                    $this->maplist
                )
            );
        }
        catch (Exception $e)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
        }
    }

    public function loadMaps()
    {
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Loading maps ...'
        );

        if ($this->maplist === null)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_INFORMATION,
                'No maplist specified, using currently juked maps.'
            );
        }
        elseif (is_array($this->maplist))
            $this->loadMapFolder();
        elseif (is_string($this->maplist))
            $this->loadMapList();
        else
            assert(false);
    }

    private function parseMapFolderPath(string $mapfiles, bool $scan = true)
    {
        $mapfiles = $this->replaceWildcards($mapfiles);

        $recurse = false;
        if (substr($mapfiles, -1) === '*')
        {
            $mapfiles = substr($mapfiles, 0, -1);
            if (substr($mapfiles, -1) === '*')
            {
                $mapfiles = substr($mapfiles, 0, -1);
                $recurse = true;
            }

            $mapfiles = $this->maniaControl->getServer()->getDirectory()->getMapsFolder() . $mapfiles;

            if ($scan)
                $mapfiles = $this->scanMapFiles($mapfiles, $recurse);
            else
                return is_readable($mapfiles) && is_dir($mapfiles);
        }
        else
        {
            if ($this->isChallengeFileName($mapfiles) || $this->isMapFileName($mapfiles))
                $mapfiles = array($mapfiles);
            else
                $mapfiles = array();
        }

        return $mapfiles;
    }

    public function replaceWildcards(string $s)
    {
        $wildcardNumber = $this->getWildcardNumber();
        if (strlen($wildcardNumber) > 0)
            $s = str_replace($wildcardNumber, $this->getNumber(), $s);

        $wildcardStyle = $this->getWildcardStyle();
        if (strlen($wildcardStyle) > 0)
            $s = str_replace($wildcardStyle, $this->getStyle(), $s);
        
        return $s;
    }

    private function scanMapFiles($directory, $recurse)
    {
        if (!is_readable($directory) || !is_dir($directory))
            return false;

        $mapfiles = array();
        $dirFiles = scandir($directory);
        foreach ($dirFiles as $filename)
        {
            if (FileUtil::isHiddenFile($filename))
                continue;

            $fullFileName = $directory . $filename;
            if (!is_readable($fullFileName))
                continue;

            if (is_dir($fullFileName))
            {
                if ($recurse)
                {
                    $mapfiles = array_merge(
                        $mapfiles,
                        $this->scanMapFiles(
                            $fullFileName . DIRECTORY_SEPARATOR,
                            $recurse
                        )
                    );
                }
            }
            else
            {
                if ($this->isChallengeFileName($filename) || $this->isMapFileName($filename))
                    array_push($mapfiles, $fullFileName);
            }
        }
        return $mapfiles;
    }
}
