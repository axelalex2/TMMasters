<?php

namespace TMMasters\KnockOut\Extensions;

use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\TimerListener;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;
use TMMasters\TMMUtils;

class WarmUpExtension extends Extension implements CallbackListener, TimerListener
{
    /**
     * Settings
     */
    private const SETTING_MINIMUM_S_WARMUPNB      = 'WarmUp/Minimum S_WarmUpNb';
    private const SETTING_REDUCEEVERYNBMAPS       = 'WarmUp/Reduce every Nb Maps';
    private const SETTING_SCRIPT_S_WARMUPDURATION = 'WarmUp/Script/S_WarmUpDuration';
    private const SETTING_SCRIPT_S_WARMUPNB       = 'WarmUp/Script/S_WarmUpNb';

    private const SCRIPT_S_ROUNDSPERMAP = 'S_RoundsPerMap';

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    /**
     * Setting Functions
     */
    public function getMinimumSWarmUpNb()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MINIMUM_S_WARMUPNB
        );
    }
    public function getReduceEveryNbMaps()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_REDUCEEVERYNBMAPS
        );
    }
    public function getScriptSWarmUpDuration()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SCRIPT_S_WARMUPDURATION
        );
    }
    public function getScriptSWarmUpNb()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SCRIPT_S_WARMUPNB
        );
    }

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_BEGIN,
            $this,
            'handleBeginKnockOutCallback'
        );
        
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_WARMUP_ROUND_BEGIN,
            $this,
            'handleBeginWarmUpRoundCallback'
        );
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_WARMUP_ROUND_BEGIN,
            $this,
            'checkReducingWarmUps'
        );
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_REDUCEEVERYNBMAPS       => 0,
                self::SETTING_MINIMUM_S_WARMUPNB      => 1,
                self::SETTING_SCRIPT_S_WARMUPDURATION => 60,
                self::SETTING_SCRIPT_S_WARMUPNB       => 1,
            ),
            PresetManager::PRESET_DEVELOP => array(
                self::SETTING_REDUCEEVERYNBMAPS       => 1,
                self::SETTING_MINIMUM_S_WARMUPNB      => 0,
                self::SETTING_SCRIPT_S_WARMUPDURATION => 10,
                self::SETTING_SCRIPT_S_WARMUPNB       => 1,
            ),
            PresetManager::PRESET_SPECIAL => array(
                self::SETTING_REDUCEEVERYNBMAPS       => 1,
                self::SETTING_SCRIPT_S_WARMUPDURATION => 15,
                self::SETTING_SCRIPT_S_WARMUPNB       => 5,
            ),
            PresetManager::PRESET_TC => array(
                self::SETTING_REDUCEEVERYNBMAPS  => 2,
                self::SETTING_MINIMUM_S_WARMUPNB => 1,
                self::SETTING_SCRIPT_S_WARMUPNB  => 2,
            ),
            PresetManager::PRESET_TTC => array(
                self::SETTING_REDUCEEVERYNBMAPS       => 2,
                self::SETTING_MINIMUM_S_WARMUPNB      => 0,
                self::SETTING_SCRIPT_S_WARMUPDURATION => 10,
                self::SETTING_SCRIPT_S_WARMUPNB       => 5,
            ),
            PresetManager::PRESET_TTC3 => array(
                self::SETTING_REDUCEEVERYNBMAPS => 1,
                self::SETTING_SCRIPT_S_WARMUPNB => 6,
            ),
            PresetManager::PRESET_TTC4 => array(
                self::SETTING_SCRIPT_S_WARMUPDURATION => 15,
                self::SETTING_SCRIPT_S_WARMUPNB       => 4,
            ),
        );
        return $presets;
    }

    public function unload()
    {
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;
        
        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    public function checkReducingWarmUps(int $nbWarmUpRound)
    {
        // WU reducing off
        $reduce = $this->getReduceEveryNbMaps();
        if ($reduce <= 0)
            return;

        $currentWarmUpDuration = 0;
        $currentWarmUpNb = 0;

        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
        {
            $maniascriptSettings = $this->maniaControl->getClient()->getModeScriptSettings();
            $currentWarmUpDuration = $maniascriptSettings[
                $this->knockOutPlugin->getPluginSettingToScriptSetting(
                    self::SETTING_SCRIPT_S_WARMUPDURATION
                )
            ];
            $currentWarmUpNb = $maniascriptSettings[
                $this->knockOutPlugin->getPluginSettingToScriptSetting(
                    self::SETTING_SCRIPT_S_WARMUPNB
                )
            ];
        }
        else
        {
            $currentWarmUpDuration = $this->getScriptSWarmUpDuration();
            $currentWarmUpNb = $this->knockOutPlugin->getDataManager()->getNbWarmUpRoundsTotal();
        }

        if ($currentWarmUpDuration === 0 || $currentWarmUpNb === 0)
            return;

        $minWarmUpNb = $this->getMinimumSWarmUpNb();
        if ($currentWarmUpNb <= $minWarmUpNb)
            return;
        
        if ($nbWarmUpRound < $currentWarmUpNb)
            return;
        
        // in last warm up round
        $nbMapCurrent = 1 + $this->knockOutPlugin->getDataManager()->getNbMapsFinished();
        $nbMapsFinishedToReduce = $nbMapCurrent % $reduce;

        $warmupReduceLimitReached = (boolean) ($nbMapsFinishedToReduce === 0);
        $newWarmUpNb = $currentWarmUpNb-1;

        // reduce
        if ($warmupReduceLimitReached)
        {
            if ($newWarmUpNb === 0)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    'No more WarmUps on the next map!'
                );
            }
            elseif ($newWarmUpNb > 0)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    TMMUtils::formatMessage(
                        'Reducing Number of WarmUps to %s for next map!',
                        $newWarmUpNb
                    )
                );
            }

            if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            {
                $this->maniaControl->getClient()->setModeScriptSettings(
                    $this->knockOutPlugin->getPluginSettingsToScriptSettings(array(
                        self::SETTING_SCRIPT_S_WARMUPNB => $newWarmUpNb
                    ))
                );
            }
            else
            {
                $this->maniaControl->getClient()->setAllWarmUpDuration($newWarmUpNb);
            }
        }
    }

    public function handleBeginKnockOutCallback()
    {
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
        {
            $this->maniaControl->getClient()->setModeScriptSettings(
                $this->knockOutPlugin->getPluginSettingsToScriptSettings(array(
                    self::SETTING_SCRIPT_S_WARMUPDURATION => $this->getScriptSWarmUpDuration(),
                    self::SETTING_SCRIPT_S_WARMUPNB       => $this->getScriptSWarmUpNb(),
                ))
            );
        }
        else
        {
            $this->maniaControl->getClient()->setAllWarmUpDuration($this->getScriptSWarmUpNb());
        }
    }

    public function handleBeginWarmUpRoundCallback(int $nbWarmUpRound)
    {
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            return;
        
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            TMMUtils::formatMessage(
                'WarmUp %s/%s for ~%s seconds, only to prevent lags!',
                $nbWarmUpRound,
                $this->knockOutPlugin->getDataManager()->getNbWarmUpRoundsTotal(),
                $this->getScriptSWarmUpDuration()
            )
        );
        
        $this->maniaControl->getTimerManager()->registerOneTimeListening(
            $this,
            function () {
                try
                {
                    $this->maniaControl->getClient()->forceEndRound();
                }
                catch (\Exception $e)
                {
                    $this->knockOutPlugin->chat(
                        ChatMode::ADMIN_EXCEPTION,
                        $e
                    );
                    $this->knockOutPlugin->chat(
                        ChatMode::ADMIN_ERROR,
                        'Could not end WarmUp!'
                    );
                }
            },
            1000 * $this->getScriptSWarmUpDuration()
        );
    }
}
