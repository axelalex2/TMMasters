<?php

namespace TMMasters\KnockOut\Extensions;

use ManiaControl\Admin\AuthenticationManager;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerActions;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;
use TMMasters\TMMUtils;

class PlayerExtension extends Extension implements CallbackListener
{
    /**
     * Settings
     */
    private const SETTING_KICKUNTILNBPLAYERS        = 'Players/Kick until Nb Players';
    private const SETTING_MESSAGEONKNOCKOUTKICK     = 'Players/Message On KO-Kick';
    private const SETTING_NOKICKAUTHENTICATIONLEVEL = 'Players/No-Kick Authentication Level';
    private const SETTING_SPECTATORLIST             = 'Players/Spectators List';
    private const SETTING_SPECTATORLIST_ISFILE      = 'Players/Spectators List Is File';

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    private $spectatorList = array();

    public function getSpectatorList() { return $this->spectatorList; }

    /**
     * Setting Functions
     */
    public function getKickUntilNbPlayers()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_KICKUNTILNBPLAYERS
        );
    }
    public function getMessageOnKnockoutKick()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MESSAGEONKNOCKOUTKICK
        );
    }
    public function getSpectatorListIsFile()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SPECTATORLIST_ISFILE
        );
    }

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_KO,
            $this,
            'handlePlayerKoCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            SettingManager::CB_SETTING_CHANGED,
            $this,
            'handleSettingChangedCallback'
        );

        // Settings
        $this->maniaControl->getAuthenticationManager()->definePluginPermissionLevel(
            $this->knockOutPlugin,
            self::SETTING_NOKICKAUTHENTICATIONLEVEL,
            AuthenticationManager::AUTH_LEVEL_MODERATOR,
            AuthenticationManager::AUTH_LEVEL_PLAYER
        );
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_KICKUNTILNBPLAYERS        => 0,
                self::SETTING_MESSAGEONKNOCKOUTKICK     => 'You got eliminated & kicked to reduce server load!',
                self::SETTING_NOKICKAUTHENTICATIONLEVEL => AuthenticationManager::AUTH_NAME_PLAYER,
                self::SETTING_SPECTATORLIST             => '',
                self::SETTING_SPECTATORLIST_ISFILE      => false,
            ),
            PresetManager::PRESET_THROWBACK => array(
                self::SETTING_SPECTATORLIST => '',
            ),
            PresetManager::PRESET_TC => array(
                self::SETTING_SPECTATORLIST        => 'tc_spectators.txt',
                self::SETTING_SPECTATORLIST_ISFILE => true,
            ),
            PresetManager::PRESET_TTC => array(
                self::SETTING_KICKUNTILNBPLAYERS        => 5,
                self::SETTING_NOKICKAUTHENTICATIONLEVEL => AuthenticationManager::AUTH_NAME_MODERATOR,
                self::SETTING_SPECTATORLIST             => 'railag',
            ),
        );
        return $presets;
    }

    public function unload()
    {
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;

        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    public function handlePlayerKoCallback(Player $player, int $position, int $nbRoundsSurvived)
    {
        $kickUntil = $this->getKickUntilNbPlayers();
        if ($position <= $kickUntil ||
            $this->maniaControl->getAuthenticationManager()->checkPluginPermission(
                $this->knockOutPlugin,
                $player,
                self::SETTING_NOKICKAUTHENTICATIONLEVEL
            ) )
        {
            // spec force if player has higher Authentication Level or there are a few players left
            $success = $this->knockOutPlugin->force(
                $player,
                PlayerActions::SPECTATOR_SPECTATOR
            );

            if (!$success)
            {
                $this->knockOutPlugin->kick(
                    $player,
                    'Not enough spec-slots available, please re-join later!'
                );
            }
        }
        elseif ($kickUntil > 0)
        {
            $this->knockOutPlugin->kick(
                $player,
                $this->getMessageOnKnockoutKick()
            );
        }
    }

    public function handleSettingChangedCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this->knockOutPlugin))
            return;

        if ($setting->setting != self::SETTING_SPECTATORLIST
         && $setting->setting != self::SETTING_SPECTATORLIST_ISFILE)
            return;

        $this->parseSpectatorList();
    }

    public function parseSpectatorList()
    {
        $this->spectatorList = array();
        $logins = (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SPECTATORLIST
        );

        if ($this->getSpectatorListIsFile())
        {
            $filename = $logins;
            if (strlen($filename) <= 0)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'No file specified to parse Spectator List from!'
                );
                return;
            }

            $logins = file_get_contents($filename);
            if ($logins === false)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    TMMUtils::formatMessage(
                        'Could not open file %s to parse Spectator List from!',
                        $filename
                    )
                );

                return;
            }
        }

        $this->spectatorList = preg_split('/[^\-\.0-9\_a-z]+/', $logins, -1, PREG_SPLIT_NO_EMPTY);
        $this->spectatorList = array_unique($this->spectatorList);
        $this->knockOutPlugin->chat(
            ChatMode::ADMIN_INFORMATION,
            TMMUtils::formatMessage(
                'Spectator List: %s',
                implode(', ', $this->spectatorList)
            )
        );
    }
}
