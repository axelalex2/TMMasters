<?php

namespace TMMasters\KnockOut\Extensions;

use FML\Controls\Frame;
use FML\Controls\Labels\Label_Text;
use FML\Controls\Quad;
use FML\Controls\Quads\Quad_Bgs1;
use FML\Controls\Quads\Quad_Bgs1InRace;
use FML\ManiaLink;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallQueueListener;
use ManiaControl\Logger;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use ManiaControl\Utils\Formatter;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOut\KnockOutRoundScore;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;

class WidgetRound extends Extension implements CallbackListener, CallQueueListener
{
    /**
     * ManiaLink IDs
     */
    private const MLID_WIDGET      = 'KnockOut.WidgetRound';
    private const MLID_WIDGET_LINE = 'KnockOut.WidgetRound.Line.';

    /**
     * Settings
     */
    private const SETTING_BACKGROUNDLINE     = 'WidgetRound/Background Line';
    private const SETTING_COLOR_DC           = 'WidgetRound/Color/DC';
    private const SETTING_COLOR_DNF          = 'WidgetRound/Color/DNF';
    private const SETTING_COLOR_KO           = 'WidgetRound/Color/KO';
    private const SETTING_COLOR_LAST         = 'WidgetRound/Color/Last';
    private const SETTING_COLOR_PRELAST      = 'WidgetRound/Color/Pre-Last';
    private const SETTING_COLOR_SAFE         = 'WidgetRound/Color/Safe';
    private const SETTING_LINEHEIGHT         = 'WidgetRound/Line Height';
    private const SETTING_NBLINESBOTTOM      = 'WidgetRound/Nb Lines Bottom';
    private const SETTING_NBLINESTOP         = 'WidgetRound/Nb Lines Top';
    private const SETTING_POSITIONX          = 'WidgetRound/Position X';
    private const SETTING_POSITIONY          = 'WidgetRound/Position Y';
    private const SETTING_TIMEDIFFERENCE     = 'WidgetRound/Show Time Difference';
    private const SETTING_TIMEDIFFERENCE_DNF = 'WidgetRound/Show Time Difference of DNFs to Pre-Last';
    private const SETTING_TITLE              = 'WidgetRound/Title';
    private const SETTING_WIDTH              = 'WidgetRound/Width';

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    private $backgroundLines = array();

    /**
     * Setting Functions
     */
    public function getBackgroundLine()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_BACKGROUNDLINE
        );
    }
    public function getColorDC()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_COLOR_DC
        );
    }
    public function getColorDNF()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_COLOR_DNF
        );
    }
    public function getColorKO()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_COLOR_KO
        );
    }
    public function getColorLast()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_COLOR_LAST
        );
    }
    public function getColorPreLast()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_COLOR_PRELAST
        ); 
    }
    public function getColorSafe()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_COLOR_SAFE
        );
    }
    public function getLineHeight()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_LINEHEIGHT
        );
    }
    public function getNbLinesBottom()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_NBLINESBOTTOM
        );
    }
    public function getNbLinesTop()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_NBLINESTOP
        );
    }
    public function getPositionX()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_POSITIONX
        );
    }
    public function getPositionY()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_POSITIONY
        );
    }
    public function getTimeDifference()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_TIMEDIFFERENCE
        );
    }
    public function getTimeDifferenceDNF()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_TIMEDIFFERENCE_DNF
        );
    }
    public function getTitle()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_TITLE
        );
    }
    public function getWidth()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_WIDTH
        );
    }

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        if (!$this->knockOutPlugin->getDataManager()->isScriptMode())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                'Server not running in Script Mode, cannot display WidgetRound properly, disabling!'
            );
            return;
        }

        // Callbacks
        // newly joined player would not see the widget
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_JOIN,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_BEGIN,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_MAP_BEGIN,
            $this,
            'clearBackgroundLines'
        );
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_MAP_BEGIN,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this,
            'clearBackgroundLines'
        );
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_FINISH,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_RETIRE,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_KO,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_DC,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_END,
            $this,
            'clearBackgroundLines'
        );
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_END,
            $this,
            'hide'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            SettingManager::CB_SETTING_CHANGED,
            $this,
            'handleUpdateSettingsCallback'
        );
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_BACKGROUNDLINE     => true,
                self::SETTING_COLOR_DC           => '500',
                self::SETTING_COLOR_DNF          => 'a00',
                self::SETTING_COLOR_KO           => 'f00',
                self::SETTING_COLOR_LAST         => 'f80',
                self::SETTING_COLOR_PRELAST      => 'ff0',
                self::SETTING_COLOR_SAFE         => 'fff',
                self::SETTING_LINEHEIGHT         => 4,
                self::SETTING_NBLINESBOTTOM      => 10,
                self::SETTING_NBLINESTOP         => 5,
                self::SETTING_POSITIONX          => -135,
                self::SETTING_POSITIONY          => 48,
                self::SETTING_TIMEDIFFERENCE     => true,
                self::SETTING_TIMEDIFFERENCE_DNF => true,
                self::SETTING_TITLE              => 'KO Round',
                self::SETTING_WIDTH              => 50,
            ),
            PresetManager::PRESET_SPECIAL => array(
                self::SETTING_NBLINESBOTTOM => 15,
            ),
            PresetManager::PRESET_TTC => array(
                self::SETTING_TITLE => 'TTC Round',
            ),
        );
        return $presets;
    }

    public function registerCallsOnKoUnload()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'hide'
            )
        );
    }

    public function unload()
    {
        $this->hide();
        
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;
        
        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    /**
     * Updates the round widget at the end of the round.
     * @param $timestamp (unused)
     * @param $nbPlayers (for tests)
     * @param $nbSurvivors (for tests)
     */
    public function build($nbPlayers = null, $nbSurvivors = null)
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive()
         || !$this->knockOutPlugin->getDataManager()->isScriptMode())
        {
            $this->hide();
            return;
        }

        if ($nbPlayers === null)
            $nbPlayers = $this->knockOutPlugin->getDataManager()->getNbPlayersRoundStart();
        if ($nbSurvivors === null)
            $nbSurvivors = $nbPlayers - $this->knockOutPlugin->getDataManager()->getNbKnockoutsRoundStart();

        $lineHeight    = $this->getLineHeight();
        $nbLinesBottom = $this->getNbLinesBottom();
        $nbLinesTop    = $this->getNbLinesTop();
        $posX          = $this->getPositionX();
        $posY          = $this->getPositionY();
        $title         = $this->getTitle();
        $width         = $this->getWidth();
        $nbLines       = $nbLinesTop + $nbLinesBottom;

        $labelStyle   = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultLabelStyle();
        $quadStyle    = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadStyle();
        $quadSubstyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadSubstyle();

        $boundTopPosition = $nbLinesTop;
        $boundBottomPosition = $nbPlayers - $nbLinesBottom + 1;

        $manialink = new ManiaLink(self::MLID_WIDGET);
        $frame = new Frame();
        $manialink->addChild($frame);
        $frame->setPosition($posX, $posY);

        $backgroundQuad = new Quad();
        $frame->addChild($backgroundQuad);
        $height = 8. + $nbLines*$lineHeight + $lineHeight/2;
        $backgroundQuad->setSize($width, $height);
        $backgroundQuad->setStyles($quadStyle, $quadSubstyle);
        $backgroundQuad->setVerticalAlign($backgroundQuad::TOP);

        $titleLabel = new Label_Text();
        $frame->addChild($titleLabel);
        $titleLabel->setPosition(0, -(0.9*$lineHeight));
        $titleLabel->setStyle($labelStyle);
        $titleLabel->setText($title);
        $titleLabel->setTextSize(2);
        $titleLabel->setWidth($width);

        $status = $this->knockOutPlugin->getDataManager()->isRoundLive() ? '$0f0Live' : '$f00Idle';
        $subtitleLabel = new Label_Text();
        $frame->addChild($subtitleLabel);
        $subtitleLabel->setPosition(0, -(1.5*$lineHeight));
        $subtitleLabel->setStyle($labelStyle);
        $subtitleLabel->setText("Status: {$status}");
        $subtitleLabel->setTextSize(0.5);
        $subtitleLabel->setWidth($width);

        $posY = -9.;

        $koSeparator = false;
        // if $nbRoundScores == 0 this will get skipped anyways
        $nbRoundScores = $this->knockOutPlugin->getDataManager()->getNbRoundScores();
        for ($position = 1; $position <= $nbRoundScores; $position++)
        {
            // separator for top positions
            if ($position === $boundTopPosition+1 && $nbPlayers > $nbLines)
            {
                $roundScore = $this->knockOutPlugin->getDataManager()->getRoundScore($position-2);
                if ($roundScore != null && $roundScore->finished())
                {
                    $posY -= $lineHeight/4;
                    $frame->addChild($this->buildSeparatorLine($posY + $lineHeight/6, 5));
                    $frame->addChild($this->buildSeparatorLine($posY - $lineHeight/6, 5));
                    $posY -= $lineHeight/4;
                }
            }

            $roundScore = $this->knockOutPlugin->getDataManager()->getRoundScore($position-1);
            assert($roundScore != null);
            $finished = $roundScore->finished();
            // separator for KOs
            if (!$koSeparator && ($position == $nbSurvivors+1 || !$finished))
            {
                $frame->addChild($this->buildSeparatorLine($posY, 6));
                $koSeparator = true;
            }

            if ($boundTopPosition < $position && $position < $boundBottomPosition && $finished)
                continue;
            
            $this->buildLineBackgroundQuad($posY, $roundScore->getPlayerLogin());

            // build round score, visible to everyone
            list($posStr, $timeStr, $color) = $this->getSingleScoreFrameProperties($position, $nbSurvivors);

            $roundScoreFrame = new Frame();
            $frame->addChild($roundScoreFrame);
            $roundScoreFrame->setPosition(0, $posY);

            $positionLabel = new Label_Text();
            $roundScoreFrame->addChild($positionLabel);
            $positionLabel->setHorizontalAlign($positionLabel::LEFT);
            $positionLabel->setSize($width * 0.1, $lineHeight);
            $positionLabel->setText($posStr);
            $positionLabel->setTextColor($color);
            $positionLabel->setTextEmboss(true);
            $positionLabel->setTextPrefix('$o');
            $positionLabel->setTextSize(1);
            $positionLabel->setX($width * -0.46);

            $nameLabel = new Label_Text();
            $roundScoreFrame->addChild($nameLabel);
            $nameLabel->setHorizontalAlign($nameLabel::LEFT);
            $nameLabel->setSize($width * 0.6, $lineHeight);
            $nameLabel->setText($roundScore->getPlayerName());
            $nameLabel->setTextEmboss(true);
            $nameLabel->setTextSize(1);
            $nameLabel->setX($width * -0.35);

            $timeLabel = new Label_Text();
            $roundScoreFrame->addChild($timeLabel);
            $timeLabel->setHorizontalAlign($timeLabel::RIGHT);
            $timeLabel->setSize($width * 0.3, $lineHeight);
            $timeLabel->setText($timeStr);
            $timeLabel->setTextColor($color);
            $timeLabel->setTextEmboss(true);
            $timeLabel->setTextPrefix($finished ? '' : '$o');
            $timeLabel->setTextSize(1);
            $timeLabel->setX($width * 0.46);

            $posY -= $lineHeight;
        }

        $this->maniaControl->getManialinkManager()->sendManialink($manialink);
    }

    private function buildLineBackgroundQuad($relPosY, string $login)
    {
        if (!$this->getBackgroundLine())
            return;
        
        $lineHeight = $this->getLineHeight();
        $posX       = $this->getPositionX();
        $absPosY    = $this->getPositionY();
        $width      = $this->getWidth();

        $manialink = new ManiaLink(self::MLID_WIDGET_LINE . $login);
        $frame = new Frame();
        $manialink->addChild($frame);
        $frame->setPosition($posX, $absPosY + $relPosY);

        $backgroundQuad = new Quad();
        $frame->addChild($backgroundQuad);
        $backgroundQuad->setSize($width * 0.95, $lineHeight);
        $backgroundQuad->setStyles(Quad_Bgs1::STYLE, Quad_Bgs1::SUBSTYLE_BgCardList);

        array_push($this->backgroundLines, $login);
        $this->maniaControl->getManialinkManager()->sendManialink($manialink, $login);
    }

    private function buildSeparatorLine($posY, $mult)
    {
        $lineHeight = $this->getLineHeight();
        $width      = $this->getWidth();

        $separatorQuad = new Quad();
        $separatorQuad->setSize($width * 0.95, $lineHeight/$mult);
        $separatorQuad->setStyles(Quad_Bgs1InRace::STYLE, Quad_Bgs1InRace::SUBSTYLE_BgCardList);
        $separatorQuad->setY($posY + $lineHeight/2);
        return $separatorQuad;
    }

    public function clearBackgroundLines()
    {
        foreach ($this->backgroundLines as $login)
            $this->maniaControl->getManialinkManager()->hideManialink(self::MLID_WIDGET_LINE . $login, $login);
        
        $this->backgroundLines = array();
    }

    private function getSingleScoreFrameProperties(int $position, int $nbSurvivors)
    {
        $roundScore = $this->knockOutPlugin->getDataManager()->getRoundScore($position-1);

        $posStr = '';
        $timeStr = '';
        $color = '';
        if ($roundScore->finished())
        {
            $posStr = (string) $position;
            if ($this->getTimeDifference() && $position > 1)
            {
                if ($this->getTimeDifferenceDNF() && $position == $nbSurvivors)
                    $timeStr = Formatter::formatTime($roundScore->getTime());
                else
                {
                    $roundScoreCompare = $this->getTimeDifferenceDNF() && $position > $nbSurvivors ? $nbSurvivors : 1;
                    $roundScoreCompare = $this->knockOutPlugin->getDataManager()->getRoundScore($roundScoreCompare - 1);
                    assert($roundScoreCompare != null);
                    $timeStr = $this->getTimeDiffString($roundScore, $roundScoreCompare);
                }
            }
            else
            {
                $timeStr = Formatter::formatTime($roundScore->getTime());
            }

            if ($position == 1 || $position < $nbSurvivors-1)
                $color = $this->getColorSafe();
            elseif ($position == $nbSurvivors-1)
                $color = $this->getColorPreLast();
            elseif ($position == $nbSurvivors)
                $color = $this->getColorLast();
            elseif ($position > $nbSurvivors)
                $color = $this->getColorKO();
        }
        else
        {
            $posStr = '---';

            if ($roundScore->isDNF())
            {
                $timeStr = 'DNF';
                $color = $this->getColorDNF();
            }
            elseif ($roundScore->isDC())
            {
                $timeStr = 'DC';
                $color = $this->getColorDC();
            }
            else
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Internal Error, check Logs.'
                );
                Logger::logError("Internal Error: WidgetRound::getRoundScoreFrameProperties({$position}, {$nbSurvivors})");
            }
        }

        return array($posStr, $timeStr, $color);
    }

    public function getTimeDiffString(KnockOutRoundScore $roundScore, KnockOutRoundScore $roundScoreCompare)
    {
        $timeDiff = $roundScore->getTime() - $roundScoreCompare->getTime();
        if ($timeDiff < 0)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                'Internal Error, check Logs.'
            );
            Logger::logError("Internal Error: WidgetRound::getTimeDiffString({$roundScore}, {$roundScoreCompare})");
        }
        $timeDiffStr = Formatter::formatTime($timeDiff);
        if ($timeDiff < 60000)
            $timeDiffStr = substr($timeDiffStr, ($timeDiff < 10000 ? 3 : 2));
        $timeStr = "+ {$timeDiffStr}";

        return $timeStr;
    }

    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this->knockOutPlugin))
            return;
        
        $this->queueUpdate();
    }

    public function hide()
    {
        $this->clearBackgroundLines();
        $this->maniaControl->getManialinkManager()->hideManialink(self::MLID_WIDGET);
    }

    public function queueUpdate()
    {
        if (!$this->maniaControl->getCallQueueManager()->hasListening($this, 'build'))
            $this->maniaControl->getCallQueueManager()->registerListening($this, 'build');
    }
}
