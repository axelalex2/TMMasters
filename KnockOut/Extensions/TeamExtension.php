<?php

namespace TMMasters\KnockOut\Extensions;

use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallQueueListener;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;
use TMMasters\TMMUtils;

class TeamExtension extends Extension implements CallbackListener, CallQueueListener, CommandListener
{
    /**
     * Commands
     */
    private const COMMAND_TEAM      = array('koteam');
    private const COMMAND_TEAM_DROP = array('koteamdrop');

    /**
     * Settings
     */
    private const SETTING_JOIN_MESSAGE   = 'Team/Join Message';
    private const SETTING_MULTIPLE_LIVES = 'Team/Multiple Lives';
    private const SETTING_TEAMSIZE       = 'Team/Teamsize';

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    private $maxTeamsize       = null;
    private $minTeamsize       = null;
    private $playerToTeamIndex = array(); // $player => index of $teamsRegistered
    private $teamsRegistered   = array(); // array($allTeammates)
    private $teamsRequested    = array(); // $player => array($allTeammates)

    /**
     * Setting Functions
     */
    public function getJoinMessage()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_JOIN_MESSAGE
        );
    }
    public function getMultipleLives()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MULTIPLE_LIVES
        );
    }
    public function isTeammode()
    {
        return is_int($this->maxTeamsize) && is_int($this->minTeamsize);
    }

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            PlayerManager::CB_PLAYERCONNECT,
            $this,
            'handlePlayerConnectCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            FormatExtension::KOCB_PLAYER_KILL,
            $this,
            'handlePlayerKillCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_KO,
            $this,
            'handlePlayerKoCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this,
            'handleRoundBeginCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            SettingManager::CB_SETTING_CHANGED,
            $this,
            'handleUpdateSettingsCallback'
        );

        // Command
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_TEAM,
            $this,
            'onCommandKoTeam',
            false,
            'Register your team for the KnockOut.'
        );
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_TEAM_DROP,
            $this,
            'onCommandKoTeamDropAdmin',
            true,
            'Drops all teams (or a players request) from the KnockOut.'
        );
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_TEAM_DROP,
            $this,
            'onCommandKoTeamDropPersonal',
            false,
            'Drops your team from the KnockOut.'
        );
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_JOIN_MESSAGE   => 'Get some teammates and register with $<$fff/koteam$>',
                self::SETTING_TEAMSIZE       => '',
                self::SETTING_MULTIPLE_LIVES => true,
            ),
        );
        return $presets;
    }

    public function registerCallsOnKoLoad()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'kickSoloPlayers',
                'loadPlayerNbLives',
            )
        );
    }

    public function registerCallsOnKoUnload()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'unloadPlayerNbLives',
                'dropAllTeams',
            )
        );
    }

    public function unload()
    {
        $this->maniaControl->getCommandManager()->unregisterCommandListener($this);
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;

        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    public function dropAllTeams($admin = null)
    {
        $this->playerToTeamIndex = array();
        $this->teamsRegistered   = array();
        $this->teamsRequested    = array();

        if ($admin instanceof Player)
        {
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                TMMUtils::formatMessage(
                    '%s dropped all teams!',
                    $player
                )
            );
        }
        else
        {
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                'All teams got dropped!'
            );
        }
    }

    public function dropTeam(Player $admin, $player = null)
    {
        if (is_array($player) && !empty($player))
        {
            foreach ($player as $p)
                $this->dropTeam($admin, $p);
            
            return;
        }

        assert($player instanceof Player);
        
        if (array_key_exists($player->login, $this->playerToTeamIndex))
        {
            $teamIndex = $this->playerToTeamIndex[$player->login];
            assert(array_key_exists($teamIndex, $this->teamsRegistered));
            $teammates = $this->teamsRegistered[$teamIndex];
            foreach ($teammates as $teammate)
            {
                assert(array_key_exists($teammate->login, $this->playerToTeamIndex));
                assert($this->playerToTeamIndex[$teammates->login] === $teamIndex);
                unset($this->playerToTeamIndex[$teammate->login]);

                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    TMMUtils::formatMessage(
                        '%s dropped your team!',
                        $admin
                    ),
                    $teammate
                );
            }

            unset($this->teamsRegistered[$teamIndex]);
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                TMMUtils::formatMessage(
                    'You dropped the team of %s!',
                    $player
                ),
                $admin
            );
        }
        elseif (!array_key_exists($player->login, $this->teamsRequested))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                TMMUtils::formatMessage(
                    'Cannot drop team request of %s, no team request available!',
                    $player
                ),
                $admin
            );
        }
        else
        {
            unset($this->teamsRequested[$player->login]);
            $this->knockOutPlugin->chat(
                ChatMode::SUCCESS,
                TMMUtils::formatMessage(
                    'Dropped team request of %s!',
                    $player
                ),
                $admin
            );
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                TMMUtils::formatMessage(
                    '%s dropped your team request!',
                    $admin
                ),
                $player
            );
        }
    }

    private function generateTeamIndex()
    {
        static $teamIndex = 0;
        $teamIndex++;
        return $teamIndex;
    }
    
    public function handlePlayerConnectCallback(Player $player)
    {
        if ($this->knockOutPlugin->getDataManager()->isMatchLive())
            return;
        
        if (!$this->isTeammode())
            return;
        
        if ($this->minTeamsize === 1 && $this->maxTeamsize > 1)
        {
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                'This KO can be played as a team!',
                $player
            );
        }
        elseif ($this->minTeamsize > 1)
        {
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                'This KO has to be played as a team!',
                $player
            );
        }
        
        $strTeamsizeNeeded = (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_TEAMSIZE
        );

        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            $this->getJoinMessage(),
            $player
        );
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            TMMUtils::formatMessage(
                'You need to be in a team of %s player(s) to participate!',
                $strTeamsizeNeeded
            ),
            $player
        );
    }

    public function handlePlayerKillCallback(Player $player)
    {
        if (!$this->isTeammode())
            return;
        
        $teamIndex = $this->playerToTeamIndex[$player->login];
        if (!array_key_exists($teamIndex, $this->teamsRegistered))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                TMMUtils::formatMessage(
                    'TeamIndex %s of %s is not registered!',
                    $teamIndex,
                    $player
                )
            );
            return;
        }

        $teammates = $this->teamsRegistered[$teamIndex];
        foreach ($teammates as $teammate)
        {
            if ($teammate === $player)
                continue;
            
            $formatExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(FormatExtension::class);
            if ($formatExtension != null)
                $formatExtension->handlePlayerKillCallback($teammate);
        }
    }

    public function handlePlayerKoCallback(Player $player, int $position, int $nbRoundsSurvived)
    {
        if (!$this->isTeammode())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->getPlayerList()->isAlive($player))
            return;
        
        $teamIndex = $this->playerToTeamIndex[$player->login];
        unset($this->playerToTeamIndex[$player->login]);
        if (array_key_exists($teamIndex, $this->teamsRegistered))
        {
            $teammates = $this->teamRegistered[$teamIndex];
            foreach ($teammates as $teammate)
            {
                $nbRoundsTotalFinished = $this->knockOutPlugin->getDataManager()->getNbRoundsTotalFinished();
                $this->maniaControl->getCallQueueManager()->registerListening(
                    $this,
                    function () use ($teammate, $nbRoundsTotalFinished) {
                        $nbPlayersCurrent = $this->knockOutPlugin->getDataManager()->getNbPlayersCurrent();
                        $this->maniaControl->getCallbackManager()->triggerCallback(
                            KnockOutCallbacks::KO_PLAYER_KO,
                            $teammate,
                            $nbPlayersCurrent,
                            $nbRoundsTotalFinished
                        );
                    }
                );
            }

            unset($this->teamsRegistered[$teamIndex]);
        }
    }

    public function handleRoundBeginCallback()
    {
        if (!$this->isTeammode())
            return;
        
        $requiredTeamsCount = 2 - (int) $this->knockOutPlugin->getAdminsDebug();
        if (count($this->teamsRegistered) < $requiredTeamsCount)
            $this->knockOutPlugin->stopKnockOut();
    }

    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this->knockOutPlugin))
            return;

        if ($setting->setting !== self::SETTING_TEAMSIZE)
            return;

        if (!preg_match('/([0-9]+-[0-9]+)|[0-9]*/', $setting->value))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                TMMUtils::formatMessage(
                    'Teamsize setting %s is invalid!',
                    $setting->value
                )
            );
            return;
        }

        $this->dropAllTeams();
        $this->maxTeamsize = null;
        $this->minTeamsize = null;

        if ($setting->value === '')
            return;

        $posRange = strpos($setting->value, '-');
        if ($posRange === false)
        {
            $teamsize = intval($setting->value);
            if ($teamsize <= 0)
                return;
            
            $this->maxTeamsize = $teamsize;
            $this->minTeamsize = $teamsize;
        }
        else
        {
            $maxTeamsize = intval(substr($setting->value, $posRange+1));
            $minTeamsize = intval(substr($setting->value, 0, $posRange));

            if ($maxTeamsize < $minTeamsize)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    TMMUtils::formatMessage(
                        'Teamsize setting %s is invalid, max teamsize cannot be smaller than the min teamsize!',
                        $setting->value
                    )
                );
                return;
            }

            $this->maxTeamsize = $maxTeamsize;
            $this->minTeamsize = $minTeamsize;
        }
    }

    public function kickSoloPlayers()
    {
        if (!$this->isTeammode())
            return;

        $players = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($players as $player)
        {
            if (!array_key_exists($player->login, $this->playerToTeamIndex))
            {
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    TMMUtils::formatMessage(
                        'Kicking %s for playing solo in Team KO!',
                        $player
                    )
                );
                $this->knockOutPlugin->kick(
                    $player,
                    'You have no teammates in the Team KO!'
                );
            }
        }
    }

    public function loadPlayerNbLives()
    {
        if (!$this->isTeammode())
            return;
        
        if (!$this->getMultipleLives())
            return;

        $formatExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(FormatExtension::class);
        if ($formatExtension === null)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                'FormatExtension not loaded, cannot make use of multiple lives!'
            );
            return;
        }

        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Loading teams lives ...'
        );

        $players = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($players as $player)
        {
            $nbLives = $player->getCache(
                $formatExtension,
                FormatExtension::CACHE_NBLIVES
            );
            if ($nbLives === null)
                $nbLives = 1;

            assert(array_key_exists($player->login, $this->playerToTeamIndex));
            $teamIndex = $this->playerToTeamIndex[$player->login];

            if (!array_key_exists($teamIndex, $this->teamsRegistered))
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    TMMUtils::formatMessage(
                        'TeamIndex %s of %s is not registered!',
                        $teamIndex,
                        $player
                    )
                );
                continue;
            }

            $teammates = $this->teamsRegistered[$teamIndex];
            $player->setCache(
                $formatExtension,
                FormatExtension::CACHE_NBLIVES,
                $nbLives * count($teammates)
            );
            var_dump($player->login);
        }
    }

    public function onCommandKoTeam(array $chatCallback, Player $player)
    {
        if ($this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot request team, KO-Match live!',
                $player
            );
            return;
        }

        if (!$this->isTeammode())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot request team, teams not enabled!',
                $player
            );
            return;
        }

        if (array_key_exists($player->login, $this->playerToTeamIndex))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot request team, you are already registered!',
                $player
            );
            return;
        }
        
        $chatCommand = explode(' ', $chatCallback[1][2]);
        $command = array_shift($chatCommand);
        assert(in_array($command, self::COMMAND_TEAM));
        
        $nbLogins = count($chatCommand);
        if ($nbLogins+1 < $this->minTeamsize || $this->maxTeamsize < $nbLogins+1)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot request team, wrong amount of logins specified!',
                $player
            );
            return;
        }

        try
        {
            $team = TMMUtils::loginsToPlayers($this->maniaControl, $chatCommand, true);
            if (!array_key_exists($player->login, $team))
                $team[$player->login] = $player;
            
            $team = array_values($team);
        }
        catch (Exception $e)
        {
            $this->knockOutPlugin->chat(
                ChatMode::EXCEPTION,
                $e,
                $player
            );
            return;
        }

        if (count($team) < $this->minTeamsize || $this->maxTeamsize < count($team))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot request team, not enough players!',
                $player
            );
            return;
        }

        foreach ($team as $teammate)
        {
            if (array_key_exists($teammate->login, $this->playerToTeamIndex))
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ERROR,
                    TMMUtils::formatMessage(
                        'Cannot request team, %s is already registered for another team!',
                        $teammate
                    ),
                    $player
                );
                return;
            }
        }

        $this->requestTeam($player, $team);
    }

    public function onCommandKoTeamDropAdmin(array $chatCallback, Player $admin)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkRight(
            $admin,
            $this->knockOutPlugin->getAdminsAuthenticationLevel()
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($admin);
            return;
        }

        if ($this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot drop teams, KO-Match live!',
                $admin
            );
            return;
        }
        
        $chatCommand = explode(' ', $chatCallback[1][2]);
        $command = array_shift($chatCommand);
        assert(in_array($command, self::COMMAND_TEAM_DROP));

        try
        {
            $players = TMMUtils::loginsToPlayers($this->maniaControl, $chatCommand, true);
            if (!empty($players))
                $this->dropTeam($admin, $players);
            else
                $this->dropAllTeams($admin);
        }
        catch (Exception $e)
        {
            $this->knockOutPlugin->chat(
                ChatMode::EXCEPTION,
                $e,
                $admin
            );
        }
    }

    public function onCommandKoTeamDropPersonal(array $chatCallback, Player $player)
    {
        if ($this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot drop team, KO-Match live!',
                $player
            );
            return;
        }

        if (!array_key_exists($player->login, $this->playerToTeamIndex))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot drop team, not registered yet!',
                $player
            );
            return;
        }
        
        $chatCommand = explode(' ', $chatCallback[1][2]);
        $command = array_shift($chatCommand);
        assert(in_array($command, self::COMMAND_TEAM_DROP));

        $this->dropTeam($player, $player);
    }

    private function requestTeam(Player $player, array $team)
    {
        $this->teamsRequested[$player->login] = $team;
        $teamComplete = true;
        foreach ($team as $teammate)
        {
            if (!array_key_exists($teammate->login, $this->teamsRequested))
            {
                $teamComplete = false;
                break;
            }

            $requestByTeammate = TMMUtils::playersToLogins($this->teamsRequested[$teammate->login]);
            $requestByPlayer   = TMMUtils::playersToLogins($team);
            sort($requestByTeammate);
            sort($requestByPlayer);
            if ($requestByPlayer != $requestByTeammate)
            {
                $teamComplete = false;
                break;
            }
        }

        if ($teamComplete)
        {
            $teamIndex = $this->generateTeamIndex();
            $this->teamsRegistered[$teamIndex] = $team;
            foreach ($team as $teammate)
            {
                $this->playerToTeamIndex[$teammate->login] = $teamIndex;
                unset($this->teamsRequested[$teammate->login]);
            }

            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                TMMUtils::formatMessage(
                    'Team %s registered: %s',
                    "#{$teamIndex}",
                    TMMUtils::chatFormatPlayer($team)
                )
            );
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                TMMUtils::formatMessage(
                    'If you want to change your team before the KO starts, type %s, and your current team will be dropped!',
                    self::COMMAND_TEAM_DROP[0]
                ),
                $team
            );
        }
        else
        {
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                TMMUtils::formatMessage(
                    '%s requested a team: %s',
                    $player,
                    TMMUtils::chatFormatPlayer($team)
                ),
                $team
            );
            foreach ($team as $teammate)
            {
                if (array_key_exists($teammate->login, $this->teamsRequested))
                    continue;
                
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    TMMUtils::formatMessage(
                        'If you want to accept this team request, type: %s %s',
                        self::COMMAND_TEAM[0],
                        implode(' ', TMMUtils::playersToLogins($team))
                    ),
                    $teammate
                );
            }
        }
    }

    public function unloadPlayerNbLives()
    {
        $formatExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(FormatExtension::class);
        assert($formatExtension != null);

        $players = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($players as $player)
        {
            $player->destroyCache(
                $formatExtension,
                FormatExtension::CACHE_NBLIVES
            );
        }
    }
}
