<?php

namespace TMMasters\KnockOut\Extensions;

use FML\Controls\Frame;
use FML\Controls\Labels\Label_Text;
use FML\Controls\Quad;
use FML\ManiaLink;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallQueueListener;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\Extensions\FormatExtension;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;

class WidgetInfo extends Extension implements CallbackListener, CallQueueListener
{
    /**
     * ManiaLink IDs
     */
    private const MLID_WIDGET = 'KnockOut.WidgetInfo';
    
    /**
     * Settings
     */
    private const SETTING_DESIGN_LINEHEIGHT = 'WidgetInfo/Design/Line Height';
    private const SETTING_DESIGN_POSITIONX  = 'WidgetInfo/Design/Position X';
    private const SETTING_DESIGN_POSITIONY  = 'WidgetInfo/Design/Position Y';
    private const SETTING_DESIGN_TITLE      = 'WidgetInfo/Design/Title';
    private const SETTING_DESIGN_WIDTH      = 'WidgetInfo/Design/Width';

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    /**
     * Setting Functions
     */
    public function getDesignLineHeight()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_DESIGN_LINEHEIGHT
        );
    }
    public function getDesignPositionX()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_DESIGN_POSITIONX
        );
    }
    public function getDesignPositionY()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_DESIGN_POSITIONY
        );
    }
    public function getDesignTitle()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_DESIGN_TITLE
        );
    }
    public function getDesignWidth()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_DESIGN_WIDTH
        );
    }

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        // newly joined player would not see the widget
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            PlayerManager::CB_PLAYERCONNECT,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_BEGIN,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_MAP_BEGIN,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_RETIRE,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_KO,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_DC,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_END,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            BreakExtension::KOCB_BREAK_STATE,
            $this,
            'queueUpdate'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            SettingManager::CB_SETTING_CHANGED,
            $this,
            'handleUpdateSettingsCallback'
        );
    }

    public function getOptionalDependencies()
    {
        static $optionalDependencies = array(
            FormatExtension::class,
        );
        return $optionalDependencies;
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_DESIGN_LINEHEIGHT => 4,
                self::SETTING_DESIGN_POSITIONX  => -135,
                self::SETTING_DESIGN_POSITIONY  => 73,
                self::SETTING_DESIGN_TITLE      => 'KO Info',
                self::SETTING_DESIGN_WIDTH      => 50,
            ),
            PresetManager::PRESET_TTC => array(
                self::SETTING_DESIGN_TITLE => 'TTC Info',
            ),
        );
        return $presets;
    }

    public function unload()
    {
        $this->hide();

        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;
        
        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    public function build()
    {
        $dataManager = $this->knockOutPlugin->getDataManager();

        $lineHeight = $this->getDesignLineHeight();
        $posX       = $this->getDesignPositionX();
        $posY       = $this->getDesignPositionY();
        $title      = $this->getDesignTitle();
        $width      = $this->getDesignWidth();
        $height     = 8. + 4 * $lineHeight;

        $labelStyle   = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultLabelStyle();
        $quadStyle    = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadStyle();
        $quadSubstyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadSubstyle();

        $manialink = new ManiaLink(self::MLID_WIDGET);
        $frame = new Frame();
        $manialink->addChild($frame);
        $frame->setPosition($posX, $posY);

        $backgroundQuad = new Quad();
        $frame->addChild($backgroundQuad);
        $backgroundQuad->setSize($width, $height);
        $backgroundQuad->setStyles($quadStyle, $quadSubstyle);
        $backgroundQuad->setVerticalAlign($backgroundQuad::TOP);

        $titleLabel = new Label_Text();
        $frame->addChild($titleLabel);
        $titleLabel->setPosition(0, -(0.9*$lineHeight));
        $titleLabel->setStyle($labelStyle);
        $titleLabel->setText($title);
        $titleLabel->setTextSize(2);
        $titleLabel->setWidth($width);

        $status = $dataManager->isPaused() ? '$bb0Paused' : ($dataManager->isMatchLive() ? '$0f0Live' : '$f00Idle');
        $subtitleLabel = new Label_Text();
        $frame->addChild($subtitleLabel);
        $subtitleLabel->setPosition(0, -(1.5*$lineHeight));
        $subtitleLabel->setStyle($labelStyle);
        $subtitleLabel->setText("Status: {$status}");
        $subtitleLabel->setTextSize(0.5);
        $subtitleLabel->setWidth($width);

        if ($dataManager->isMatchLive())
        {
            // TODO cleanup
            
            $posY = -9.;
            $nbRoundsTotalFrame = $this->buildLineFrame(
                $posY,
                'Round (Total):',
                "{$dataManager->getNbRoundsTotalCurrent()} / ~{$dataManager->getNbRoundsTotalMax()}"
            );
            $frame->addChild($nbRoundsTotalFrame);

            $posY -= $lineHeight;
            $roundOnMapMax = $dataManager->getNbRoundsMapMax();
            $nbRoundsMapFrame = $this->buildLineFrame(
                $posY,
                'Round (Map):',
                "{$dataManager->getNbRoundsMapCurrent()} / {$roundOnMapMax}"
            );
            $frame->addChild($nbRoundsMapFrame);

            $posY -= $lineHeight;
            $nbParticipantsFrame = $this->buildLineFrame(
                $posY,
                'Number Players:',
                "{$dataManager->getNbPlayersCurrent()} (-{$dataManager->getNbPlayersKoPrevRound()})"
            );
            $frame->addChild($nbParticipantsFrame);

            $posY -= $lineHeight;
            $nbKnockouts = $dataManager->getNbKnockoutsRoundStart();
            $strUntil = '';
            $formatExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(FormatExtension::class);
            if ($formatExtension != null)
            {
                $nextPlayerLimit = $formatExtension->getNextMultiKOPlayerLimit()[1];
                $strUntil = $nextPlayerLimit <= 1 ? ' (to the end)' : " (until {$nextPlayerLimit} Players)";
            }
            $nbKnockoutsFrame = $this->buildLineFrame(
                $posY,
                'Number KOs:',
                "{$nbKnockouts}{$strUntil}"
            );
            $frame->addChild($nbKnockoutsFrame);
        }

        $this->maniaControl->getManialinkManager()->sendManialink($manialink);
    }

    private function buildLineFrame($posY, string $desc, string $info)
    {
        $lineHeight = $this->getDesignLineHeight();
        $width      = $this->getDesignWidth();

        $frame = new Frame();
        $frame->setPosition(0, $posY);

        $descLabel = new Label_Text();
        $frame->addChild($descLabel);
        $descLabel->setHorizontalAlign($descLabel::LEFT);
        $descLabel->setSize($width * 0.5, $lineHeight);
        $descLabel->setText($desc);
        $descLabel->setTextEmboss(true);
        $descLabel->setTextSize(1);
        $descLabel->setX($width * -0.46);

        $infoLabel = new Label_Text();
        $frame->addChild($infoLabel);
        $infoLabel->setHorizontalAlign($infoLabel::RIGHT);
        $infoLabel->setSize($width * 0.5, $lineHeight);
        $infoLabel->setText($info);
        $infoLabel->setTextEmboss(true);
        $infoLabel->setTextPrefix('$o');
        $infoLabel->setTextSize(1);
        $infoLabel->setX($width * 0.46);

        return $frame;
    }

    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this->knockOutPlugin))
            return;

        $this->queueUpdate();
    }

    public function hide()
    {
        $this->maniaControl->getManialinkManager()->hideManialink(self::MLID_WIDGET);
    }

    public function queueUpdate()
    {
        if (!$this->maniaControl->getCallQueueManager()->hasListening($this, 'build'))
            $this->maniaControl->getCallQueueManager()->registerListening($this, 'build');
    }
}
