<?php

namespace TMMasters\KnockOut\Extensions;

use FML\Controls\Labels\Label_Text;
use FML\ManiaLink;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\TimerListener;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Manialinks\ManialinkManager;
use ManiaControl\Players\Player;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\Extensions\BreakExtension;
use TMMasters\KnockOut\Extensions\FormatExtension;
use TMMasters\KnockOut\Extensions\WarmUpExtension;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;
use TMMasters\TMMUtils;

class InfoExtension extends Extension implements CallbackListener, CommandListener, TimerListener
{
    /**
     * Commands
     */
    private const COMMAND_INFO = array('koinfo');

    /**
     * Settings
     */
    private const SETTING_ENABLE_WHEN_LIVE = 'Info/Enable when live';
    private const SETTING_MESSAGE_INTERVAL = 'Info/Message Interval [s]';

    /**
     * Setting Functions
     */
    public function getEnableWhenLive()
    {
        return (bool) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_ENABLE_WHEN_LIVE
        );
    }

    public function getMessageInterval()
    {
        return 1000 * (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MESSAGE_INTERVAL
        );
    }

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            SettingManager::CB_SETTING_CHANGED,
            $this,
            'handleUpdateSettingsCallback'
        );

        // Commands
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_INFO,
            $this,
            'onCommandInfo',
            false,
            'Displays information about the KnockOut.'
        );

        // Timers
        $this->maniaControl->getTimerManager()->registerTimerListening(
            $this,
            'showChatMessage',
            $this->getMessageInterval()
        );
    }

    public function getOptionalDependencies()
    {
        static $optionalDependencies = array(
            BreakExtension::class,
            FormatExtension::class,
            WarmUpExtension::class,
        );
        return $optionalDependencies;
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_ENABLE_WHEN_LIVE => false,
                self::SETTING_MESSAGE_INTERVAL => 120,
            ),
        );
        return $presets;
    }

    public function unload()
    {
        $this->maniaControl->getTimerManager()->unregisterTimerListenings($this);
        $this->maniaControl->getCommandManager()->unregisterCommandListener($this);
        $this->maniaControl = null;
        
        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    private function buildMessages()
    {
        $messages = array(
            'disconnect, retire or dnf = knockout(KO)',
            'else the last player(s) will get eliminated',
            'number of KOs decreases with less players',
        );

        $formatExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(FormatExtension::class);
        if ($formatExtension != null)
        {
            $respawnOnOff = $formatExtension->getScriptSAllowRespawn() ? 'on' : 'off';
            $messages = array_merge(
                $messages,
                array(
                    "respawn is {$respawnOnOff}, " .
                    "{$formatExtension->getScriptSRoundsPerMap()} round(s) per map, " .
                    "{$formatExtension->getScriptSFinishTimeout()} secs finish timeout",
                )
            );
        }

        $warmUpExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(WarmUpExtension::class);
        if ($warmUpExtension != null)
        {
            $sWarmUpNb = $warmUpExtension->getScriptSWarmUpNb(); 
            $warmUpNbReduce = $warmUpExtension->getReduceEveryNbMaps();
            $warmUpMessageAddition = $sWarmUpNb * $warmUpNbReduce;
            $warmUpMessageAddition = $warmUpNbReduce <= 0 ? '' : ", but only the first {$warmUpMessageAddition} maps";

            $messages = array_merge(
                $messages,
                array(
                    "{$sWarmUpNb} warmup(s){$warmUpMessageAddition}",
                    "no real warmup, only ~{$warmUpExtension->getScriptSWarmUpDuration()} seconds to prevent lags",
                )
            );
        }

        $breakExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(BreakExtension::class);
        if ($breakExtension != null)
        {
            $messages = array_merge(
                $messages,
                array(
                    "{$breakExtension->getDuration()} seconds break before half of the cup is over",
                )
            );
        }

        $messages = array_merge(
            $messages,
            array(
                'no cutting, swearing, insulting, spamming etc...',
                'violations may result in (permanent) disqualifications',
            )
        );

        return $messages;
    }

    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this->knockOutPlugin))
            return;
        
        if ($setting->setting != self::SETTING_MESSAGE_INTERVAL)
            return;

        $result = $this->maniaControl->getTimerManager()->updateTimerListening(
            $this,
            'showChatMessage',
            $this->getMessageInterval()
        );
    }
    
    public function onCommandInfo(array $chatCallback, Player $player)
    {
        if (!$this->getEnableWhenLive() && $this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot display information about KO format, when KO is live!',
                $player
            );
            return;
        }

        $messages = $this->buildMessages();

        // Build Manialink
        $height       = $this->maniaControl->getManialinkManager()->getStyleManager()->getListWidgetsHeight();
        $width        = $this->maniaControl->getManialinkManager()->getStyleManager()->getListWidgetsWidth();
        $quadStyle    = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultMainWindowStyle();
        $quadSubstyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultMainWindowSubStyle();

        $manialink = new ManiaLink(ManialinkManager::MAIN_MLID);
		$frame = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultListFrame();
        $manialink->addChild($frame);

        // Headline
        $label = new Label_Text();
        $frame->addChild($label);
        $label->setPosition(0, $height/2 - 5);
        $label->setStyle($label::STYLE_TextCardMedium);
        $label->setText('$o$s$f00TM Masters True Talent Cup');
        $label->setTextSize(3);

        $posX = -$width/2 +  7.5;
        $posY = $height/2 - 10  ;
        $alternativeColor = true;
        foreach ($messages as $line)
        {
            $label = new Label_Text();
            $frame->addChild($label);
            $label->setHorizontalAlign($label::LEFT);
            $label->setPosition($posX, $posY);
            $label->setStyle($label::STYLE_TextCardMedium);
            $label->setText($line);
            $label->setTextColor($alternativeColor ? 'f00' : 'ff0');
            $label->setTextPrefix('- ');
            $label->setTextSize(1.75);

            $posY -= 4.5;
            $alternativeColor = !$alternativeColor;
        }
        
        // Footer
        $label = new Label_Text();
        $frame->addChild($label);
        $label->setPosition(0, -$height/2 + 10);
        $label->setStyle($label::STYLE_TextCardMedium);
        $label->setText('Starting soon. Drive safe, do not give up and have fun!');
        $label->setTextEmboss(true);
        $label->setTextSize(1);

        // Display manialink
        $this->maniaControl->getManialinkManager()->sendManialink($manialink, $player);
    }

    public function showChatMessage()
    {
        if (!$this->getEnableWhenLive() && $this->knockOutPlugin->getDataManager()->isMatchLive())
            return;
        
        $command = self::COMMAND_INFO[0];
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            TMMUtils::formatMessage(
                'Type in %s to see information about the format!',
                "/{$command}"
            )
        );
    }
}
