<?php

namespace TMMasters\KnockOut\Extensions;

use FML\Controls\Frame;
use FML\Controls\Labels\Label_Text;
use FML\ManiaLink;
use FML\Script\Features\Paging;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Manialinks\ManialinkManager;
use ManiaControl\Players\Player;
use ManiaControl\Settings\SettingManager;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;

class ChangeLogExtension extends Extension implements CommandListener
{
    /**
     * Constants
     */
    private const CHANGELOG = array(
        '20.09' => array(
            'Add Team-support',
            'Fix wrong amount of KOs if number of KOs changed and player left early',
        ),
        '20.05' => array(
            'Added linear pointsystem',
            'Added multi-map support for pointsystems',
        ),
        '20.04' => array(
            'Added support for legacy-rounds mode',
            'Installed Karma-Plugin',
        ),
        '20.03' => array(
            'New relative Time to last-save player',
        ),
        '20.02' => array(
            'Disabled player slots when KO running',
            'Enabled chat message commands (/gg /glhf /nt etc.)',
            'Format settings reflect rounds survived',
        ),
        '20.01' => array(
            'Automatic map loader',
            'Disabled unnecessary ManiaControl commands',
            'Local record notification (Top 3 = public, Top 100 = private)',
            'Whitelist file feature',
        ),
        '19.12' => array(
            'Balancing calls of features',
            'Display widgets to players joining',
            'Multiple lives feature',
            'Split features from main plugin into separate extensions',
        ),
        '19.11' => array(
            'Color settings for scoreboard',
        ),
        '19.08' => array(
            'Automatic break trigger update (rounds finished >= rounds remaining)',
        ),
        '19.06' => array(
            'KO-Calls queued for performance balancing',
        ),
        '19.04' => array(
            'Categorized settings',
        ),
        '18.11' => array(
            'Chat prefix for TTC messages',
            'WarmUp reduction system',
        ),
        '18.10' => array(
            'Multiple KOs live updateable',
            'Scoreboard displaying time difference',
            'Scoreboard formatting updates',
        ),
        '18.09' => array(
            'Automatic spec-forcing of author and maptesters',
            'Better visible breaktimer',
            'Scoreboard (left-side ManiaControl-style)',
            'Scoreboard format live updateable',
            'Script load delay',
        ),
        '18.07' => array(
            'Small breaktimer',
            'Scoreboard header about KO state',
        ),
        '18.06' => array(
            'Initial plugin version',
            'Automatic break (rounds >= 35 + players)',
            'Chat messages about KO state',
            'Disabling ManiaPlanet UI',
            'Multiple KOs',
            'Result file',
            'Script settings',
            'Scoreboard (right-side Aseco-Style)',
            'Setting presets',
            'Whitelist',
        ),
    );

    /**
     * Commands
     */
    private const COMMAND_OPEN_CHANGELOG = array('kochangelog');

    /**
     * Settings
     */
    private const SETTING_ENABLE_WHEN_LIVE = 'ChangeLog/Enable when live';

    /**
     * Setting Functions
     */
    public function getEnableWhenLive()
    {
        return (bool) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_ENABLE_WHEN_LIVE
        );
    }

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Commands
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_OPEN_CHANGELOG,
            $this,
            'onCommandOpenChangeLog',
            false,
            'Display the log about plugin changes.'
        );
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_ENABLE_WHEN_LIVE => true,
            ),
        );
        return $presets;
    }

    public function unload()
    {
        $this->maniaControl->getCommandManager()->unregisterCommandListener($this);
        $this->maniaControl = null;
        
        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    public function onCommandOpenChangeLog(array $chatCallback, Player $player)
    {
        if (!$this->getEnableWhenLive() && $this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot open changelog, when KO is live!',
                $player
            );
            return;
        }

        // Build Manialink
        $height       = $this->maniaControl->getManialinkManager()->getStyleManager()->getListWidgetsHeight();
        $width        = $this->maniaControl->getManialinkManager()->getStyleManager()->getListWidgetsWidth();
        $quadStyle    = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultMainWindowStyle();
        $quadSubstyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultMainWindowSubStyle();

        $manialink = new ManiaLink(ManialinkManager::MAIN_MLID);
		$script    = $manialink->getScript();
		$paging    = new Paging();
        $script->addFeature($paging);
        
		$frame = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultListFrame($script, $paging);
		$manialink->addChild($frame);

        // Headline
        $label = new Label_Text();
        $frame->addChild($label);
        $label->setHorizontalAlign($label::LEFT);
        $label->setPosition(-$width/2 + 2, $height/2 - 2);
        $label->setStyle($label::STYLE_TextCardMedium);
        $label->setText('Changelog of TMMasters\\KnockOutPlugin');
        $label->setTextSize(4);
        $label->setVerticalAlign($label::TOP);

        $initialPosX = - $width/2 + 5;
        $initialPosY =  $height/2 - 13;
        foreach (self::CHANGELOG as $version => $changes)
        {
            $posX = $initialPosX;
            $posY = $initialPosY;

            $pageFrame = new Frame();
            $frame->addChild($pageFrame);
            $paging->addPageControl($pageFrame);
            
            $label = new Label_Text();
            $pageFrame->addChild($label);
            $label->setHorizontalAlign($label::LEFT);
            $label->setPosition($posX, $posY);
            $label->setStyle($label::STYLE_TextCardMedium);
            $label->setText("Version {$version}:");
            $label->setTextSize(2);

            $posX += 2;
            $posY -= 4;

            foreach ($changes as $change)
            {
                $label = new Label_Text();
                $pageFrame->addChild($label);
                $label->setHorizontalAlign($label::LEFT);
                $label->setPosition($posX, $posY);
                $label->setStyle($label::STYLE_TextCardSmall);
                $label->setText("- {$change}");
                $label->setTextSize(1);
                $posY -= 4;
            }
        }

        // Display manialink
        $this->maniaControl->getManialinkManager()->sendManialink($manialink, $player);
    }
}
