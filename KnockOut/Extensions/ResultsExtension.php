<?php

namespace TMMasters\KnockOut\Extensions;

use FML\Controls\Frame;
use FML\Controls\Labels\Label_Text;
use FML\Controls\Quad;
use FML\Controls\Quads\Quad_Bgs1;
use FML\Controls\Quads\Quad_Bgs1InRace;
use FML\ManiaLink;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Manialinks\ManialinkPageAnswerListener;
use ManiaControl\Players\Player;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\Extensions\EditionExtension;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;
use TMMasters\TMMUtils;

class KnockOutResult
{
    private $login            = '';
    private $name             = '';
    private $nbRoundsSurvived = 0;
    private $position         = 0;

    public function __construct(Player $player, int $position, int $nbRoundsSurvived)
    {
        $this->login            = $player->login;
        $this->name             = str_replace('"', "''", $player->getEscapedNickname());
        $this->nbRoundsSurvived = $nbRoundsSurvived;
        $this->position         = $position;
    }

    public function getNbRoundsSurvived() { return $this->nbRoundsSurvived; }
    public function getPlayerLogin     () { return $this->login           ; }
    public function getPlayerName      () { return $this->name            ; }
    public function getPosition        () { return $this->position        ; }
}

class ResultsExtension extends Extension implements CallbackListener, CommandListener, ManialinkPageAnswerListener
{
    /**
     * ManiaLink IDs
     */
    private const MLID_ACTION_HIDETOP30  = 'KnockOut.Results.Action.HideTop30';
    private const MLID_ACTION_HIDETOP250 = 'KnockOut.Results.Action.HideTop250';
    private const MLID_WIDGET            = 'KnockOut.Results.Widget';
    
    /**
     * Settings
     */
    // private const SETTING_DATABASE_PASSWORD = 'Results/Database/Password';
    // private const SETTING_DATABASE_SERVER   = 'Results/Database/Server';
    // private const SETTING_DATABASE_USER     = 'Results/Database/User';
    private const SETTING_FILENAME = 'Results/Filename';

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;
    private $results        = array();

    /**
     * Setting Functions
     */
    // None simple so far

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_KO,
            $this,
            'insertPlayerInResults'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_END,
            $this,
            'insertWinnerInResults'
        );

        // Commands
        $this->maniaControl->getCommandManager()->registerCommandListener(
            'koresults',
            $this,
            'onCommandKOResults',
            true,
            'Shows the results of the latest KnockOut.'
        );

        // Manialinks
        $this->maniaControl->getManialinkManager()->registerManialinkPageAnswerListener(
            self::MLID_ACTION_HIDETOP30,
            $this,
            'handleManialinkAnswerHideTop30'
        );

        $this->maniaControl->getManialinkManager()->registerManialinkPageAnswerListener(
            self::MLID_ACTION_HIDETOP250,
            $this,
            'handleManialinkAnswerHideTop250'
        );
    }

    public function getOptionalDependencies()
    {
        static $optionalDependencies = array(
            EditionExtension::class,
        );
        return $optionalDependencies;
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_FILENAME => '<Ymd>_ko_results.txt',
            ),
            PresetManager::PRESET_TC => array(
                self::SETTING_FILENAME => '<Ymd>_tc_qualification_<number>.txt',
            ),
            PresetManager::PRESET_TTC => array(
                self::SETTING_FILENAME => '<Ymd>_ttc_<style>_<number>.txt',
            ),
        );
        return $presets;
    }

    public function registerCallsOnKoLoad()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'clearCurrentResults',
            )
        );
    }

    public function registerCallsOnKoUnload()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'writeResultsFile',
            )
        );
    }

    public function unload()
    {
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl->getCommandManager()->unregisterCommandListener($this);
        $this->maniaControl->getManialinkManager()->unregisterManialinkPageAnswerListener($this);
        $this->maniaControl = null;
        
        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    public function clearCurrentResults()
    {
        $this->results = array();
    }

    private function displayResults(Player $player)
    {
        if ($this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Results cannot be displayed yet, KnockOut is still running!',
                $player
            );

            return;
        }

        $numberTotalParticipants = count($this->results);
        if ($numberTotalParticipants === 0)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'No results to show!',
                $player
            );

            return;
        }

        // sometimes the last place got inserted with the incorrect position
        if ($this->results[0]->getPosition() != $numberTotalParticipants)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Results are not complete!',
                $player
            );
        }

        $this->displayResultsTop30($player);
        // Top 250 will be displayed, when we get an answer from the ManialinkPage
    }

    private function displayResultsTop30(Player $player)
    {
        $numberTotalParticipants = count($this->results);

        $posX = -25.0;
        $posY = 70;

        $manialink = new ManiaLink(self::MLID_WIDGET);
        $frame = new Frame();
        $manialink->addChild($frame);
        $frame->setPosition(0, 0);

        $backgroundQuad = new Quad();
        $frame->addChild($backgroundQuad);
        $backgroundQuad->setAction(self::MLID_ACTION_HIDETOP30);
        $backgroundQuad->setPosition($posX+25, $posY-65, -1);
        $backgroundQuad->setSize(59, 160);
        $backgroundQuad->setStyles(Quad_Bgs1::STYLE, Quad_Bgs1::SUBSTYLE_BgTitle2);

        $quad = new Quad();
        $frame->addChild($quad);
        $quad->setPosition($posX+25, $posY-65, 0);
        $quad->setSize(57, 158);
        $quad->setStyles(Quad_Bgs1InRace::STYLE, Quad_Bgs1InRace::SUBSTYLE_BgList);

        $foregroundQuad = new Quad();
        $frame->addChild($foregroundQuad);
        $foregroundQuad->setPosition($posX+25, $posY+10, 1);
        $foregroundQuad->setSize(55, 5);
        $foregroundQuad->setStyles(Quad_Bgs1InRace::STYLE, Quad_Bgs1InRace::SUBSTYLE_BgList);

        $servernameLabel = new Label_Text();
        $frame->addChild($servernameLabel);
        $servernameLabel->setPosition($posX+25, $posY+10.3, 2);
        $servernameLabel->setSize(54, 4.5);
        $servernameLabel->setText($this->maniaControl->getClient()->getServerName());
        $servernameLabel->setTextEmboss(true);
        
        for ($i = 1; $i <= min($numberTotalParticipants, 30); $i++)
        {
            $result = $this->results[$numberTotalParticipants-$i];

            $positionQuad = new Quad();
            $frame->addChild($positionQuad);
            $positionQuad->setPosition($posX+1.0, $posY+5.0, 1);
            $positionQuad->setSize(7.0, 5.1);
            $positionQuad->setStyles(Quad_Bgs1InRace::STYLE, Quad_Bgs1InRace::SUBSTYLE_BgList);

            $positionLabel = new Label_Text();
            $frame->addChild($positionLabel);
            $positionLabel->setPosition($posX+0.8, $posY+5.3, 2);
            $positionLabel->setSize(6.5, 5);
            $positionLabel->setText("{$result->getPosition()}");
            $positionLabel->setTextColor('0f0');
            $positionLabel->setTextEmboss(true);

            $playerQuad = new Quad();
            $frame->addChild($playerQuad);
            $playerQuad->setPosition($posX+28.5, $posY+5.0, 1);
            $playerQuad->setSize(48.5, 5.1);
            $playerQuad->setStyles(Quad_Bgs1InRace::STYLE, Quad_Bgs1InRace::SUBSTYLE_BgList);

            $playerLabel = new Label_Text();
            $frame->addChild($playerLabel);
            $playerLabel->setHorizontalAlign($playerLabel::LEFT);
            $playerLabel->setPosition($posX+5.0, $posY+5.3, 2);
            $playerLabel->setSize(47, 4.5);
            $playerLabel->setText("{$result->getPlayerName()} \$z\$i\$n\$777({$result->getPlayerLogin()})({$result->getNbRoundsSurvived()})");
            $playerLabel->setTextEmboss(true);

            $posY -= 5;
        }

        $this->maniaControl->getManialinkManager()->sendManialink($manialink, $player);
    }

    private function displayResultsTop250(Player $player)
    {
        $numberTotalParticipants = count($this->results);

        $posX = -126.0;
        $posY = 86;

        $manialink = new ManiaLink(self::MLID_WIDGET);
        $frame = new Frame();
        $manialink->addChild($frame);
        $frame->setPosition(0, 0);

        $backgroundQuad = new Quad();
        $frame->addChild($backgroundQuad);
        $backgroundQuad->setAction(self::MLID_ACTION_HIDETOP250);
        $backgroundQuad->setPosition($posX+126.0, $posY-73.5, -1);
        $backgroundQuad->setSize(264, 156);
        $backgroundQuad->setStyles(Quad_Bgs1::STYLE, Quad_Bgs1::SUBSTYLE_BgTitle2);

        $quad = new Quad();
        $frame->addChild($quad);
        $quad->setPosition($posX+126, $posY-73.5, 0);
        $quad->setSize(262, 154);
        $quad->setStyles(Quad_Bgs1InRace::STYLE, Quad_Bgs1InRace::SUBSTYLE_BgList);

        $foregroundQuad = new Quad();
        $frame->addChild($foregroundQuad);
        $foregroundQuad->setPosition($posX+126.0, $posY-154.0, 1);
        $foregroundQuad->setSize(55, 5);
        $foregroundQuad->setStyles(Quad_Bgs1InRace::STYLE, Quad_Bgs1InRace::SUBSTYLE_BgList);

        $servernameLabel = new Label_Text();
        $frame->addChild($servernameLabel);
        $servernameLabel->setPosition($posX+126.0, $posY-153.7, 2);
        $servernameLabel->setScale(0.9);
        $servernameLabel->setSize(54, 4.5);
        $servernameLabel->setText($this->maniaControl->getClient()->getServerName());
        $servernameLabel->setTextEmboss(true);
            
        for ($i = 1; $i <= min($numberTotalParticipants, 250); $i++)
        {
            $result = $this->results[$numberTotalParticipants-$i];

            $positionQuad = new Quad();
            $frame->addChild($positionQuad);
            $positionQuad->setPosition($posX+1.0, $posY, 1);
            $positionQuad->setSize(8.8, 3.5);
            $positionQuad->setStyles(Quad_Bgs1InRace::STYLE, Quad_Bgs1InRace::SUBSTYLE_BgList);

            $positionLabel = new Label_Text();
            $frame->addChild($positionLabel);
            $positionLabel->setPosition($posX+0.8, $posY+0.3, 2);
            $positionLabel->setScale(0.7);
            $positionLabel->setSize(7.5, 0);
            $positionLabel->setText("{$result->getPosition()}");
            $positionLabel->setTextColor('0f0');
            $positionLabel->setTextEmboss(true);

            $playerQuad = new Quad();
            $frame->addChild($playerQuad);
            $playerQuad->setPosition($posX+26.0, $posY, 1);
            $playerQuad->setSize(43.0, 3.5);
            $playerQuad->setStyles(Quad_Bgs1InRace::STYLE, Quad_Bgs1InRace::SUBSTYLE_BgList);

            $playerLabel = new Label_Text();
            $frame->addChild($playerLabel);
            $playerLabel->setHorizontalAlign($playerLabel::LEFT);
            $playerLabel->setPosition($posX+6.0, $posY+0.3, 2);
            $playerLabel->setScale(0.7);
            $playerLabel->setSize(47, 3.0);
            $playerLabel->setText("{$result->getPlayerName()} \$z\$i\$n\$777({$result->getPlayerLogin()})({$result->getNbRoundsSurvived()})");
            $playerLabel->setTextEmboss(true);

            $posY -= 3.0;
            
            if ($i % 50 == 0)
            {
                $posX += 52;
                $posY += 150;
            }
        }

        $this->maniaControl->getManialinkManager()->sendManialink($manialink, $player);
    }

    public function handleManialinkAnswerHideTop30(array $callback, Player $player)
    {
        $this->hideResults($player);
        $this->displayResultsTop250($player);
    }

    public function handleManialinkAnswerHideTop250(array $callback, Player $player)
    {
        $this->hideResults($player);
    }

    private function hideResults(Player $player)
    {
        $this->maniaControl->getManialinkManager()->hideManialink(self::MLID_WIDGET, $player);
    }

    private function insertInResults(Player $player, int $position, int $nbRoundsSurvived)
    {
        array_push(
            $this->results,
            new KnockOutResult(
                $player,
                $position,
                $nbRoundsSurvived
            )
        );
    }

    public function insertPlayerInResults(Player $player, int $position, int $nbRoundsSurvived)
    {
        $this->insertInResults(
            $player,
            $position,
            $nbRoundsSurvived
        );
    }

    public function insertWinnerInResults($player, int $nbRoundsSurvived)
    {
        // there might be no winner to a KO
        if ($player === null)
            return;

        if ($player instanceof Player)
            $player = array($player);
        
        assert(is_array($player));
        foreach ($player as $p)
        {
            $this->insertInResults(
                $p,
                1,
                $nbRoundsSurvived
            );
        }
    }

    public function onCommandKOResults(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkRight(
            $player,
            $this->knockOutPlugin->getAdminsAuthenticationLevel()
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }
        
        $chatCommand = explode(' ', $chatCallback[1][2]);
        $command = array_shift($chatCommand);
        $this->displayResults($player);
    }

    private function parseResultsFilename()
    {
        $filename = (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_FILENAME
        );
        
        $editionExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(EditionExtension::class);
        if ($editionExtension != null)
            $filename = $editionExtension->replaceWildcards($filename);

        $countOpen  = substr_count($filename, "<");
        $countClose = substr_count($filename, ">");

        $incorrectDateFormat = new Exception('Name of resultsfile has a incorrectly formatted date-placeholder!');
        if ($countOpen != $countClose)
            throw $incorrectDateFormat;

        if ($countOpen > 1)
            throw new Exception('Name of resultsfile can only have one date-placeholder!');

        if ($countOpen == 1)
        {
            assert($countClose == 1);
            $indexStart = strpos($filename, "<") + 1;
            $indexEnd   = strpos($filename, ">");
            if ($indexStart >= $indexEnd)
                throw $incorrectDateFormat;

            $dateFormat = substr($filename, $indexStart, $indexEnd - $indexStart);
            $dateString = date($dateFormat);
            if (!$dateString)
                throw $incorrectDateFormat;

            $filename = str_replace("<{$dateFormat}>", $dateString, $filename);
        }

        return $filename;
    }

    public function writeResultsFile()
    {
        $filename = 'KO_results.txt';
        try
        {
            $filename = $this->parseResultsFilename();
        }
        catch (Exception $e)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                TMMUtils::formatMessage(
                    'Using default filename %s!',
                    $filename
                )
            );
        }

        if (!is_file($filename))
            file_put_contents($filename, '');

        if (!is_writable($filename))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_EXCEPTION,
                new Exception('Resultsfile is not writeable!')
            );

            return;
        }

        $resultfile = fopen($filename, 'a');
        if (!$resultfile)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_EXCEPTION,
                new Exception('Could not open resultsfile!')
            );

            return;
        }

        $numberTotalParticipants = count($this->results);
        for ($i = 1; $i <= $numberTotalParticipants; $i++)
        {
            $result = $this->results[$numberTotalParticipants - $i];
            $php_eol = PHP_EOL;
            $resultString = "{$i}. {$result->getPlayerLogin()} ({$result->getNbRoundsSurvived()} rounds survived){$php_eol}";
            if (!fwrite($resultfile, $resultString))
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_EXCEPTION,
                    new Exception('Writing into resultsfile failed!')
                );

                fclose($resultfile);
                return;
            }
        }

        fclose($resultfile);
    }
}
