<?php

namespace TMMasters\KnockOut\Extensions;

use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallbackManager;
use ManiaControl\Callbacks\Callbacks;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Players\Player;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use ManiaPlanet\DedicatedServer\Structures\GameInfos;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;
use TMMasters\TMMUtils;

class BreakState
{
    const FREE    = 'KnockOut.Extension.Break.State.Free';
    const ADDED   = 'KnockOut.Extension.Break.State.Added';
    const ONGOING = 'KnockOut.Extension.Break.State.OnGoing';
    const DONE    = 'KnockOut.Extension.Break.State.Done';
}

class BreakExtension extends Extension implements CallbackListener, CommandListener
{
    /**
     * Custom Callbacks
     */
    public const KOCB_BREAK_STATE = 'KnockOut.Extension.Break.State';

    /**
     * Commands
     */
    public const COMMAND_BREAK = array('kobreak');

    /**
     * Settings
     */
    private const SETTING_AUTOMATIC     = 'Break/Automatic';
    private const SETTING_DURATION      = 'Break/Duration [s]';
    private const SETTING_MAPFILENAME   = 'Break/Map Filename (Legacy-only)';
    private const SETTING_MESSAGE_BEGIN = 'Break/Message/Begin';
    private const SETTING_MESSAGE_END   = 'Break/Message/End';

    /**
     * Other Constants
     */
    private const SCRIPT_S_CHATTIME     = 'S_ChatTime';
    private const SCRIPT_S_ROUNDSPERMAP = 'S_RoundsPerMap';

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    private $prevChatTime   = null;
    private $prevWUDuration = null;
    private $state          = null;

    /**
     * Setting Functions
     */
    public function getAutomatic()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_AUTOMATIC
        );
    }
    public function getDuration()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_DURATION
        );
    }
    public function getMapFilename()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MAPFILENAME
        );
    }
    public function getMessageBegin()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MESSAGE_BEGIN
        );
    }
    public function getMessageEnd()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MESSAGE_END
        );
    }

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_MAP_BEGIN,
            $this,
            'handleBeginMapCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this,
            'checkAddingBreak'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            CallbackManager::CB_MP_BEGINROUND,
            $this,
            'handleBeginRoundLegacyCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::MP_PODIUMSTART,
            $this,
            'handleBeginPodiumCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            CallbackManager::CB_MP_ENDMAP,
            $this,
            'handleEndMapCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_MAP_END,
            $this,
            'handleEndMapKoCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            SettingManager::CB_SETTING_CHANGED,
            $this,
            'handleUpdateSettingsCallback'
        );

        // Command
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_BREAK,
            $this,
            'onCommandKOBreak',
            true,
            'Manually sets a break at the end of the map.'
        );
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_AUTOMATIC     => false,
                self::SETTING_DURATION      => 0,
                self::SETTING_MAPFILENAME   => '',
                self::SETTING_MESSAGE_BEGIN => '',
                self::SETTING_MESSAGE_END   => '',
            ),
            PresetManager::PRESET_DEVELOP => array(
                self::SETTING_AUTOMATIC => true,
                self::SETTING_DURATION  => 30,
            ),
            PresetManager::PRESET_TC => array(
                self::SETTING_AUTOMATIC => false,
                self::SETTING_DURATION  => 180,
            ),
            PresetManager::PRESET_TTC => array(
                self::SETTING_AUTOMATIC     => true,
                self::SETTING_DURATION      => 300,
                self::SETTING_MESSAGE_BEGIN => 'Having a small break, see the timer on the top left!',
                self::SETTING_MESSAGE_END   => 'Enjoyed the break? Back to the action!',
            ),
        );
        return $presets;
    }

    public function registerCallsOnKoLoad()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'resetState'
            )
        );
    }

    public function registerCallsOnKoUnload()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'unsetState'
            )
        );
    }

    public function unload()
    {
        $this->maniaControl->getCommandManager()->unregisterCommandListener($this);
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;

        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    public function addBreak($duration = null, $player = null)
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive())
            return;
    
        if ($duration === null)
            $duration = $this->getDuration();
        
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
        {
            // do not override, if a break is added on the same map
            if ($this->prevChatTime === null)
            {
                $this->prevChatTime = $this->maniaControl->getClient()->getModeScriptSettings()[self::SCRIPT_S_CHATTIME];
                $this->maniaControl->getClient()->setModeScriptSettings(array(
                    self::SCRIPT_S_CHATTIME => $duration
                ));
            }

            if ($player instanceof Player)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    TMMUtils::formatMessage(
                        '%s set a break of %s secs after last round on this map!',
                        $player,
                        $duration
                    )
                );
            }
            else
            {
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    TMMUtils::formatMessage(
                        'Break of %s secs after last round on this map!',
                        $duration
                    )
                );
            }
        }
        else
        {
            $mapFilename = $this->getMapFilename();
            if ($mapFilename === '')
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Cannot add break, no map to drive TA-break on!'
                );
                return false;
            }

            try
            {
                $this->prevWUDuration = $this->maniaControl->getClient()->getAllWarmUpDuration();
                $this->maniaControl->getClient()->insertMap($this->getMapFilename());
                $this->maniaControl->getClient()->setGameMode(GameInfos::GAMEMODE_TIMEATTACK);
                $this->maniaControl->getClient()->setTimeAttackLimit(1000 * $duration);
                $this->maniaControl->getClient()->setAllWarmUpDuration(0);
            }
            catch (\Exception $e)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_EXCEPTION,
                    $e
                );
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not add break!'
                );
                return false;
            }

            if ($player instanceof Player)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    TMMUtils::formatMessage(
                        '%s set a TA-break of %s secs after last round on this map!',
                        $player,
                        $duration
                    )
                );
            }
            else
            {
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    TMMUtils::formatMessage(
                        'TA-break of %s secs after last round on this map!',
                        $duration
                    )
                );
            }
        }

        $this->setState(BreakState::ADDED);
        return true;
    }

    public function checkAddingBreak()
    {
        if ($this->state !== BreakState::FREE || !$this->getAutomatic())
            return;

        $roundsPerMap          = $this->knockOutPlugin->getDataManager()->getNbRoundsMapMax();
        $nbRoundsFinishedTotal = $this->knockOutPlugin->getDataManager()->getNbRoundsTotalFinished();
        $nbRoundsRemainingMax  = $this->knockOutPlugin->getDataManager()->getNbRoundsRemainingMax();
        // look ahead by one full map, aka $roundsPerMap
        if ($nbRoundsFinishedTotal + $roundsPerMap >= $nbRoundsRemainingMax - $roundsPerMap)
            $this->addBreak();
    }

    public function handleBeginMapCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode() && $this->state === BreakState::ADDED)
            return;
        
        if ($this->state !== BreakState::ONGOING)
            return;

        $messageEnd = $this->getMessageEnd();
        if (strlen($messageEnd) > 0)
        {
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                $messageEnd
            );
        }

        $this->setState(BreakState::DONE);

        if (is_int($this->prevChatTime))
        {
            if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            {
                $this->maniaControl->getClient()->setModeScriptSettings(array(
                    self::SCRIPT_S_CHATTIME => $this->prevChatTime
                ));
            }
            else
                $this->maniaControl->getClient()->setChatTime($this->prevChatTime);

            $this->prevChatTime = null;
        }
    }

    public function handleBeginPodiumCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode())
            return;

        if ($this->state === BreakState::ADDED)
        {
            $messageBegin = $this->getMessageBegin();
            if (strlen($messageBegin) > 0)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    $messageBegin
                );
            }

            $this->setState(BreakState::ONGOING);
        }
    }

    public function handleBeginRoundLegacyCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive())
            return;

        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if ($this->state !== BreakState::ADDED)
            return;
        
        // restore settings for after break
        try
        {
            $this->maniaControl->getClient()->setGameMode(GameInfos::GAMEMODE_ROUNDS);
            if (is_int($this->prevWUDuration))
            {
                $this->maniaControl->getClient()->setAllWarmUpDuration($this->prevWUDuration);
                $this->prevWUDuration = null;
            }
            $this->maniaControl->getClient()->removeMap($this->getMapFilename());
        }
        catch (\Exception $e)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                'Could not restore settings for after TA-break!'
            );
            return false;
        }

        $this->setState(BreakState::ONGOING);
    }

    public function handleEndMapCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive())
            return;
        
        if (!$this->knockOutPlugin->getDataManager()->isPaused())
            return;
        
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            return;
        
        if ($this->state !== BreakState::ONGOING)
            return;
        
        $this->knockOutPlugin->getDataManager()->unpause();

        $messageEnd = $this->getMessageEnd();
        if (strlen($messageEnd) > 0)
        {
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                $messageEnd
            );
        }
        $this->setState(BreakState::DONE);
    }

    public function handleEndMapKoCallback()
    {
        if (!$this->knockOutPlugin->getDataManager()->isScriptMode() && $this->state === BreakState::ADDED)
        {
            $this->knockOutPlugin->getDataManager()->pause();
            $messageBegin = $this->getMessageBegin();
            if (strlen($messageBegin) > 0)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::INFORMATION,
                    $messageBegin
                );
            }
        }
    }

    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this->knockOutPlugin))
            return;

        if ($setting->setting !== self::SETTING_MAPFILENAME)
            return;
        
        if ($setting->value === '')
            return;
         
        $filepath = $this->maniaControl->getServer()->getDirectory()->getMapsFolder() . $setting->value;
        if (!file_exists($filepath))
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                TMMUtils::formatMessage(
                    'Break-Mapfile %s does not exist!',
                    $setting->value
                )
            );
        }
    }

    public function onCommandKOBreak(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkRight(
            $player,
            $this->knockOutPlugin->getAdminsAuthenticationLevel()
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }

        if (!$this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            $this->knockOutPlugin->chat(
                ChatMode::ERROR,
                'Cannot set break, no KO-Match live!'
            );
            return;
        }
        
        $chatCommand = explode(' ', $chatCallback[1][2]);
        $command = array_shift($chatCommand);
        assert(in_array($command, self::COMMAND_BREAK));
        
        $duration = null;
        if (count($chatCommand))
        {
            $durationStr = array_shift($chatCommand);
            if (!empty($durationStr) && is_numeric($durationStr))
                $duration = (int) $durationStr;
        }

        $this->addBreak($duration, $player);
    }

    public function resetState()
    {
        $this->setState(BreakState::FREE);
    }

    private function setState($breakState)
    {
        $this->state = $breakState;

        if ($this->state !== BreakState::FREE)
        {
            $this->maniaControl->getCallbackManager()->triggerCallback(
                self::KOCB_BREAK_STATE,
                $this->state
            );
        }
    }

    public function unsetState()
    {
        $this->state = null;
    }
}
