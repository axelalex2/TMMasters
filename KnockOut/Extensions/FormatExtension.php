<?php

namespace TMMasters\KnockOut\Extensions;

use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallQueueListener;
use ManiaControl\Callbacks\TimerListener;
use ManiaControl\ManiaControl;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use ManiaPlanet\DedicatedServer\Structures\GameInfos;
use ManiaPlanet\DedicatedServer\Xmlrpc\ChangeInProgressException;
use ManiaPlanet\DedicatedServer\Xmlrpc\FaultException;
use TMMasters\ChatMode;
use TMMasters\KnockOut\DataManager;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;
use TMMasters\TMMUtils;

class PreviousScriptSettings
{
    private $scriptName        = '';
    private $scriptSettings    = array();
    private $scriptText        = '';

    public function __construct(ManiaControl $maniaControl)
    {
        $this->scriptName     = $maniaControl->getClient()->getScriptName()['CurrentValue'];
        $this->scriptSettings = $maniaControl->getClient()->getModeScriptSettings();
        $this->scriptText     = $maniaControl->getClient()->getModeScriptText();
    }

    public function getScriptName    () { return $this->scriptName    ; }
    public function getScriptSettings() { return $this->scriptSettings; }
    public function getScriptText    () { return $this->scriptText    ; }
}

class FormatExtension extends Extension implements CallbackListener, CallQueueListener, TimerListener
{
    /**
     * Cache
     */
    public const CACHE_NBLIVES = 'KnockOut.Player.NbLives';

    /**
     * Custom Callbacks
     */
    public const KOCB_PLAYER_KILL = 'KnockOut.Extension.Format.PlayerKill';

    /**
     * Settings
     */
    // we can't make use of S_MapsPerMatch, since it would skip over S_ChatTime, which would kill the break
    // private const AUTOSETTING_SCRIPT_S_MAPSPERMATCH = 'S_MapsPerMatch';
    private const AUTOSETTING_SCRIPT_S_POINTSLIMIT  = 'S_PointsLimit';
    private const AUTOSETTING_SCRIPT_S_ROUNDSPERMAP = 'S_RoundsPerMap';
    private const AUTOSETTING_SCRIPT_S_USETIEBREAK  = 'S_UseTieBreak';

    private const SETTING_KO_MULTIPLIER             = 'Format/KO Multiplier';
    private const SETTING_MULTIKNOCKOUTPLAYERLIMITS = 'Format/Multi-KOs Player Limits';
    private const SETTING_PLAYERNBLIVES             = 'Format/Player Nb Lives';
    private const SETTING_POINTSYSTEM               = 'Format/Pointsystem';
    private const SETTING_SCRIPT_LOADDELAY          = 'Format/Script/Load Delay [ms]';
    private const SETTING_SCRIPT_S_ALLOWRESPAWN     = 'Format/Script/S_AllowRespawn';
    private const SETTING_SCRIPT_S_CHATTIME         = 'Format/Script/S_ChatTime';
    private const SETTING_SCRIPT_S_FINISHTIMEOUT    = 'Format/Script/S_FinishTimeout';
    private const SETTING_SCRIPT_S_ROUNDSPERMAP     = 'Format/Script/' . self::AUTOSETTING_SCRIPT_S_ROUNDSPERMAP;

    private const POINTSYSTEM_LINEARPOSITION = 'Linear Position';
    private const POINTSYSTEM_ROUNDSSURVIVED = 'Rounds survived';
    private const POINTSYSTEMS = array(
        self::POINTSYSTEM_LINEARPOSITION,
        self::POINTSYSTEM_ROUNDSSURVIVED,
    );

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;
    
    private $multiKoPlayerLimits  = array();
    private $prevGamemodeSettings = null;

    /**
     * Setting Functions
     */
    public function getKOMultiplier()
    {
        return max(1, (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_KO_MULTIPLIER
        ) );
    }
    public function getPlayerNbLives()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_PLAYERNBLIVES
        );
    }
    public function getPointsystem()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_POINTSYSTEM
        );
    }
    public function getScriptLoadDelay()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SCRIPT_LOADDELAY
        );
    }
    public function getScriptSAllowRespawn()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SCRIPT_S_ALLOWRESPAWN
        );
    }
    public function getScriptSChatTime()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SCRIPT_S_CHATTIME
        );
    }
    public function getScriptSFinishTimeout()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SCRIPT_S_FINISHTIMEOUT
        );
    }
    public function getScriptSRoundsPerMap()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SCRIPT_S_ROUNDSPERMAP
        );
    }

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            self::KOCB_PLAYER_KILL,
            $this,
            'handlePlayerKillCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_DC,
            $this,
            'handlePlayerDisconnectCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            SettingManager::CB_SETTING_CHANGED,
            $this,
            'handleUpdateSettingsCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this,
            'legacySkipMap'
        );
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this,
            'updateScriptPointsystem'
        );
    }
    
    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_KO_MULTIPLIER             => 1,
                self::SETTING_MULTIKNOCKOUTPLAYERLIMITS => '',
                self::SETTING_PLAYERNBLIVES             => 1,
                self::SETTING_POINTSYSTEM               => self::POINTSYSTEMS,
                self::SETTING_SCRIPT_LOADDELAY          => 5000,
                self::SETTING_SCRIPT_S_ALLOWRESPAWN     => true,
                self::SETTING_SCRIPT_S_CHATTIME         => 10,
                self::SETTING_SCRIPT_S_FINISHTIMEOUT    => 20,
                self::SETTING_SCRIPT_S_ROUNDSPERMAP     => 5,
            ),
            PresetManager::PRESET_DEVELOP => array(
                self::SETTING_SCRIPT_LOADDELAY      => 500,
                self::SETTING_SCRIPT_S_ROUNDSPERMAP => 2,
            ),
            PresetManager::PRESET_SPECIAL => array(
                self::SETTING_MULTIKNOCKOUTPLAYERLIMITS => '5,10,20,30,40,50,60,70,85,100,125,150,175,200',
                self::SETTING_SCRIPT_S_FINISHTIMEOUT    => 60,
            ),
            PresetManager::PRESET_TC => array(
                self::SETTING_KO_MULTIPLIER          => 3,
                self::SETTING_PLAYERNBLIVES          => 3,
                self::SETTING_SCRIPT_S_ALLOWRESPAWN  => false,
                self::SETTING_SCRIPT_S_FINISHTIMEOUT => 15,
                self::SETTING_SCRIPT_S_ROUNDSPERMAP  => 7,
            ),
            PresetManager::PRESET_THROWBACK => array(
                self::SETTING_MULTIKNOCKOUTPLAYERLIMITS => '20,60,120,200',
                self::SETTING_SCRIPT_S_FINISHTIMEOUT    => 30,
            ),
            PresetManager::PRESET_TTC3 => array(
                self::SETTING_MULTIKNOCKOUTPLAYERLIMITS => '10,30,60,100,150,210',
                self::SETTING_SCRIPT_S_FINISHTIMEOUT    => 45,
            ),
            PresetManager::PRESET_TTC4 => array(
                self::SETTING_MULTIKNOCKOUTPLAYERLIMITS => '15,45,90,150,225',
                self::SETTING_SCRIPT_S_FINISHTIMEOUT    => 30,
            ),
            PresetManager::PRESET_TTC5 => array(
                self::SETTING_MULTIKNOCKOUTPLAYERLIMITS => '20,60,120,200',
            ),
        );
        return $presets;
    }

    public function registerCallsOnKoLoad()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'savePrevGamemodeSettings',
                'loadPlayerNbLives',
                'loadRoundsMode',
                'updatePointsystem',
                'parseMultiKoPlayerLimits',
            )
        );
    }

    public function registerCallsOnKoUnload()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'loadPrevGamemodeSettings',
                'unloadPlayerNbLives',
            )
        );
    }

    public function unload()
    {
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;

        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    /**
     * @return array($nbKnockouts @ $nbPlayers to apply, until $playerLimit)
     */
    public function getNextMultiKOPlayerLimit($nbPlayers = null)
    {
        if ($nbPlayers === null)
            $nbPlayers = $this->knockOutPlugin->getDataManager()->getNbPlayersCurrent();
        
        $max = count($this->multiKoPlayerLimits);
        for ($i = $max-1; $i >= 0; $i--)
        {
            $playerLimit = $this->multiKoPlayerLimits[$i];
            if ($nbPlayers > $playerLimit)
                return array(($i+2)*$this->getKOMultiplier(), $playerLimit);
        }

        return array($this->getKOMultiplier(), 1);
    }

    public function handlePlayerKillCallback(Player $player)
    {
        $nbLives = $player->getCache($this, self::CACHE_NBLIVES);
        assert($nbLives !== null);

        $nbLives--;
        $player->setCache(
            $this,
            self::CACHE_NBLIVES,
            $nbLives
        );

        if ($nbLives > 0)
        {
            // still alive
            $this->knockOutPlugin->chat(
                ChatMode::INFORMATION,
                TMMUtils::formatMessage(
                    '%s lost a life, has now %s left!',
                    $player,
                    $nbLives
                )
            );
        }
        elseif ($nbLives === 0)
        {
            $nbRoundsTotalFinished = $this->knockOutPlugin->getDataManager()->getNbRoundsTotalFinished();
            $this->maniaControl->getCallQueueManager()->registerListening(
                $this,
                function () use ($player, $nbRoundsTotalFinished) {        
                    $nbPlayersCurrent = $this->knockOutPlugin->getDataManager()->getNbPlayersCurrent();
                    $this->maniaControl->getCallbackManager()->triggerCallback(
                        KnockOutCallbacks::KO_PLAYER_KO,
                        $player,
                        $nbPlayersCurrent,
                        $nbRoundsTotalFinished
                    );
                }
            );
        }
    }

    public function handlePlayerDisconnectCallback(Player $player)
    {
        $nbLives = $player->getCache(
            $this,
            self::CACHE_NBLIVES
        );
        if ($nbLives >= 1)
        {
            // only KO the player, if he has lives left
            $player->setCache(
                $this,
                self::CACHE_NBLIVES,
                1
            );
            $this->knockOutPlugin->knockoutPlayer($player);
        }
    }

    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this->knockOutPlugin))
            return;

        if ($this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            if (strpos($setting->setting, 'Script/S_') !== false)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Changing script settings in the plugin has no effect, when the KO is live, use "Script Settings" instead!'
                );
            }
        }

        if ($setting->setting != self::SETTING_MULTIKNOCKOUTPLAYERLIMITS)
            return;
        
        $this->parseMultiKoPlayerLimits();

        if ($this->knockOutPlugin->getDataManager()->isMatchLive())
            $this->updateScriptPointsystem();
    }

    public function legacySkipMap(int $nbRoundTotal, int $nbRoundMap)
    {
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            return;
        
        $nbRoundsMapMax = $this->knockOutPlugin->getDataManager()->getNbRoundsMapMax();
        if ($this->knockOutPlugin->getAdminsDebug())
            $nbRoundsMapMax = $this->getScriptSRoundsPerMap();
        
        if ($nbRoundMap > $nbRoundsMapMax)
        {
            try
            {
                $this->knockOutPlugin->getDataManager()->preventKO();
                $this->maniaControl->getClient()->nextMap();
            }
            catch (\Exception $e)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_EXCEPTION,
                    $e
                );
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not skip map automatically!'
                );
            }
        }
    }

    public function loadPlayerNbLives()
    {
        $players = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($players as $player)
        {
            $player->setCache(
                $this,
                self::CACHE_NBLIVES,
                $this->getPlayerNbLives()
            );
        }
    }

    public function loadPrevGamemodeSettings()
    {
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Restoring previous gamemode settings ...'
        );
        
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
        {
            TMMUtils::loadScript(
                $this->maniaControl,
                $this->getScriptLoadDelay(),
                $this->prevGamemodeSettings->getScriptName(),
                $this->prevGamemodeSettings->getScriptSettings(),
                $this->prevGamemodeSettings->getScriptText()
            );
        }
        else
        {
            try
            {
                $this->maniaControl->getClient()->setGameInfos($this->prevGamemodeSettings);
        
                $this->maniaControl->getTimerManager()->registerOneTimeListening(
                    $this,
                    function () {
                        try
                        {
                            $this->maniaControl->getClient()->nextMap();
                        }
                        catch (ChangeInProgressException $e)
                        { }
                        catch (\Exception $e)
                        {
                            $this->knockOutPlugin->chat(
                                ChatMode::ADMIN_EXCEPTION,
                                $e
                            );
                            $this->knockOutPlugin->chat(
                                ChatMode::ADMIN_ERROR,
                                'Could not skip Map!'
                            );
                        }
                    },
                    $this->getScriptLoadDelay()
                );
            }
            catch (\Exception $e)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_EXCEPTION,
                    $e
                );
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Will stay in current mode!'
                );
            }
        }

        $this->prevGamemodeSettings = null;
    }

    public function loadRoundsMode()
    {
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Loading Rounds-Mode ...'
        );

        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
        {
            $loadedScriptSettings = $this->knockOutPlugin->getPluginSettingsToScriptSettings(array(
                self::AUTOSETTING_SCRIPT_S_POINTSLIMIT => 0,
                self::AUTOSETTING_SCRIPT_S_USETIEBREAK => false,

                self::SETTING_SCRIPT_S_ALLOWRESPAWN  => $this->getScriptSAllowRespawn(),
                self::SETTING_SCRIPT_S_CHATTIME      => $this->getScriptSChatTime(),
                self::SETTING_SCRIPT_S_FINISHTIMEOUT => $this->getScriptSFinishTimeout(),
                self::SETTING_SCRIPT_S_ROUNDSPERMAP  => $this->getScriptSRoundsPerMap(),
            ));
            
            $scriptName = 'Rounds.Script.txt';
            TMMUtils::loadScript(
                $this->maniaControl,
                $this->getScriptLoadDelay(),
                $scriptName,
                $loadedScriptSettings
            );
        }
        else
        {
            try
            {
                $this->maniaControl->getClient()->setGameMode(GameInfos::GAMEMODE_ROUNDS);
                $this->maniaControl->getClient()->setChatTime(1000 * $this->getScriptSChatTime());
                $this->maniaControl->getClient()->setFinishTimeout(1000 * $this->getScriptSFinishTimeout());
                $this->maniaControl->getClient()->setDisableRespawn(! $this->getScriptSAllowRespawn());
                $this->maniaControl->getClient()->setRoundPointsLimit(PHP_INT_MAX);
        
                $this->maniaControl->getTimerManager()->registerOneTimeListening(
                    $this,
                    function () {
                        try
                        {
                            $this->maniaControl->getClient()->nextMap();
                        }
                        catch (\Exception $e)
                        {
                            $this->knockOutPlugin->chat(
                                ChatMode::ADMIN_EXCEPTION,
                                $e
                            );
                            $this->knockOutPlugin->chat(
                                ChatMode::ADMIN_ERROR,
                                'Could not skip Map!'
                            );
                        }
                    },
                    $this->getScriptLoadDelay()
                );
            }
            catch (\Exception $e)
            {
                // $e can somehow be a string, very weird error sometimes
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_EXCEPTION,
                    $e
                );
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not load Rounds-Mode!'
                );
            }
        }
    }

    /**
     * Parses the Multi-KO-Settings into an int-array to be actually useable.
     */
    public function parseMultiKoPlayerLimits()
    {
        $this->multiKoPlayerLimits = array();

        $multiKoPlayerLimits = (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_MULTIKNOCKOUTPLAYERLIMITS
        );
        if ($multiKoPlayerLimits === '')
            return;
        
        $multiKoPlayerLimits = explode(',', $multiKoPlayerLimits);
        foreach ($multiKoPlayerLimits as $i => $limit)
        {
            $limit = trim($limit);
            if (!is_numeric($limit))
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Check Multi-KO-Settings, elements need to be numeric, using single KO instead!'
                );
                return;
            }

            // force to int
            $limit = $limit + 0;
            if (!is_int($limit))
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Check Multi-KO-Settings, elements need to be integer, using single KO instead!'
                );
                return;
            }

            $multiKoPlayerLimits[$i] = $limit;
        }

        for ($i = 1; $i < count($multiKoPlayerLimits); $i++)
        {
            if ($multiKoPlayerLimits[$i] <= $multiKoPlayerLimits[$i-1])
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Check Multi-KO-Settings, elements need to ascend each time, using single KO instead!'
                );
                return;
            }
        }

        $this->multiKoPlayerLimits = $multiKoPlayerLimits;
    }

    public function savePrevGamemodeSettings()
    {
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Saving previous gamemode settings ...'
        );

        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            $this->prevGamemodeSettings = new PreviousScriptSettings($this->maniaControl);
        else
        {
            try
            {
                $this->prevGamemodeSettings = $this->maniaControl->getClient()->getCurrentGameInfo();
            }
            catch (FaultException $e)
            {
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_EXCEPTION,
                    $e
                );
                $this->knockOutPlugin->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not save previous gamemode settings!'
                );
            }
        }
    }

    public function test()
    {
        $result = true;
        $koMultiplier = $this->getKOMultiplier();

        $this->multiKoPlayerLimits = array(20, 60, 120, 200);
        $result &= $this->getNextMultiKOPlayerLimit(201) == array(5*$koMultiplier, 200);
        $result &= $this->getNextMultiKOPlayerLimit(200) == array(4*$koMultiplier, 120);
        $result &= $this->getNextMultiKOPlayerLimit(121) == array(4*$koMultiplier, 120);
        $result &= $this->getNextMultiKOPlayerLimit(120) == array(3*$koMultiplier,  60);
        $result &= $this->getNextMultiKOPlayerLimit( 61) == array(3*$koMultiplier,  60);
        $result &= $this->getNextMultiKOPlayerLimit( 60) == array(2*$koMultiplier,  20);
        $result &= $this->getNextMultiKOPlayerLimit( 21) == array(2*$koMultiplier,  20);
        $result &= $this->getNextMultiKOPlayerLimit( 20) == array(1*$koMultiplier,   1);
        $this->multiKoPlayerLimits = array();

        return $result;
    }

    public function unloadPlayerNbLives()
    {
        $players = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($players as $player)
        {
            $player->destroyCache(
                $this,
                self::CACHE_NBLIVES
            );
        }
    }

    public function updateLegacyPointsystem()
    {
        $dataManager = $this->knockOutPlugin->getDataManager();
        if ($this->dataManager->isScriptMode())
            return;

        $success = false;
        // we need to relax the constraints to set the scores
        $relaxConstraints = true;
        // to set as many point positions as possible, first, create the full pointsystem
        $pointSystem = array();
        for ($points = DataManager::MAX_PLAYERS; $points >= 1; $points--)
            array_push($pointSystem, $points);

        // second try to set point positions, reduce if unsuccessful
        while (!empty($pointSystem))
        {
            try
            {
                $this->maniaControl->getClient()->setRoundCustomPoints($pointSystem, $relaxConstraints);
                $success = true;
                break;
            }
            catch (\Exception $e)
            { }

            array_pop($pointSystem);
        }

        if (!$success)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                'Could not load Pointsystem!'
            );
        }

        return $success;
    }

    public function updatePointsystem()
    {
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Loading Pointsystem ...'
        );
        
        if ($this->knockOutPlugin->getDataManager()->isScriptMode())
            $this->updateScriptPointsystem();
        else
            $this->updateLegacyPointsystem();
    }

    public function updateScriptPointsystem()
    {
        $dataManager = $this->knockOutPlugin->getDataManager();
        if (!$dataManager->isScriptMode())
            return;
        
        $pointSystem = $this->getPointsystem();
        switch ($pointSystem)
        {
            case self::POINTSYSTEM_ROUNDSSURVIVED:
                $nbKnockouts = $dataManager->getNbKnockouts();
                $nbPlayersCurrent = $dataManager->getNbPlayersCurrent();
                if ($nbKnockouts >= $nbPlayersCurrent)
                {
                    // TODO move somewhere else
                    $this->knockOutPlugin->chat(
                        ChatMode::INFORMATION,
                        'Number of KOs exceeds number of players, initiating final round ...'
                    );

                    $nbKnockouts = max($nbPlayersCurrent-1, 0);
                }

                $nbSurvivors = $nbPlayersCurrent - $nbKnockouts;
                if (!$this->knockOutPlugin->getAdminsDebug())
                    assert($nbSurvivors > 0 && $nbKnockouts > 0);
                
                if ($nbSurvivors < 0 || $nbKnockouts < 0)
                    return;

                $pointsRepartition = array_merge(
                    array_fill(0, $nbSurvivors, '1'),
                    array_fill(0, $nbKnockouts, '0')
                );
                
                $this->maniaControl->getModeScriptEventManager()->setTrackmaniaPointsRepartition($pointsRepartition);
            break;
            case self::POINTSYSTEM_LINEARPOSITION:
                $pointsRepartition = array();
                for ($points = DataManager::MAX_PLAYERS; $points >= 1; $points--)
                    array_push($pointsRepartition, strval($points));
                
                $this->maniaControl->getModeScriptEventManager()->setTrackmaniaPointsRepartition($pointsRepartition);
            break;
        }
        
        // only change S_RoundsPerMap if players started with only 1 life
        if ($this->getPlayerNbLives() === 1)
        {
            $nbFinalRounds = $dataManager->getNbRoundsRemainingMax() + $dataManager->getNbRoundsMapFinished();
            $roundsPerMap = min($dataManager->getNbRoundsMapMax(), $nbFinalRounds);
            if ($roundsPerMap < 1)
                $roundsPerMap = 1;
            
            $this->maniaControl->getClient()->setModeScriptSettings(array(
                self::AUTOSETTING_SCRIPT_S_ROUNDSPERMAP => $roundsPerMap,
            ));
        }
    }
}
