<?php

namespace TMMasters\KnockOut\Extensions;

use ManiaControl\Callbacks\Callbacks;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use ManiaControl\ManiaControl;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;
use TMMasters\ChatMode;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\Extensions\EditionExtension;
use TMMasters\KnockOutPlugin;
use TMMasters\KnockOut\PresetManager;

class KnockOutPreviousSettings
{
    private $password          = '';
    private $passwordSpectator = '';

    public function __construct(ManiaControl $maniaControl)
    {
        $this->password          = $maniaControl->getClient()->getServerPassword();
        $this->passwordSpectator = $maniaControl->getClient()->getServerPasswordForSpectator();
    }

    public function getPassword         () { return $this->password         ; }
    public function getPasswordSpectator() { return $this->passwordSpectator; }
}

class ServerExtension extends Extension implements CallbackListener
{
    /**
     * Settings
     */
    private const SETTING_NAME                      = 'Server/Name';
    private const SETTING_PASSWORD_PLAYER           = 'Server/Password/Player';
    private const SETTING_PASSWORD_SPECTATOR        = 'Server/Password/Spectator';
    private const SETTING_RESTORE_PREVIOUS          = 'Server/Restore previous settings';
    private const SETTING_SLOTS_LOBBY_MAXPLAYERS    = 'Server/Slots/Lobby/Max Players';
    private const SETTING_SLOTS_LOBBY_MAXSPECTATORS = 'Server/Slots/Lobby/Max Spectators';
    private const SETTING_SLOTS_MAXSPECTATORS       = 'Server/Slots/Max Spectators';

    /**
     * Private Properties
     */
    private $knockOutPlugin = null;
    private $maniaControl   = null;

    private $prevSettings = null;
    
    /**
     * Setting Functions
     */
    public function getPasswordPlayer()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_PASSWORD_PLAYER
        );
    }
    public function getPasswordSpectator()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_PASSWORD_SPECTATOR
        );
    }
    public function getRestorePrevious()
    {
        return (boolean) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_RESTORE_PREVIOUS
        );
    }
    public function getSlotsLobbyMaxPlayers()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SLOTS_LOBBY_MAXPLAYERS
        );
    }
    public function getSlotsLobbyMaxSpectators()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SLOTS_LOBBY_MAXSPECTATORS
        );
    }
    public function getSlotsMaxSpectators()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_SLOTS_MAXSPECTATORS
        );
    }

    /**
     * Extension Functions
     */
    public function __construct(KnockOutPlugin $plugin)
    {
        $this->knockOutPlugin = $plugin;
        $this->maniaControl = $this->knockOutPlugin->getManiaControl();

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            EditionExtension::KOCB_EDITION_UPDATE,
            $this,
            'handleEditionUpdateCallback'
        );

        // use original Callback as we do different things depending on if we are live or not
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            Callbacks::ENDMAP,
            $this,
            'updateSlots'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            SettingManager::CB_SETTING_CHANGED,
            $this,
            'handleUpdateSettingsCallback'
        );
    }

    public function getOptionalDependencies()
    {
        static $optionalDependencies = array(
            EditionExtension::class,
        );
        return $optionalDependencies;
    }

    public function getPresets()
    {
        static $presets = array(
            PresetManager::PRESET_DEFAULT => array(
                self::SETTING_NAME                      => '',
                self::SETTING_PASSWORD_PLAYER           => '',
                self::SETTING_PASSWORD_SPECTATOR        => '',
                self::SETTING_RESTORE_PREVIOUS          => true,
                self::SETTING_SLOTS_LOBBY_MAXPLAYERS    => 180,
                self::SETTING_SLOTS_LOBBY_MAXSPECTATORS => 0,
                self::SETTING_SLOTS_MAXSPECTATORS       => 20,
            ),
            PresetManager::PRESET_DEVELOP => array(
                self::SETTING_SLOTS_LOBBY_MAXPLAYERS    => 10,
                self::SETTING_SLOTS_LOBBY_MAXSPECTATORS => 5,
                self::SETTING_SLOTS_MAXSPECTATORS       => 15,
            ),
            PresetManager::PRESET_TC => array(
                self::SETTING_SLOTS_LOBBY_MAXPLAYERS    => 47,
                self::SETTING_SLOTS_LOBBY_MAXSPECTATORS => 10,
                self::SETTING_SLOTS_MAXSPECTATORS       => 20,
            ),
            PresetManager::PRESET_TTC => array(
                self::SETTING_NAME               => '$i$f00TM Masters $06fTTC <style> #<number> !',
                self::SETTING_PASSWORD_PLAYER    => 'nutelllla',
                self::SETTING_PASSWORD_SPECTATOR => 'spamonly',
            ),
        );
        return $presets;
    }

    public function registerCallsOnKoLoad()
    {
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'savePrevSettings',
                'lockServer',
            )
        );
    }

    public function registerCallsOnKoUnload()
    {
        $calls = array(
            'deletePrevSettings'
        );
        if ($this->getRestorePrevious())
        {
            $calls = array_merge(
                array(
                    'resetSlots',
                    'restorePasswords',
                ),
                $calls
            );
        }

        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            $calls
        );
    }

    public function unload()
    {
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;

        $this->knockOutPlugin = null;
    }

    /**
     * Main Functionality
     */
    public function deletePrevSettings()
    {
        $this->prevSettings = null;
    }

    public function handleEditionUpdateCallback()
    {
        if (!$this->maniaControl->getCallQueueManager()->hasListening($this, 'updateName'))
            $this->maniaControl->getCallQueueManager()->registerListening($this, 'updateName');
    }
    
    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this->knockOutPlugin))
            return;
        
        if ($setting->setting === self::SETTING_NAME)
            $this->handleEditionUpdateCallback();

        if ($setting->setting === self::SETTING_SLOTS_LOBBY_MAXPLAYERS || $setting->setting === self::SETTING_SLOTS_LOBBY_MAXSPECTATORS)
            $this->updateSlots();
    }

    public function lockServer()
    {
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Locking server ...'
        );

        $this->maniaControl->getClient()->setServerPassword(
            $this->getPasswordPlayer()
        );
        $this->maniaControl->getClient()->setServerPasswordForSpectator(
            $this->getPasswordSpectator()
        );
    }

    public function resetSlots()
    {
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Resetting Slots ...'
        );

        $this->maniaControl->getClient()->setMaxPlayers(
            $this->getSlotsLobbyMaxPlayers()
        );
        $this->maniaControl->getClient()->setMaxSpectators(
            $this->getSlotsLobbyMaxSpectators()
        );
    }

    public function restorePasswords()
    {
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Restoring old password ...'
        );

        $this->maniaControl->getClient()->setServerPassword(
            $this->prevSettings->getPassword()
        );
        $this->maniaControl->getClient()->setServerPasswordForSpectator(
            $this->prevSettings->getPasswordSpectator()
        );
    }

    public function savePrevSettings()
    {
        $this->knockOutPlugin->chat(
            ChatMode::INFORMATION,
            'Saving current server settings ...'
        );
        $this->prevSettings = new KnockOutPreviousSettings($this->maniaControl);
    }

    public function updateName()
    {
        $name = (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this->knockOutPlugin,
            self::SETTING_NAME
        );
        if (strlen($name) <= 0)
            return;
        
        $editionExtension = $this->knockOutPlugin->getExtensionManager()->getExtension(EditionExtension::class);
        if ($editionExtension != null)
            $name = $editionExtension->replaceWildcards($name);
        
        try
        {
		    $this->maniaControl->getClient()->setServerName($name);
        }
        catch (Exception $e)
        {
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->knockOutPlugin->chat(
                ChatMode::ADMIN_ERROR,
                'Could not update server name automatically!'
            );
        }
    }

    public function updateSlots()
    {
        if (!$this->knockOutPlugin->getDataManager()->isMatchLive())
        {
            $this->maniaControl->getClient()->setMaxPlayers(
                $this->getSlotsLobbyMaxPlayers()
            );
            $this->maniaControl->getClient()->setMaxSpectators(
                $this->getSlotsLobbyMaxSpectators()
            );
            
            return;
        }

        // update Slot limits
        $this->maniaControl->getClient()->setMaxPlayers(
            0
        );

        $nbPlayersCurrent = $this->knockOutPlugin->getDataManager()->getNbPlayersCurrent();
        $this->maniaControl->getClient()->setMaxSpectators(
            // 255 is the technical maximum amount of spectator slots
            min(
                255 - $nbPlayersCurrent,
                max(0, $this->getSlotsMaxSpectators()) // safety to stay above 0
            )
        );
    }
}
