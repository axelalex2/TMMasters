<?php

namespace TMMasters\KnockOut;

class KnockOutCallbacks
{
    //
    public const KO_BEGIN = 'KnockOut.Begin';

    // Player|array|null $winner, int $nbRoundsTotal
    public const KO_END = 'KnockOut.End';

    //
    public const KO_MANUAL_SKIP = 'KnockOut.Manual.Skip';

    // int $nbMap
    public const KO_MAP_BEGIN = 'KnockOut.Map.Begin';

    // int $nbMap
    public const KO_MAP_END = 'KnockOut.Map.End';

    // Player $player
    public const KO_PLAYER_DC = 'KnockOut.Player.Disconnect';

    // Player $player, int $time
    public const KO_PLAYER_FINISH = 'KnockOut.Player.Finish';

    // Player $player
    public const KO_PLAYER_JOIN = 'KnockOut.Player.Join';

    // Player $player, int $position, int $nbRoundsSurvived
    public const KO_PLAYER_KO = 'KnockOut.Player.KnockOut';

    // Player $player
    public const KO_PLAYER_RETIRE = 'KnockOut.Player.Retire';

    // int $nbRoundTotal, int $nbRoundMap
    public const KO_ROUND_BEGIN = 'KnockOut.Round.Begin';

    // int $nbRoundTotal, int $nbRoundMap
    public const KO_ROUND_END = 'KnockOut.Round.End';

    // int $targetRoundPoints
    public const KO_ROUND_KO_LEGACY = 'KnockOut.Round.KnockOut.Legacy';

    // OnScoresStructure $structure
    public const KO_ROUND_KO_SCRIPT = 'KnockOut.Round.KnockOut.Script';

    //
    public const KO_WARMUP_BEGIN = 'KnockOut.WarmUp.Begin';

    //
    public const KO_WARMUP_END = 'KnockOut.WarmUp.End';

    // int $nbWarmUpRound
    public const KO_WARMUP_ROUND_BEGIN = 'KnockOut.WarmUp.Round.Begin';

    // int $nbWarmUpRound
    public const KO_WARMUP_ROUND_END = 'KnockOut.WarmUp.Round.End';
}
