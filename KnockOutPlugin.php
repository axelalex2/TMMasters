<?php

namespace TMMasters;

use FML\ManiaLink;
use FML\Controls\Frame;
use FML\Controls\Labels\Label_Text;
use FML\Script\Features\Paging;
use ManiaControl\Admin\AuthenticationManager;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallQueueListener;
use ManiaControl\Callbacks\Structures\TrackMania\OnScoresStructure;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Logger;
use ManiaControl\ManiaControl;
use ManiaControl\Manialinks\ManialinkManager;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerActions;
use ManiaControl\Plugins\Plugin;
use ManiaControl\Plugins\PluginMenu;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;
use TMMasters\KnockOut\DataManager;
use TMMasters\KnockOut\Extension;
use TMMasters\KnockOut\ExtensionManager;
use TMMasters\KnockOut\Extensions\FormatExtension;
use TMMasters\KnockOut\Extensions\PlayerExtension;
use TMMasters\KnockOut\KnockOutCallbackManager;
use TMMasters\KnockOut\KnockOutCallbacks;
use TMMasters\KnockOut\PresetManager;

class KnockOutPlugin implements CallbackListener, CallQueueListener, CommandListener, Plugin
{
    /**
     * Constants
     */
    private const PLUGIN_ID      = 999;
    private const PLUGIN_VERSION = 20.05;
    private const PLUGIN_NAME    = 'KnockOutPlugin';
    private const PLUGIN_AUTHOR  = 'axelalex2, hbarney, jonthekiller';

    /**
     * Commands
     */
    private const COMMAND_DUMP        = array('kodump');
    private const COMMAND_MAP_SKIP    = array('koskip', 'koskipmap');
    private const COMMAND_MATCH_BEGIN = array('kobegin', 'kostart');
    private const COMMAND_MATCH_END   = array('koend', 'kostop');
    private const COMMAND_PLAYER_KO   = array('kologin', 'koplayer');
    private const COMMAND_ROUND_END   = array('koendround');
    private const COMMAND_SETTINGS    = array('kosetting', 'kosettings');
    private const COMMAND_TEST        = array('kotest');

    private const COMMANDS_TO_DISABLE_PERMANENTLY = array(
        'delrec',
        'xlist',
        'xmaps',
    );
    private const ADMINCOMMANDS_TO_DISABLE_PERMANENTLY = array(
        'addbot',
        'addbots',
        'autoteambalance',
        'balance',
        'forceblue',
        'forcered',
        'removebot',
        'removebots',
        'teambalance',
    );
    private const COMMANDS_TO_DISABLE_LIVE = array();
    private const ADMINCOMMANDS_TO_DISABLE_LIVE = array(
        'checkpluginsupdate',
        'checkupdate',
        'coreupdate',
        'forceplay',
        'forceplayall',
        'forcespec',
        'forcespecall',
        'forcespectator',
        'free',
        'freeall',
        'next',
        'nextmap',
        'pluginsupdate',
        'restart',
        'shutdown',
        'shutdownserver',
        'skip',
    );

    /**
     * Settings
     */
    // TODO make private after PresetManager::DEPRECATED_PRESETS are gone
    public const SETTING_ADMINS_AUTHENTICATIONLEVEL = 'Admins/Authentication Level';
    public const SETTING_ADMINS_DEBUG               = 'Admins/Debug';

    public const SETTING_MESSAGES_CHATPREFIX          = 'Messages/Chat Prefix';
    public const SETTING_MESSAGES_FORMATDEBUGMESSAGES = 'Messages/Format Debug Messages';
    
    /*
     * Private properties
     */
    /** @var ManiaControl $maniaControl */
    private $maniaControl = null;

    private $dataManager       = null;
    private $extensionManager  = null;
    private $koCallbackManager = null;
    private $presetManager     = null;

    /**
     * @see \ManiaControl\Plugins\Plugin::prepare()
     * @param ManiaControl $maniaControl
     */
    public static function prepare(ManiaControl $maniaControl)
    {
        foreach (PresetManager::ALL_PRESETS as $preset)
        {
            if (strpos($preset, ' ') !== false)
                throw Exception('The name of a Knockout Preset should not contain any spaces, please check!');
        }
    }

    public static function getAuthor     () { return self::PLUGIN_AUTHOR ; }
    public static function getId         () { return self::PLUGIN_ID     ; }
    public static function getName       () { return self::PLUGIN_NAME   ; }
    public static function getVersion    () { return self::PLUGIN_VERSION; }
    public static function getDescription() { return 'Plugin offers a KnockOut Plugin'; }

    public function getAdminsAuthenticationLevel() { return AuthenticationManager::getAuthLevel($this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_ADMINS_AUTHENTICATIONLEVEL)); }
    public function getAdminsDebug() { return (boolean) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_ADMINS_DEBUG); }

    public function getMessagesChatPrefix() { return (string) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_MESSAGES_CHATPREFIX); }
    public function getMessagesFormatDebugMessages() { return (string) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_MESSAGES_FORMATDEBUGMESSAGES); }

    public function getDataManager() { return $this->dataManager; }
    public function getExtensionManager() { return $this->extensionManager; }
    public function getManiaControl() { return $this->maniaControl; }

    public function load(ManiaControl $maniaControl)
    {
        $this->maniaControl = $maniaControl;

        // Settings
        $this->maniaControl->getAuthenticationManager()->definePluginPermissionLevel(
            $this,
            self::SETTING_ADMINS_AUTHENTICATIONLEVEL,
            AuthenticationManager::AUTH_LEVEL_SUPERADMIN,
            AuthenticationManager::AUTH_LEVEL_MODERATOR
        );

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_JOIN,
            $this,
            'handlePlayerJoinCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_BEGIN,
            $this,
            'handleRoundBeginCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_KO_LEGACY,
            $this,
            'handleRoundKoLegacyCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_ROUND_KO_SCRIPT,
            $this,
            'handleRoundKoScriptCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_KO,
            $this,
            'handlePlayerKoCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_PLAYER_DC,
            $this,
            'handlePlayerDisconnectCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_MAP_END,
            $this,
            'handleMapEndCallback'
        );

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            KnockOutCallbacks::KO_END,
            $this,
            'koStopAnnounceWinner'
        );

        // Commands
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_DUMP,
            $this,
            'onCommandDump',
            true,
            'Dumps all runtime data of the plugin and its extensions.'
        );
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_MAP_SKIP,
            $this,
            'onCommandMapSkip',
            true,
            'Skips to the next Map without knocking everyone out.'
        );
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_MATCH_BEGIN,
            $this,
            'onCommandMatchBegin',
            true,
            'Starts a KnockOut.'
        );
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_MATCH_END,
            $this,
            'onCommandMatchEnd',
            true,
            'Stops the current KnockOut.'
        );
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_PLAYER_KO,
            $this,
            'onCommandPlayerKO',
            true,
            'Manually knocksout a player.'
        );
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_ROUND_END,
            $this,
            'onCommandRoundEnd',
            true,
            'Ends the Round without knocking everyone out.'
        );
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_SETTINGS,
            $this,
            'onCommandSettings',
            true,
            'Open the configurator with the KO Settings.'
        );
        $this->maniaControl->getCommandManager()->registerCommandListener(
            self::COMMAND_TEST,
            $this,
            'onCommandTest',
            true,
            'Runs simple tests on the KnockOut plugin and its extensions.'
        );

        /* disabled until tests for modules are implemented
        $this->maniaControl->getCommandManager()->registerCommandListener(
            'kotest',
            $this,
            'onCommandKO',
            true,
            'Tests certain features of KnockOutPlugin.'
        );
        */

        // disable overall unwanted commands
        $this->maniaControl->getCommandManager()->disableCommand(
            self::COMMANDS_TO_DISABLE_PERMANENTLY,
            false,
            $this
        );
        $this->maniaControl->getCommandManager()->disableCommand(
            self::ADMINCOMMANDS_TO_DISABLE_PERMANENTLY,
            true,
            $this
        );

        // Default UI
        $this->maniaControl->getModeScriptEventManager()->setTrackmaniaUIProperties('
            <ui_properties>
                <endmap_ladder_recap visible="false" />
                <map_info            visible="false" />
                <round_scores        visible="false" />
            </ui_properties>
        ');

        $this->dataManager       = new DataManager($this);
        $this->extensionManager  = new ExtensionManager($this);
        $this->koCallbackManager = new KnockOutCallbackManager($this);
        $this->presetManager     = new PresetManager($this);
    }

    public function onCommandDump(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkPluginPermission(
            $this,
            $player,
            self::SETTING_ADMINS_AUTHENTICATIONLEVEL
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }

        $this->dump($player);
    }

    public function onCommandMapSkip(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkPluginPermission(
            $this,
            $player,
            self::SETTING_ADMINS_AUTHENTICATIONLEVEL
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }

        $this->manualSkipMap($player);
    }

    public function onCommandMatchBegin(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkPluginPermission(
            $this,
            $player,
            self::SETTING_ADMINS_AUTHENTICATIONLEVEL
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }

        $this->startKnockOut();
    }

    public function onCommandMatchEnd(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkPluginPermission(
            $this,
            $player,
            self::SETTING_ADMINS_AUTHENTICATIONLEVEL
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }

        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            function () use ($player) {
                $this->stopKnockOut($player);
            }
        );
    }

    public function onCommandPlayerKO(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkPluginPermission(
            $this,
            $player,
            self::SETTING_ADMINS_AUTHENTICATIONLEVEL
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }

        $chatCommand = explode(' ', $chatCallback[1][2]);
        $command = array_shift($chatCommand);

        if (!$this->dataManager->isMatchLive())
        {
            $this->chat(
                ChatMode::ERROR,
                'Cannot knockout player, no KO-Match live!'
            );
            return;
        }

        $this->knockoutPlayer($chatCommand);
    }

    public function onCommandRoundEnd(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkPluginPermission(
            $this,
            $player,
            self::SETTING_ADMINS_AUTHENTICATIONLEVEL
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }

        if (!$this->dataManager->isMatchLive())
        {
            $this->chat(
                ChatMode::ERROR,
                'Cannot end round, no KO-Match live!'
            );
            return;
        }

        $this->manualEndRound($player);
    }

    public function onCommandSettings(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkPluginPermission(
            $this,
            $player,
            self::SETTING_ADMINS_AUTHENTICATIONLEVEL
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }

        $pluginMenu = $this->maniaControl->getPluginManager()->getPluginMenu();
        $player->setCache(
            $pluginMenu,
            PluginMenu::CACHE_SETTING_CLASS,
            self::class
        );
        $this->maniaControl->getConfigurator()->showMenu($player, $pluginMenu);
    }

    public function onCommandTest(array $chatCallback, Player $player)
    {
        if ( !$this->maniaControl->getAuthenticationManager()->checkPluginPermission(
            $this,
            $player,
            self::SETTING_ADMINS_AUTHENTICATIONLEVEL
        ) )
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }

        if ($this->dataManager->isMatchLive())
        {
            $this->chat(
                ChatMode::ERROR,
                'Cannot execute tests, when a KO-Match is live!'
            );
            return;
        }

        $this->test($player);
    }

    public function startKnockOut()
    {
        if ($this->dataManager->isMatchLive())
        {
            $this->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot start KnockOut, already running!'
            );

            return;
        }

        $playerCount = $this->maniaControl->getPlayerManager()->getPlayerCount();
        $requiredPlayerCount = 2 - (int) $this->getAdminsDebug();
        if ($playerCount < $requiredPlayerCount)
        {
            $this->chat(
                ChatMode::ADMIN_ERROR,
                TMMUtils::formatMessage(
                    'Minimum %s Player(s) required to start a KO-Cup!',
                    $requiredPlayerCount
                )
            );

            return;
        }

        $this->chat(
            ChatMode::INFORMATION,
            'Loading KO ...'
        );
 
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'koStartForcePlayers',
                'koStartBlockCommands',
            ),
            'koStartError'
        );
        $this->dataManager->registerCallsOnKoLoad();
        $this->extensionManager->registerCallsOnKoLoad();
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            'koStartSuccess'
        );
    }

    public function koStartForcePlayers()
    {
        $this->chat(
            ChatMode::INFORMATION,
            'Forcing everyone into playmode ...'
        );

        $this->maniaControl->getClient()->keepPlayerSlots(false);

        $spectatorLogins = array();
        $playerExtension = $this->extensionManager->getExtension(PlayerExtension::class);
        if ($playerExtension !== null)
            $spectatorLogins = array_merge($spectatorLogins, $playerExtension->getSpectatorList());
        
        $players = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($players as $player)
        {
            $this->maniaControl->getCallQueueManager()->registerListening(
                $this,
                function () use ($player, $spectatorLogins) {
                    if (in_array($player->login, $spectatorLogins))
                    {
                        $this->chat(
                            ChatMode::ADMIN_INFORMATION,
                            TMMUtils::formatMessage(
                                'By settings, %s is not allowed to play, forcing into Spectator Mode ...',
                                $player
                            )
                        );
                        $this->force(
                            $player,
                            PlayerActions::SPECTATOR_SPECTATOR
                        );
                    }
                    else
                    {
                        $this->force(
                            $player,
                            PlayerActions::SPECTATOR_PLAYER
                        );
                    }
                },
                'koStartError',
                true
            );
        }
    }

    public function koStartBlockCommands()
    {
        $this->chat(
            ChatMode::INFORMATION,
            'Blocking commands ...'
        );

        // have one EndRound, in which the players do not get KOed
        $this->maniaControl->getCommandManager()->disableCommand(
            self::COMMANDS_TO_DISABLE_LIVE,
            false,
            $this
        );
        $this->maniaControl->getCommandManager()->disableCommand(
            self::ADMINCOMMANDS_TO_DISABLE_LIVE,
            true,
            $this
        );
    }

    public function koStartSuccess()
    {
        $this->chat(
            ChatMode::SUCCESS,
            'KO match starts!'
        );
        $this->maniaControl->getCallbackManager()->triggerCallback(KnockOutCallbacks::KO_BEGIN);
    }

    public function koStartError($errorOnMethod)
    {
        $this->chat(
            ChatMode::ADMIN_ERROR,
            TMMUtils::formatMessage(
                'Error when executing %s!',
                $errorOnMethod
            )
        );
    }

    public function stopKnockOut($player = null)
    {
        if (!$this->dataManager->isMatchLive())
        {
            $this->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot stop KO, none running!'
            );

            return;
        }

        if ($player instanceof Player)
        {
            $this->chat(
                ChatMode::INFORMATION,
                TMMUtils::formatMessage(
                    "%s stopped KO!",
                    $player
                )
            );
        }
        
        $teamExtension = $this->extensionManager->getExtension(TeamExtension::class);
        if ($teamExtension === null || !$teamExtension->isTeammode())
        {
            $this->maniaControl->getCallbackManager()->triggerCallback(
                KnockOutCallbacks::KO_END,
                $this->dataManager->getWinner(),
                $this->dataManager->getNbRoundsTotalFinished()
            );
        }
        elseif ($player === null)
        {
            // stopKnockOut got called by TeamExtension, the players remaining are the winners
            $this->maniaControl->getCallbackManager()->triggerCallback(
                KnockOutCallbacks::KO_END,
                $this->dataManager->getPlayerList()->getPlayers(),
                $this->dataManager->getNbRoundsTotalFinished()
            );
        }

        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            array(
                'koStopUnblockCommands',
                'koStopFreePlayers',
            ),
            'koStartError'
        );
        $this->dataManager->registerCallsOnKoUnload();
        $this->extensionManager->registerCallsOnKoUnload();
        $this->maniaControl->getCallQueueManager()->registerListening(
            $this,
            'koStopSuccess',
            'koStartError'
        );
    }

    public function koStopAnnounceWinner($player, int $nbRoundsSurvived)
    {
        if ($player instanceof Player || is_array($player))
        {
            $this->chat(
                ChatMode::SUCCESS,
                TMMUtils::formatMessage(
                    '$fb0Congratulations to %s for winning the KO.',
                    TMMUtils::chatFormatPlayer($player)
                )
            );
        }
        elseif ($player === null)
        {
            $this->chat(
                ChatMode::SUCCESS,
                '$fb0No Winner in the KO.'
            );
        }
        else
        {
            assert(False);
        }
    }

    public function koStopUnblockCommands()
    {
        $this->chat(
            ChatMode::INFORMATION,
            'Unblocking commands ...'
        );

        $this->maniaControl->getCommandManager()->enableCommand(
            self::COMMANDS_TO_DISABLE_LIVE,
            false,
            $this
        );
        $this->maniaControl->getCommandManager()->enableCommand(
            self::ADMINCOMMANDS_TO_DISABLE_LIVE,
            true,
            $this
        );
    }

    public function koStopFreePlayers()
    {
        $this->chat(
            ChatMode::INFORMATION,
            'Freeing all players ...'
        );
        $players = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($players as $player)
        {
            $this->force(
                $player,
                PlayerActions::SPECTATOR_USER_SELECTABLE
            );
        }
        $this->maniaControl->getClient()->keepPlayerSlots(false);
    }

    public function koStopSuccess()
    {
        $this->chat(
            ChatMode::SUCCESS,
            'KO match ended!'
        );
    }

    public function koStopError($errorOnMethod)
    {
        $this->chat(
            ChatMode::ADMIN_ERROR,
            TMMUtils::formatMessage(
                'Error when stopping KO on %s!',
                $errorMessage
            )
        );
    }

    public function manualEndRound(Player $player)
    {
        $this->maniaControl->getCallbackManager()->triggerCallback(KnockOutCallbacks::KO_MANUAL_SKIP);
        $this->maniaControl->getModeScriptEventManager()->forceTrackmaniaRoundEnd();
        $this->chat(
            ChatMode::INFORMATION,
            TMMUtils::formatMessage(
                '%s forced end of round!',
                $player
            )
        );
    }

    public function manualSkipMap(Player $player)
    {
        $this->maniaControl->getCallbackManager()->triggerCallback(KnockOutCallbacks::KO_MANUAL_SKIP);
        $this->maniaControl->getMapManager()->getMapActions()->skipMap();
        $this->chat(
            ChatMode::INFORMATION,
            TMMUtils::formatMessage(
                '%s skipped map!',
                $player
            )
        );
    }

    public function handlePlayerJoinCallback(Player $player)
    {
        if ( $this->force(
            $player,
            PlayerActions::SPECTATOR_SPECTATOR
        ) )
        {
            $this->chat(
                ChatMode::INFORMATION,
                'A KO match is in progress, please wait!',
                $player
            );
        }
        else
        {
            $this->chat(
                ChatMode::ADMIN_ERROR,
                TMMUtils::formatMessage(
                    '%s did not get forced into spec-mode on live-join!',
                    $player
                )
            );
        }
    }

    public function handleRoundBeginCallback()
    {
        $requiredPlayerCount = 2 - (int) $this->getAdminsDebug();
        if ($this->dataManager->getNbPlayersCurrent() < $requiredPlayerCount)
        {
            $this->chat(
                ChatMode::ERROR,
                TMMUtils::formatMessage(
                    'Minimum %s Player(s) required to start a KO-Round, stopping KO!',
                    $requiredPlayerCount
                )
            );

            $this->maniaControl->getCallQueueManager()->registerListening(
                $this,
                'stopKnockOut'
            );
        }
    }

    public function handleMapEndCallback()
    {
        $requiredPlayerCount = 2 - (int) $this->getAdminsDebug();
        if ($this->dataManager->getNbPlayersCurrent() < $requiredPlayerCount)
        {
            // use case, so no error message
            $this->maniaControl->getCallQueueManager()->registerListening(
                $this,
                'stopKnockOut'
            );
        }
    }

    public function handleRoundKoLegacyCallback(int $targetRoundPoints)
    {
        $currentRanking = null;
        try
        {
            $currentRanking = $this->maniaControl->getClient()->getCurrentRanking();
        }
        catch (\Exception $e)
        {
            $this->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot execute KO procedure!'
            );
            return;
        }

        for (end($currentRanking); key($currentRanking) !== null; prev($currentRanking))
        {
            $playerRanking = current($currentRanking);
            $player = $this->maniaControl->getPlayerManager()->getPlayer($playerRanking->login);
            if ($player === null) {
                $this->chat(
                    ChatMode::ADMIN_ERROR,
                    'Internal error on handleRoundKoLegacyCallback, check logs for details!'
                );
                Logger::logInfo(var_export($playerRanking, true));
                continue;
            }

            $playerPoints = $player->getCache(
                $this,
                DataManager::KO_LEGACY_POINTS
            );
            $playerRoundPoints = $playerRanking->score - $playerPoints;
            if ($playerRoundPoints < $targetRoundPoints)
                $this->knockoutPlayer($player);

            $player->setCache(
                $this,
                DataManager::KO_LEGACY_POINTS,
                $playerRanking->score
            );
        }
    }

    // DO NOT USE DataManager-RoundScores, since it does not track non-retiring DNFs
    public function handleRoundKoScriptCallback(OnScoresStructure $structure)
    {
        // 1. add players by round time (keep ordering from ManiaScript as much as possible)
        $results = $structure->getPlayerScores();
        $playersSlowest = array();
        for (end($results); key($results) !== null; prev($results))
        {
            $result = current($results);
            $player = $result->getPlayer();
            if (!$player->isConnected || $player->isSpectator)
                continue;
            
            $prevRaceTime = $result->getPrevRaceTime();
            // if not valid, normalize to -1
            if (!is_int($prevRaceTime) || $prevRaceTime < 0)
                $prevRaceTime = -1;

            if (!array_key_exists($prevRaceTime, $playersSlowest))
                $playersSlowest[$prevRaceTime] = array();
            
            // push to have the players reported worse by ManiaScript more upfront in sorting
            array_push($playersSlowest[$prevRaceTime], $player);
        }

        // 2. automatically KO all DNFs
        $playersToKO = array();
        if (array_key_exists(-1, $playersSlowest))
        {
            $playersToKO = $playersSlowest[-1];
            unset($playersSlowest[-1]);
        }

        // 3. find $nbKnockouts slowest players (if needed)
        $nbKnockouts = $this->dataManager->getNbKnockoutsToApply();
        if (count($playersToKO) < $nbKnockouts && !empty($playersSlowest))
        {
            krsort($playersSlowest); // sorting is needed now, slowest (highest) times in front
            while (count($playersToKO) < $nbKnockouts && !empty($playersSlowest))
            {
                $slowestTime = array_key_first($playersSlowest);
                // shift to get the player reported worse by ManiaScript
                $slowestPlayer = array_shift($playersSlowest[$slowestTime]);
                array_push($playersToKO, $slowestPlayer);

                if (empty($playersSlowest[$slowestTime]))
                    unset($playersSlowest[$slowestTime]);
            }
        }

        // 4. apply KOs
        foreach ($playersToKO as $player)
            $this->knockoutPlayer($player);
    }

    public function handlePlayerDisconnectCallback(Player $player)
    {
        $formatExtension = $this->extensionManager->getExtension(FormatExtension::class);
        if ($formatExtension === null)
            $this->knockoutPlayer($player);
        // else formatExtension will take responsibility
    }

    public function handlePlayerKoCallback(Player $player, int $position, int $nbRoundsSurvived)
    {
        $this->chat(
            ChatMode::INFORMATION,
            TMMUtils::formatMessage(
                '%s is eliminated on position %s (%s round(s) survived)!',
                $player,
                "#{$position}",
                $nbRoundsSurvived
            )
        );

        if (!$this->extensionManager->isLoaded(PlayerExtension::class))
            $this->force($player, PlayerActions::SPECTATOR_SPECTATOR);
        
        if (!$this->dataManager->isScriptMode() && $this->dataManager->getNbPlayersCurrent() < 2)
        {
            $this->maniaControl->getCallQueueManager()->registerListening(
                $this,
                'stopKnockOut'
            );
        }
    }

    public function knockoutPlayer($player)
    {
        if (!$this->dataManager->isMatchLive() || $player === null)
            return;

        if (is_array($player))
        {
            foreach ($player as $p)
                $this->knockoutPlayer($p);

            return;
        }

        if (is_string($player))
            $player = $this->maniaControl->getPlayerManager()->getPlayer($player);

        assert($player instanceof Player);
        if (!$this->dataManager->getPlayerList()->isAlive($player))
            return;

        $formatExtension = $this->extensionManager->getExtension(FormatExtension::class);
        if ($formatExtension === null)
        {
            $nbRoundsTotalFinished = $this->dataManager->getNbRoundsTotalFinished();
            $this->maniaControl->getCallQueueManager()->registerListening(
                $this,
                function () use ($player, $nbRoundsTotalFinished) {
                    $nbPlayersCurrent = $this->dataManager->getNbPlayersCurrent();
                    $this->maniaControl->getCallbackManager()->triggerCallback(
                        KnockOutCallbacks::KO_PLAYER_KO,
                        $player,
                        $nbPlayersCurrent,
                        $nbRoundsTotalFinished
                    );
                }
            );
        }
        else
        {
            // FormatExtension takes the responsibility of a KO
            $this->maniaControl->getCallbackManager()->triggerCallback(
                FormatExtension::KOCB_PLAYER_KILL,
                $player
            );
        }
    }

    /**
     * Helper Functions
     */
    public function chat(int $mode, $messages, $logins = null)
    {
        if (empty($messages))
            return;

        if (!is_array($messages))
            $messages = array($messages);

        $loginsOrAuthLevel = $logins;
        switch ($mode)
        {
            case ChatMode::ADMIN_SUCCESS:
            case ChatMode::ADMIN_INFORMATION:
            case ChatMode::ADMIN_MESSAGE:
            case ChatMode::ADMIN_ERROR:
            case ChatMode::ADMIN_EXCEPTION:
                $loginsOrAuthLevel = $this->getAdminsAuthenticationLevel();
                break;
            case ChatMode::INFORMATION:
                //array_unshift($messages, $this->getChatInformationFormat());
                break;
            default: break;
        }

        $prefix = $this->getMessagesChatPrefix();
        if (strlen($prefix) <= 0)
            $prefix = true; // default

        TMMUtils::chat(
            $this->maniaControl,
            $prefix,
            $mode,
            $messages,
            $loginsOrAuthLevel
        );
    }

    /**
     * Formats message to input variable with white color, if debug-Mode is enabled.
     * @param bool $force
     * @param string $message
     * @param ...$inputs
     */
    public function debugFormatMessage(bool $force, string $message, ...$inputs)
    {
        if ($force || $this->getAdminsDebug())
        {
            TMMUtils::chat(
                $this->maniaControl,
                $this->getMessagesFormatDebugMessages(),
                ChatMode::ADMIN_MESSAGE,
                TMMUtils::formatMessage($message, ...$inputs),
                $this->getAdminsAuthenticationLevel()
            );
        }
    }

    private function dump(Player $player)
    {
        // Build Manialink
        $numLines     = 15;
        $lineHeight   = 4;
        $height       = $this->maniaControl->getManialinkManager()->getStyleManager()->getListWidgetsHeight();
        $width        = $this->maniaControl->getManialinkManager()->getStyleManager()->getListWidgetsWidth();
        $quadStyle    = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultMainWindowStyle();
        $quadSubstyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultMainWindowSubStyle();

		$manialink = new ManiaLink(ManialinkManager::MAIN_MLID);
		$script    = $manialink->getScript();
		$paging    = new Paging();
        $script->addFeature($paging);
		
		$frame = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultListFrame($script, $paging);
        $manialink->addChild($frame);
        
        $headline = new Label_Text();
        $headline->setHorizontalAlign(Label_Text::LEFT);
        $headline->setPosition(-0.45*$width, 0.45*$height);
        $headline->setSize(0.6*$width, 5);
        $headline->setStyle(Label_Text::STYLE_TextCardSmall);
        $headline->setTextSize(3);
        $headline->setVerticalAlign(Label_Text::TOP);

		$posX = -0.45*$width;
		$initialPosY = 0.4*$height - 5;
		$posY = $initialPosY;

        $extensions = array_merge(
            array(DataManager::class => $this->dataManager),
            $this->extensionManager->getExtensions()
        );

		foreach ($extensions as $name => $instance) {
            $i = 0;
            $instanceDump = TMMUtils::dump($instance);
            foreach ($instanceDump as $key => $value) {
                if ($i % $numLines == 0) {
                    // page full, or new version number
                    $pageFrame = new Frame();
                    $pageHeadline = clone $headline;
                    $pageHeadline->setText($name);

                    $pageFrame->addChild($pageHeadline);
                    $frame->addChild($pageFrame);
                    $paging->addPageControl($pageFrame);

                    $posY = $initialPosY;
                }
                
                if ($value === null)
                    $value = 'null';
                elseif (is_bool($value))
                    $value = $value ? 'true' : 'false';
			
                $label = new Label_Text();
                $pageFrame->addChild($label);
                $label->setHorizontalAlign(Label_Text::LEFT);
                $label->setPosition($posX, $posY);
                $label->setStyle(Label_Text::STYLE_TextCardMedium);
                $label->setText("{$key} => {$value}"); // prevent ManiaPlanet formatting
                $label->setTextSize(2);

                $posY -= $lineHeight;
                $i++;
            }
		}

        // Display manialink
        $this->maniaControl->getManialinkManager()->sendManialink($manialink, $player);
    }

    public function force(Player $player, $specMode)
    {
        try
        {
            $this->maniaControl->getClient()->forceSpectator(
                $player->login,
                $specMode
            );
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    public function getPluginSettingsToScriptSettings(array $pluginSettings)
    {
        foreach ($pluginSettings as $key => $value)
        {
            unset($pluginSettings[$key]);
            $pluginSettings[$this->getPluginSettingToScriptSetting($key)] = $value;
        }

        return $pluginSettings;
    }

    public function getPluginSettingToScriptSetting(string $pluginSetting)
    {
        if (strpos($pluginSetting, 'S_') !== false)
        {
            $scriptSetting = explode('S_', $pluginSetting, 2)[1];
            return "S_{$scriptSetting}";
        }

        return $pluginSetting;
    }

    public function kick(Player $player, string $message)
    {
        try
        {
            $this->maniaControl->getClient()->kick(
                $player->login,
                $message
            );
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    public function test(Player $player)
    {
        $allTestsSuccess = true;
        foreach ($this->extensionManager->getExtensions() as $extensionName => $extensionInstance)
        {
            if ($extensionInstance->test() === false)
            {
                TMMUtils::chat(
                    $this->maniaControl,
                    $this->getMessagesFormatDebugMessages(),
                    ChatMode::ERROR,
                    TMMUtils::formatMessage(
                        "Errors on Tests of extension %s, please check implementation!",
                        $extensionName
                    ),
                    $player
                );

                $allTestsSuccess = false;
            }
        }

        if ($allTestsSuccess)
        {
            $this->chat(
                ChatMode::SUCCESS,
                'All tests successful!'
            );
        }
    }

    /**
     * Unload the plugin and its resources
     * @see \ManiaControl\Plugins\Plugin::unload()
     */
    public function unload()
    {
        $this->presetManager->unload();
        $this->presetManager = null;

        $this->koCallbackManager->unload();
        $this->koCallbackManager = null;

        $this->extensionManager->unloadExtensions();
        $this->extensionManager = null;

        $this->dataManager->unload();
        $this->dataManager = null;
        
        $this->maniaControl->getCommandManager()->unregisterCommandListener($this);
        $this->maniaControl->getCallbackManager()->unregisterCallbackListener($this);
        $this->maniaControl = null;
    }
}
