<?php

namespace TMMasters;

use FML\Controls\Entry;
use FML\Controls\Frame;
use FML\Controls\Labels\Label_Text;
use FML\Controls\Quad;
use FML\Controls\Quads\Quad_Icons64x64_1;
use ManiaControl\Admin\AuthenticationManager;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallbackManager;
use ManiaControl\Callbacks\CallQueueListener;
use ManiaControl\Callbacks\TimerListener;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Logger;
use ManiaControl\ManiaControl;
use ManiaControl\Manialinks\ManialinkPageAnswerListener;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Plugins\Plugin;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;

/**
 * WhitelistPlugin provides a simple plugin to let only the people join you want to let join. Original by hbarney for Xaseco.
 *
 * @author axelalex2
 */
class WhitelistPlugin implements CallbackListener, CallQueueListener, CommandListener, ManialinkPageAnswerListener, Plugin, TimerListener
{
    /**
     * Constants
     */
    const PLUGIN_ID      = 135;
    const PLUGIN_VERSION = 20.09;
    const PLUGIN_NAME    = 'WhitelistPlugin';
    const PLUGIN_AUTHOR  = 'axelalex2';

    const TABLE_WHITELISTNAMES    = 'mc_whitelistnames';
    const TABLE_WHITELISTLOGINS   = 'mc_whitelistlogins';
    const TABLE_WHITELISTRELATION = 'mc_whitelistrelation';

    const SETTING_WHITELIST_ACTIVATIONCOUNTDOWN         = 'Countdown on activation (in s)';
    const SETTING_WHITELIST_ACTIVATIONDELAY             = 'Delay on activation (in s)';
    const SETTING_WHITELIST_AUTHENTICATIONLEVEL         = 'Authentication Level for wl* Commands';
    const SETTING_WHITELIST_AUTOPASSAUTHENTICATIONLEVEL = 'Automatic Check-Pass Authentication Level';
    const SETTING_WHITELIST_MESSAGEONWHITELISTKICK      = 'Message on Kick by Whitelist';

    const ACTION_ADD_LOGIN       = 'WhitelistPlugin.Action.AddLogin';
    const ACTION_CLEAR_WHITELIST = 'WhitelistPlugin.Action.Clear';
    const ACTION_CURRENT_PLAYERS = 'WhitelistPlugin.Action.CurrentPlayers';
    const ACTION_LOAD_WHITELIST  = 'WhitelistPlugin.Action.LoadWhitelist';
    const ACTION_SAVE_WHITELIST  = 'WhitelistPlugin.Action.SaveWhitelist';
    const ACTION_TOGGLE_ACTIVE   = 'WhitelistPlugin.Action.ToggleActive';

    /**
     * Private properties
     */
    /** @var    ManiaControl $maniaControl */
    private $maniaControl = null;

    private $active               = false;
    private $joinPlayerMessage    = null;
    private $joinSpectatorMessage = null;
    private $localWhitelist       = array();

    /**
     * @see \ManiaControl\Plugins\Plugin::prepare()
     * @param ManiaControl $maniaControl
     */
    public static function prepare(ManiaControl $maniaControl)
    { }

    public static function getAuthor     () { return self::PLUGIN_AUTHOR ; }
    public static function getId         () { return self::PLUGIN_ID     ; }
    public static function getName       () { return self::PLUGIN_NAME   ; }
    public static function getVersion    () { return self::PLUGIN_VERSION; }
    public static function getDescription() { return 'Plugin offers a Whitelist, which only lets certain people join (original by hbarney for Xaseco)'; }

    public function getActivationCountdown()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this,
            self::SETTING_WHITELIST_ACTIVATIONCOUNTDOWN
        );
    }
    public function getActivationDelay()
    {
        return (int) $this->maniaControl->getSettingManager()->getSettingValue(
            $this,
            self::SETTING_WHITELIST_ACTIVATIONDELAY
        );
    }
    public function getAuthenticationLevel()
    {
        return AuthenticationManager::getAuthLevel(
            $this->maniaControl->getSettingManager()->getSettingValue(
                $this,
                self::SETTING_WHITELIST_AUTHENTICATIONLEVEL
            )
        );
    }
    public function getAutoPassAuthenticationLevel()
    {
        return AuthenticationManager::getAuthLevel(
            $this->maniaControl->getSettingManager()->getSettingValue(
                $this,
                self::SETTING_WHITELIST_AUTOPASSAUTHENTICATIONLEVEL
            )
        );
    }
    public function getMessageOnWhitelistKick()
    {
        return (string) $this->maniaControl->getSettingManager()->getSettingValue(
            $this,
            self::SETTING_WHITELIST_MESSAGEONWHITELISTKICK
        );
    }

    /**
     * Load the plugin
     *
     * @param \ManiaControl\ManiaControl $maniaControl
     */
    public function load(ManiaControl $maniaControl)
    {
        $this->maniaControl = $maniaControl;

        $this->initTables();

        // Settings
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_WHITELIST_ACTIVATIONCOUNTDOWN, 3);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_WHITELIST_ACTIVATIONDELAY, 10);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_WHITELIST_AUTHENTICATIONLEVEL, AuthenticationManager::getPermissionLevelNameArray(AuthenticationManager::AUTH_LEVEL_ADMIN));
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_WHITELIST_AUTOPASSAUTHENTICATIONLEVEL, AuthenticationManager::getPermissionLevelNameArray(AuthenticationManager::AUTH_LEVEL_MODERATOR));
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_WHITELIST_MESSAGEONWHITELISTKICK, 'You are not whitelisted!');

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(PlayerManager::CB_PLAYERCONNECT, $this, 'handlePlayerConnectCallback');

        // Commands
        // $this->maniaControl->getCommandManager()->registerCommandListener(array('wl', 'whitelist'), $this, 'onCommandWL', true, 'Shows the graphical interface of the Whitelist.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wladdp'   , $this, 'onCommandWL', true, 'Adds Player Logins to the local Whitelist.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlclear'  , $this, 'onCommandWL', true, 'Clears local Whitelist.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlcurrent', $this, 'onCommandWL', true, 'Creates local Whitelist with current Players and Spectators.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlload'   , $this, 'onCommandWL', true, 'Loads the specified Whitelists from Database.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlloadf'  , $this, 'onCommandWL', true, 'Loads the specified Whitelists from Files.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wloff'    , $this, 'onCommandWL', true, 'Deactivates local Whitelist.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlon'     , $this, 'onCommandWL', true, 'Activates local Whitelist.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlrmp'    , $this, 'onCommandWL', true, 'Removes Player Logins from local Whitelist.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlrmwl'   , $this, 'onCommandWL', true, 'Removes Whitelists from Database.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlsave'   , $this, 'onCommandWL', true, 'Saves local Whitelist into Database.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlsavef'  , $this, 'onCommandWL', true, 'Saves local Whitelist into a File.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlshowp'  , $this, 'onCommandWL', true, 'Show Player Logins from local Whitelist.');
        $this->maniaControl->getCommandManager()->registerCommandListener('wlshowwl' , $this, 'onCommandWL', true, 'Show Names of Whitelists in Database.');

        // Manialink Page
        /*
        $this->maniaControl->getManialinkManager()->registerManialinkPageAnswerListener(self::ACTION_ADD_LOGIN, $this, 'handleActionAddLogin');
        $this->maniaControl->getManialinkManager()->registerManialinkPageAnswerListener(self::ACTION_ADD_LOGIN, $this, 'handleActionShowWhitelistMenu');
        $this->maniaControl->getManialinkManager()->registerManialinkPageAnswerListener(self::ACTION_TOGGLE_ACTIVE, $this, 'handleActionToggleActive');
        $this->maniaControl->getManialinkManager()->registerManialinkPageAnswerListener(self::ACTION_TOGGLE_ACTIVE, $this, 'handleActionShowWhitelistMenu');
        */
    }

    private function initTables()
    {
        $mysqli = $this->maniaControl->getDatabase()->getMysqli();
        $query = "CREATE TABLE IF NOT EXISTS `".self::TABLE_WHITELISTNAMES."` (
            `index` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(100) NOT NULL,
            PRIMARY KEY (`index`),
            UNIQUE (`name`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";
        $mysqli->query($query);
        if ($mysqli->error)
            trigger_error($mysqli->error, E_USER_ERROR);

        $query = "CREATE TABLE IF NOT EXISTS `".self::TABLE_WHITELISTLOGINS."` (
            `index` int(11) NOT NULL AUTO_INCREMENT,
            `login` varchar(100) NOT NULL,
            PRIMARY KEY (`index`),
            UNIQUE (`login`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";
        $mysqli->query($query);
        if ($mysqli->error)
            trigger_error($mysqli->error, E_USER_ERROR);

        $query = "CREATE TABLE IF NOT EXISTS `".self::TABLE_WHITELISTRELATION."` (
            `index` int(11) NOT NULL AUTO_INCREMENT,
            `whitelistIndex` int(11) NOT NULL,
            `loginIndex` int(11) NOT NULL,
            PRIMARY KEY (`index`),
            UNIQUE KEY `whitelist_login` (`whitelistIndex`,`loginIndex`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";
        $mysqli->query($query);
        if ($mysqli->error)
            trigger_error($mysqli->error, E_USER_ERROR);
    }

    public function onCommandWL(array $chatCallback, Player $player)
    {
        $authLevel = $this->getAuthenticationLevel();
        if (!$this->maniaControl->getAuthenticationManager()->checkRight($player, $authLevel))
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return;
        }
        
        $chatCommand = explode(' ', $chatCallback[1][2]);
        $command = array_shift($chatCommand);
        $argument = "";
        if (!empty($chatCommand))
            $argument = $chatCommand[0];

        try
        {
            switch ($command)
            {
                case '//wl'       :
                case '//whitelist': $this->showWhitelistMenu           ($player);                     break;
                case '//wladdp'   : $this->addPlayers                  ($player, $chatCommand);       break;
                case '//wlclear'  : $this->clearWhitelist              ($player);                     break;
                case '//wlcurrent': $this->currentPlayersIntoWhitelist ($player);                     break;
                case '//wlload'   : $this->loadWhitelistsFromDatabase  ($player, $chatCommand);       break;
                case '//wlloadf'  : $this->loadWhitelistsFromFile      ($player, $chatCommand);       break;
                case '//wloff'    : $this->deactivate                  ($player);                     break;
                case '//wlon'     : $this->activate                    ($player);                     break;
                case '//wlrmp'    : $this->removePlayers               ($player, $chatCommand);       break;
                case '//wlrmwl'   : $this->removeWhitelistsFromDatabase($player, $chatCommand, true); break;
                case '//wlsave'   : $this->saveWhitelistToDatabase     ($player, $argument);          break;
                case '//wlsavef'  : $this->saveWhitelistToFile         ($player, $argument);          break;
                case '//wlshowp'  : $this->showPlayers                 ($player);                     break;
                case '//wlshowwl' : $this->showWhitelistsInDatabase    ($player);                     break;
                default:
                    $this->chat(
                        ChatMode::ERROR,
                        TMMUtils::formatMessage(
                            'WL-Command %s not supported!',
                            $command
                        ),
                        $player
                    );
                    break;
            }
        }
        catch (Exception $e)
        {
            $this->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e,
                $authLevel
            );
            return;
        }
    }

    public function handlePlayerConnectCallback(Player $player)
    {
        if ($this->active)
            $this->checkKick($player);
    }

    private function activate(Player $player)
    {
        if ($this->active)
        {
            $this->chat(
                ChatMode::ERROR,
                'Whitelist already activated!',
                $player
            );

            return;
        }

        $this->chat(
            ChatMode::INFORMATION,
            'Activating Whitelist ...'
        );
        
        $this->active = true;

        // deactivate the normal join & leave messages when the whitelist is activated
        $playerManager = $this->maniaControl->getPlayerManager();

        $this->joinPlayerMessage = $this->maniaControl->getSettingManager()->getSettingValue(
            $playerManager,
            PlayerManager::SETTING_JOIN_LEAVE_MESSAGES
        );
        $this->maniaControl->getSettingManager()->setSetting(
            $playerManager,
            PlayerManager::SETTING_JOIN_LEAVE_MESSAGES,
            false
        );

        $this->joinSpectatorMessage = $this->maniaControl->getSettingManager()->getSettingValue(
            $playerManager,
            PlayerManager::SETTING_JOIN_LEAVE_MESSAGES_SPECTATOR
        );
        $this->maniaControl->getSettingManager()->setSetting(
            $playerManager,
            PlayerManager::SETTING_JOIN_LEAVE_MESSAGES_SPECTATOR,
            false
        );

        // prepare messages for whitelist activation announcement
        $activationCountdown = $this->getActivationCountdown();
        $activationDelay = max($activationCountdown, $this->getActivationDelay());

        $this->chat(
            ChatMode::INFORMATION,
            TMMUtils::formatMessage(
                'Kicking all non whitelisted players in %s seconds!',
                $activationDelay
            )
        );

        for ($i = $activationCountdown; $i > 0; $i--)
        {
            $this->maniaControl->getTimerManager()->registerOneTimeListening(
                $this,
                function () use ($i) {
                    $this->chat(
                        ChatMode::INFORMATION,
                        TMMUtils::formatMessage(
                            '%s ...',
                            $i
                        )
                    );
                },
                ($activationDelay-$i) * 1000
            );
        }

        // kick in a non-blocking way
        $this->maniaControl->getTimerManager()->registerOneTimeListening(
            $this,
            function () {
                foreach ($this->maniaControl->getPlayerManager()->getPlayers() as $onlinePlayer)
                {
                    $this->maniaControl->getCallQueueManager()->registerListening(
                        $this,
                        function () use ($onlinePlayer) {
                            $this->checkKick($onlinePlayer);
                        }
                    );
                }

                $this->maniaControl->getCallQueueManager()->registerListening(
                    $this,
                    function () {
                        $this->chat(
                            ChatMode::SUCCESS,
                            'Whitelist activated!'
                        );
                    }
                );
            },
            $activationDelay * 1000
        );
    }

    private function addPlayer(Player $player, string $login, bool $chatMessage = true)
    {
        $this->checkLogin($login);
        
        if (in_array($login, $this->localWhitelist))
        {
            $this->chat(
                ChatMode::ERROR,
                TMMUtils::formatMessage(
                    'Login %s is already whitelisted locally!',
                    $login
                ),
                $player
            );

            return;
        }

        array_push($this->localWhitelist, $login);
        assert(in_array($login, $this->localWhitelist));
        if ($chatMessage)
        {
            $this->chat(
                ChatMode::ADMIN_SUCCESS,
                TMMUtils::formatMessage(
                    '%s added %s to the local Whitelist!',
                    $player,
                    $login
                ),
                $this->getAuthenticationLevel()
            );
        }
    }

    private function addPlayers(Player $player, array $logins, bool $chatMessage = true)
    {
        foreach ($logins as $login)
            $this->addPlayer($player, $login);
    }

    public function chat($mode, $messages, $logins = null)
    {
        if (empty($messages))
            return;

        if (is_string($messages))
            $messages = array($messages);

        assert(is_array($messages));

        $loginsOrAuthLevel = $logins;
        switch ($mode)
        {
            case ChatMode::ADMIN_SUCCESS:
            case ChatMode::ADMIN_INFORMATION:
            case ChatMode::ADMIN_MESSAGE:
            case ChatMode::ADMIN_ERROR:
            case ChatMode::ADMIN_EXCEPTION:
                $loginsOrAuthLevel = $this->getAuthenticationLevel();
                break;
            default: break;
        }

        TMMUtils::chat(
            $this->maniaControl,
            true,
            $mode,
            $messages,
            $loginsOrAuthLevel
        );
    }

    private function checkKick(Player $player)
    {
        if (!$this->active)
            return;

        if ($this->maniaControl->getAuthenticationManager()->checkRight($player, $this->getAutoPassAuthenticationLevel()))
        {
            $authLevelName = $player->getAuthLevelName();
            $color = $this->maniaControl->getColorManager()->getColorByPlayer($player);
            $this->chat(
                ChatMode::SUCCESS,
                TMMUtils::formatMessage(
                    "{$color}{$authLevelName} %s automatically passes!",
                    $player
                )
            );
        }
        elseif (in_array($player->login, $this->localWhitelist))
        {
            $this->chat(
                ChatMode::SUCCESS,
                TMMUtils::formatMessage(
                    '%s is whitelisted!',
                    $player
                )
            );
        }
        elseif ($this->kick($player, $this->getMessageOnWhitelistKick(), 'could not get kicked at Whitelist-Activation!'))
        {
            $this->chat(
                ChatMode::ERROR,
                TMMUtils::formatMessage(
                    '%s is not whitelisted!',
                    $player
                )
            );
        }
    }

    private function checkLogin(string $login)
    {
        $len = strlen($login);
        if ($len <= 1)
            throw new Exception("'{$login}' is too short!");
        if ($len > 100)
            throw new Exception("'{$login}' is too long!");
        if (!preg_match("/^[\-\.0-9\_a-z]+$/", $login))
            throw new Exception("'{$login}' is not a syntactically correct ManiaPlanet-Login!");
    }

    private function checkWhitelistName(string $name)
    {
        $len = strlen($name);
        if ($len <= 0 || $len > 10)
            throw new Exception("'{$name}' does not conform with the whitelist naming rules!");
    }

    private function clearWhitelist(Player $player)
    {
        $this->localWhitelist = array();
        $this->chat(
            ChatMode::ADMIN_SUCCESS,
            TMMUtils::formatMessage(
                '%s cleared local Whitelist!',
                $player
            ),
            $this->getAuthenticationLevel()
        );
    }

    private function currentPlayersIntoWhitelist(Player $player)
    {
        $this->localWhitelist = array();
        $onlinePlayers = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($onlinePlayers as $onlinePlayer)
            $this->addPlayer($player, $onlinePlayer->login, false);

        $this->chat(
            ChatMode::ADMIN_SUCCESS,
            TMMUtils::formatMessage(
                '%s created local Whitelist with current Players and Spectators!',
                $player
            ),
            $this->getAuthenticationLevel()
        );
        Logger::log($this->formattedCurrentWhitelist());
    }

    private function deactivate(Player $player)
    {
        if (!$this->active)
        {
            $this->chat(
                ChatMode::ERROR,
                'Whitelist already deactivated!',
                $player
            );

            return;
        }

        $this->chat(
            ChatMode::INFORMATION,
            'Deactivating Whitelist ...'
        );

        // reenable the normal join & leave messages, if necessary
        $playerManager = $this->maniaControl->getPlayerManager();
        if ($this->joinPlayerMessage != null)
        {
            $this->maniaControl->getSettingManager()->setSetting(
                $playerManager,
                PlayerManager::SETTING_JOIN_LEAVE_MESSAGES,
                $this->joinPlayerMessage
            );
            $this->joinPlayerMessage = null;
        }

        if ($this->joinSpectatorMessage != null)
        {
            $this->maniaControl->getSettingManager()->setSetting(
                $playerManager,
                PlayerManager::SETTING_JOIN_LEAVE_MESSAGES_SPECTATOR,
                $this->joinSpectatorMessage
            );
            $this->joinSpectatorMessage = null;
        }

        $this->active = false;
        $this->maniaControl->getTimerManager()->unregisterTimerListenings($this);

        $this->chat(
            ChatMode::SUCCESS,
            'Whitelist deactivated!'
        );
    }

    private function fetchWhitelistIndexFromDatabase(string $name)
    {
        $this->checkWhitelistName($name);

        $mysqli = $this->maniaControl->getDatabase()->getMysqli();

        // First, fetch the index of the Whitelist (check, if whitelist $name exists)
        $query = "SELECT `index`
            FROM `".self::TABLE_WHITELISTNAMES."`
            WHERE `name` LIKE '".$name."'";
        $result = $mysqli->query($query);
        if (!$result)
        {
            trigger_error($mysqli->error, E_USER_ERROR);
            throw new Exception($mysqli->error);
        }

        $whitelistIndex = $result->fetch_object();
        $result->free();
        if (!isset($whitelistIndex))
            throw new Exception('Whitelist "'.$name.'" does not exist!');

        return $whitelistIndex->index;
    }

    private function formattedCurrentWhitelist()
    {
        $logins = array();
        for ($i = 0; $i < count($this->localWhitelist); $i++)
            array_push($logins, '['.($i+1).'] '.$this->localWhitelist[$i]);

        return implode(', ', $logins);
    }

    public function handleActionAddLogin(array $chatCallback, Player $player)
    {
        $login = $chatCallback[1][3][0]['Value'];
        try {
            $this->checkLogin($login);
            $this->addPlayer($player, $login);
        } catch (Exception $e) {
            $this->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
        }
    }

    public function handleActionShowWhitelistMenu(array $chatCallback, Player $player)
    {
        $this->showWhitelistMenu($player);
    }

    public function handleActionToggleActive(array $chatCallback, Player $player)
    {
        if ($this->active)
            $this->deactivate($player);
        else
            $this->activate($player);
    }

    private function kick(Player $player, string $message, string $errorMessage)
    {
        try
        {
            $this->maniaControl->getClient()->kick($player->login, $message);
            return true;
        }
        catch (Exception $e)
        {
            $this->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e,
                $this->getAuthenticationLevel()
            );
            $this->chat(
                ChatMode::ADMIN_ERROR,
                TMMUtils::formatMessage(
                    "%s {$errorMessage}",
                    $player
                ),
                $this->getAuthenticationLevel()
            );

            return false;
        }
    }

    private function loadWhitelistFromDatabase(Player $player, string $name)
    {
        $this->checkWhitelistName($name);

        $whitelistIndex = $this->fetchWhitelistIndexFromDatabase($name);

        $mysqli = $this->maniaControl->getDatabase()->getMysqli();

        // Then, fetch all logins, which are whitelisted for the given whitelist
        $query = "SELECT `login`
            FROM ".self::TABLE_WHITELISTLOGINS."
            JOIN ".self::TABLE_WHITELISTRELATION." ON ".self::TABLE_WHITELISTLOGINS.".`index` = ".self::TABLE_WHITELISTRELATION.".`loginIndex`
            WHERE ".self::TABLE_WHITELISTRELATION.".`whitelistIndex` = ".$whitelistIndex.";";
        $result = $mysqli->query($query);
        if (!$result)
        {
            trigger_error($mysqli->error, E_USER_ERROR);
            return null;
        }

        $whitelist = array();
        while ($login = $result->fetch_object())
            array_push($whitelist, $login->login);

        $result->free();
        return $whitelist;
    }

    private function loadWhitelistsFromDatabase(Player $player, array $whitelistNames)
    {
        if (count($whitelistNames) == 0)
        {
            $this->chat(
                ChatMode::ERROR,
                'Cannot load no Whitelist!',
                $player
            );

            return false;
        }

        $whitelistFull = array();
        foreach ($whitelistNames as $whitelistName)
        {
            try
            {
                $whitelist = $this->loadWhitelistFromDatabase($player, $whitelistName);
                if ($whitelist === null)
                {
                    $this->chat(
                        ChatMode::ERROR,
                        TMMUtils::formatMessage(
                            'Query to load Whitelist %s from Database failed!',
                            $whitelistName
                        ),
                        $player
                    );

                    return false;
                }
                $whitelistFull = array_merge($whitelistFull, $whitelist);
            }
            catch (Exception $e)
            {
                $this->chat(
                    ChatMode::EXCEPTION,
                    $e,
                    $player->login
                );

                return false;
            }
        }

        $this->localWhitelist = array_unique($whitelistFull);

        $this->chat(
            ChatMode::ADMIN_SUCCESS,
            TMMUtils::formatMessage(
                '%s loaded and merged Whitelists %s from Database!',
                $player,
                implode(', ', $whitelistNames)
            ),
            $this->getAuthenticationLevel()
        );

        return true;
    }

    private function loadWhitelistFromFile(Player $player, string $filename)
    {
        $whitelist = file_get_contents($filename);
        if ($whitelist === false)
            return null;

        $whitelist = preg_split('/[^\-\.0-9\_a-z]+/', $whitelist, -1, PREG_SPLIT_NO_EMPTY);
        return $whitelist;
    }

    private function loadWhitelistsFromFile(Player $player, array $filenames)
    {
        if (count($filenames) == 0)
        {
            $this->chat(
                ChatMode::ERROR,
                'Cannot load no Whitelist!',
                $player->login
            );

            return false;
        }

        $whitelistFull = array();
        foreach ($filenames as $filename)
        {
            try
            {
                $whitelist = $this->loadWhitelistFromFile($player, $filename);
                if ($whitelist === null)
                {
                    $this->chat(
                        ChatMode::ERROR,
                        TMMUtils::formatMessage(
                            'Loading Whitelist %s from File failed!',
                            $filename
                        ),
                        $player
                    );

                    return false;
                }
                $whitelistFull = array_merge($whitelistFull, $whitelist);
            }
            catch (Exception $e)
            {
                $this->chat(
                    ChatMode::EXCEPTION,
                    $e,
                    $player->login
                );
                return false;
            }
        }

        $this->localWhitelist = array_unique($whitelistFull);

        $this->chat(
            ChatMode::ADMIN_SUCCESS,
            TMMUtils::formatMessage(
                '%s loaded and merged Whitelists %s from Files!',
                $player,
                implode(', ', $filenames)
            ),
            $this->getAuthenticationLevel()
        );

        return true;
    }

    private function removePlayer(Player $player, string $login)
    {
        $this->checkLogin($login);

        $index = array_search($login, $this->localWhitelist);
        if ($index === false)
        {
            $this->chat(
                ChatMode::ERROR,
                TMMUtils::formatMessage(
                    'Login %s is not on local Whitelist, cannot be removed!',
                    $login
                ),
                $player
            );

            return false;
        }

        array_splice($this->localWhitelist, $index, 1);
        assert(!in_array($login, $this->localWhitelist));

        $this->chat(
            ChatMode::ADMIN_SUCCESS,
            TMMUtils::formatMessage(
                '%s removed %s from local Whitelist!',
                $player,
                $login
            ),
            $this->getAuthenticationLevel()
        );

        return true;
    }

    private function removePlayers(Player $player, array $logins)
    {
        foreach ($logins as $login)
            $this->removePlayer($player, $login);
    }

    private function removeWhitelistFromDatabase(Player $player, string $whitelistName, bool $chatCallback)
    {
        $this->checkWhitelistName($whitelistName);

        $whitelistIndex = $this->fetchWhitelistIndexFromDatabase($whitelistName);

        $mysqli = $this->maniaControl->getDatabase()->getMysqli();

        $query = "DELETE FROM ".self::TABLE_WHITELISTNAMES."
            WHERE `index` = '".((string) $whitelistIndex)."';";
        $result = $mysqli->query($query);
        if (!$result)
        {
            trigger_error($mysqli->error, E_USER_ERROR);
            return;
        }

        $query = "DELETE FROM ".self::TABLE_WHITELISTRELATION."
            WHERE `whitelistIndex` = '".$whitelistIndex."';";
        $result = $mysqli->query($query);
        if (!$result)
        {
            trigger_error($mysqli->error, E_USER_ERROR);
            return;
        }

        $query = "DELETE FROM ".self::TABLE_WHITELISTLOGINS."
            WHERE `index` NOT IN (
                SELECT `loginIndex`
                FROM ".self::TABLE_WHITELISTRELATION."
                GROUP BY `loginIndex`
            );";
        $result = $mysqli->query($query);
        if (!$result)
        {
            trigger_error($mysqli->error, E_USER_ERROR);
            return;
        }

        if ($chatCallback)
        {
            $this->chat(
                ChatMode::ADMIN_SUCCESS,
                TMMUtils::formatMessage(
                    '%s removed Whitelist %s from Database!',
                    $player,
                    $whitelistName
                ),
                $this->getAuthenticationLevel()
            );
        }
    }

    private function removeWhitelistsFromDatabase(Player $player, array $whitelistNames, bool $chatCallback)
    {
        foreach ($whitelistNames as $whitelistName)
            $this->removeWhitelistFromDatabase($player, $whitelistName, $chatCallback);
    }

    private function saveWhitelistToDatabase(Player $player, string $whitelistName)
    {
        $this->checkWhitelistName($whitelistName);

        try
        {
            $this->removeWhitelistFromDatabase($player, $whitelistName, false);
        }
        catch (Exception $e)
        { }

        $mysqli = $this->maniaControl->getDatabase()->getMysqli();

        // duplication should not occur, because we dropped the whitelist before
        $query = "INSERT INTO ".self::TABLE_WHITELISTNAMES." (`name`)
            VALUES ('".$whitelistName."');";
        $result = $mysqli->query($query);
        if (!$result)
        {
            trigger_error($mysqli->error, E_USER_ERROR);
            return;
        }
        $whitelistIndex = $mysqli->insert_id;

        $loginIndices = array();
        foreach ($this->localWhitelist as $login)
        {
            // duplications can occur, because of different whitelists
            $query = "INSERT INTO ".self::TABLE_WHITELISTLOGINS." (`login`)
                VALUES ('".$login."')
                ON DUPLICATE KEY UPDATE `index`=`index`;";
            $result = $mysqli->query($query);
            if (!$result)
            {
                trigger_error($mysqli->error, E_USER_ERROR);
                return;
            }
            array_push($loginIndices, $mysqli->insert_id);
        }

        assert(count($this->localWhitelist) == count($loginIndices));

        foreach ($loginIndices as $loginIndex)
        {
            // duplication should not occur, because we dropped the whitelist before
            $query = "INSERT INTO ".self::TABLE_WHITELISTRELATION." (`whitelistIndex`, `loginIndex`)
                VALUES (".((string) $whitelistIndex).", ".((string) $loginIndex).");";
            $result = $mysqli->query($query);
            if (!$result)
            {
                trigger_error($mysqli->error, E_USER_ERROR);
                return;
            }
        }

        $this->chat(
            ChatMode::ADMIN_SUCCESS,
            TMMUtils::formatMessage(
                '%s saved local Whitelist as %s to Database!',
                $player,
                $whitelistName
            ),
            $this->getAuthenticationLevel()
        );
    }

    private function saveWhitelistToFile(Player $player, string $filename)
    {
        $whitelistFile = fopen($filename, 'w');
        if (!$whitelistFile)
        {
            $this->chat(
                ChatMode::ERROR,
                'Could not write file!',
                $player
            );

            return false;
        }

        foreach ($this->localWhitelist as $login)
        {
            if (!fwrite($whitelistFile, "{$login}\n"))
            {
                $this->chat(
                    ChatMode::ERROR,
                    TMMUtils::formatMessage(
                        'Writing login %s into file %s failed!',
                        $login,
                        $filename
                    ),
                    $player
                );

                return false;
            }
        }

        fclose($whitelistFile);

        $this->chat(
            ChatMode::ADMIN_SUCCESS,
            TMMUtils::formatMessage(
                '%s saved local Whitelist to file %s!',
                $player,
                $filename
            ),
            $this->getAuthenticationLevel()
        );

        return true;
    }

    private function showPlayers(Player $player)
    {
        $this->chat(
            ChatMode::INFORMATION,
            TMMUtils::formatMessage(
                'Current whitelist: %s',
                $this->formattedCurrentWhitelist()
            ),
            $player
        );
    }

    private function showWhitelistMenu(Player $player)
    {
        $listBuilder = $this->maniaControl->getManialinkManager()->getListBuilder();

        // Header
        $width = $listBuilder->getHeaderFrameWidth();

		$headerFrame = new Frame();

		$label = new Label_Text();
		$headerFrame->addChild($label);
		$label->setHorizontalAlign($label::RIGHT);
		$label->setText('Add Login: ');
		$label->setTextSize(1);
		$label->setX(-$width / 2 + 15);

		$entry = new Entry();
		$headerFrame->addChild($entry);
		$entry->setHorizontalAlign($entry::LEFT);
		$entry->setName('AddLogin');
		$entry->setSize($width * 0.25, 4);
		$entry->setStyle(Label_Text::STYLE_TextValueSmall);
		$entry->setTextSize(1);
		$entry->setX(-$width / 2 + 15);

        $quad = new Quad_Icons64x64_1();
        $headerFrame->addChild($quad);
        $quad->setAction(self::ACTION_ADD_LOGIN);
        $quad->setSize(5, 5);
        $quad->setSubStyle($quad::SUBSTYLE_Add);
        $quad->setX(-$width / 2 + 15 + $width * 0.25 - 2);
        $quad->setZ(1);

		$label = new Label_Text();
		$headerFrame->addChild($label);
		$label->setHorizontalAlign($label::RIGHT);
		$label->setText('Active: ');
        $label->setTextSize(1.2);
		$label->setX($width / 2 - 30);
        

        $quad = new Quad_Icons64x64_1();
        $headerFrame->addChild($quad);
        $quad->setSize(4, 4);
        $quad->setX($width / 2 - 28);
        if ($this->active) {
            $quad->setSubStyle($quad::SUBSTYLE_LvlGreen);
        } else {
            $quad->setSubStyle($quad::SUBSTYLE_LvlRed);
        }

		$label = new Label_Text();
        $headerFrame->addChild($label);
        $label->setAction(self::ACTION_TOGGLE_ACTIVE);
		$label->setHorizontalAlign($label::LEFT);
        $label->setStyle($label::STYLE_TextButtonNavBack);
        $label->setText($this->active ? 'Deactivate' : 'Activate');
        $label->setTextColor('2af');
        $label->setTextSize(1);
		$label->setX($width / 2 - 25);

        $listBuilder->addHeaderFrame($headerFrame);

        // Footer
        $listBuilder->addFooterButton(
            'Clear Whitelist',
            self::ACTION_CLEAR_WHITELIST
        );
        $listBuilder->addFooterButton(
            'Current Players',
            self::ACTION_CURRENT_PLAYERS
        );
        $listBuilder->addFooterButton(
            'Load Whitelist',
            self::ACTION_LOAD_WHITELIST
        );
        $listBuilder->addFooterButton(
            'Save Whitelist',
            self::ACTION_SAVE_WHITELIST
        );

        // Content
        $listBuilder->addEntryCurrent(
            function (string $login) use ($player) {
                return $login === $player->login;
            }
        );
        $listBuilder->addColumn(
            '#',
            0.1,
            function (string $login) {
                $i = array_search($login, $this->localWhitelist, true);
                return $i === false ? '$f00?' : $i + 1;
            }
        );
        $listBuilder->addColumn(
            'Login',
            0.35,
            function (string $login) {
                return $login;
            }
        );
        $listBuilder->addColumn(
            'Nickname',
            0.55,
            function (string $login) {
                $p = $this->maniaControl->getPlayerManager()->getPlayer($login);
                return $p === null ? '' : $p->getEscapedNickname();
            }
        );
        $listBuilder->addAction(
            '$a00x',
            true,
            function (string $login) {
                return "Remove login '{$login}' from the Whitelist";
            }
        );
        $listBuilder->addRows($this->localWhitelist);

        // Show
        $listBuilder->renderList($player);
    }

    private function showWhitelistsInDatabase(Player $player)
    {
        $mysqli = $this->maniaControl->getDatabase()->getMysqli();

        $query = "SELECT `name`
            FROM ".self::TABLE_WHITELISTNAMES.";";
        $result = $mysqli->query($query);
        if (!$result)
        {
            trigger_error($mysqli->error, E_USER_ERROR);
            return;
        }

        $whitelistsInDB = array();
        while ($whitelist = $result->fetch_object())
            array_push($whitelistsInDB, $whitelist->name);

        $this->chat(
            ChatMode::INFORMATION,
            TMMUtils::formatMessage(
                'Available Whitelists in Database: %s',
                implode(', ', $whitelistsInDB)
            ),
            $player
        );
    }

    /**
     * Unload the plugin and its resources
     * @see \ManiaControl\Plugins\Plugin::unload()
     */
    public function unload()
    {
        $this->maniaControl = null;
    }
}
