<?php

namespace TMMasters\ToornamentMania;

use ManiaControl\Callbacks\Structures\TrackMania\OnScoresStructure;
use \TMMasters\Toornament\Models;

abstract class MatchResultParser
{
    public static function afterGameComplete(OnScoresStructure $structure, MatchDataManager $matchDataManager)
    {
        if ($structure->getUseTeams())
            return self::afterGameComplete_Team($structure, $matchDataManager);
        else
            return self::afterGameComplete_Player($structure, $matchDataManager);

        assert(false);
    }

    private static function afterGameComplete_Player(OnScoresStructure $structure, MatchDataManager $matchDataManager)
    {
        return array();
    }

    private static function afterGameComplete_Team(OnScoresStructure $structure, MatchDataManager $matchDataManager)
    {
        $results = array();
        foreach ($matchDataManager->getMatch()->opponents as $opponent)
        {
            array_push($results, array(
                Models\Common\Opponents::NUMBER => $opponent->number,
                Models\Common\Opponents::SCORE  => $opponent->score + ($structure->getWinnerTeamId() === $opponent->number-1)
            ));
        }

        return array(
            Models\Common::OPPONENTS => $results
        );
    }

    public static function completeMatch(MatchDataManager $matchDataManager)
    {
        $scores = array();
        foreach ($matchDataManager->getMatch()->opponents as $opponent)
            $scores[$opponent->number-1] = $opponent->score;

        $results = array();
        $matchType = $matchDataManager->getMatch()->type;
        switch ($matchType)
        {
            case Models\Match\Type::DUEL:
                $results = self::completeMatch_Duel($scores, $matchDataManager);
                $results = self::simpleToToornament_Duel($results, $matchDataManager);
                break;
            case Models\Match\Type::FFA:
                $results = self::completeMatch_FFA($scores, $matchDataManager);
                $results = self::simpleToToornament_FFA($results, $matchDataManager);
                break;
            default:
                assert(false);
        }

        return $results;
    }

    private static function completeMatch_Duel(array $scores, MatchDataManager $matchDataManager)
    {
        assert(count($scores) == 2);
        assert(array_key_exists(0, $scores));
        assert(array_key_exists(1, $scores));
        
        $results = array();
        if ($scores[0] > $scores[1])
        {
            $results[0] = 1;
            $results[1] = 2;
        }
        elseif ($scores[0] < $scores[1])
        {
            $results[0] = 2;
            $results[1] = 1;
        }
        else
        {
            $results[0] = 0;
            $results[1] = 0;
        }

        return $results;
    }

    private static function completeMatch_FFA(array $scores, MatchDataManager $matchDataManager)
    {
        // TODO
        /*
        arsort($scores, SORT_NUMERIC);
        $newScores = array();
        foreach ($scores as $number => $score)
            array_push($newScores, array($number, $score));
        $scores = $newScores;

        $ranks = array();
        $currentRank = 1;
        for ($i = 0; $i < count($scores); $i++)
        {
            $score = $scores[$i];
            $number = $score[0];
            $score = $score[1];

            if ($i > 0 && $scores[$i-1] !== $score)
                $currentRank = $i+1;

            $ranks[$number] = $currentRank;
        }

        return $ranks;
        */
    }

    private static function fillResults(array $results, MatchDataManager $matchDataManager)
    {
        $nbFinishers = count($results);
        $participants = $matchDataManager->getParticipants();
        foreach ($matchDataManager->getMatch()->opponents as $opponent)
        {
            if ($opponent === null || $opponent->participant === null)
                continue;

            assert(array_key_exists($opponent->participant->id, $participants));

            $participant = $participants[$opponent->participant->id];
            assert($participant != null);
            assert(!isset($participant->lineup));
            assert(isset($participant->custom_fields->maniaplanet_id));
            $login = $participant->custom_fields->maniaplanet_id;

            if (!array_key_exists($login, $results))
                $results[$login] = $nbFinishers+1;
        }

        return $results;
    }

    private static function simpleToToornament_Duel(array $results, MatchDataManager $matchDataManager)
    {
        assert(count($results) == count($matchDataManager->getMatch()->opponents));

        $toornamentResults = array();
        foreach ($results as $id => $result)
        {
            array_push($toornamentResults, array(
                Models\Common\Opponents::NUMBER => $id+1,
                Models\Common\Opponents::RESULT => ($result === 1
                                        ? Models\Common\Result::WIN
                                        :   ($result === 0
                                            ? Models\Common\Result::DRAW
                                            : Models\Common\Result::LOSS))
            ));
        }

        return array(
            Models\Common::OPPONENTS => $toornamentResults
        );
    }

    private static function simpleToToornament_FFA(array $results, MatchDataManager $matchDataManager)
    {
        $results = self::fillResults($results, $matchDataManager);
        assert(count($results) === $matchDataManager->getNbParticipants());

        $toornamentResults = array();
        $participants = $matchDataManager->getParticipants();
        $matchOpponents = $matchDataManager->getMatch()->opponents;
        foreach ($results as $login => $rank)
        {
            $participant = null;
            foreach ($participants as $p)
            {
                if ($p->custom_fields->maniaplanet_id === $login)
                {
                    $participant = $p;
                    break;
                }
            }
            assert($participant != null);

            $number = null;
            foreach ($matchOpponents as $o)
            {
                if ($o->participant->id == $participant->id)
                {
                    $number = $o->number;
                    break;
                }
            }
            assert($number != null);

            array_push($toornamentResults, array(
                Models\Common\Opponents::NUMBER => $number,
                Models\Common\Opponents::RANK => $rank
            ));
        }

        return array(
            Models\Common::OPPONENTS => $toornamentResults
        );
    }

    private static function trackmaniaToSimple_Player(OnScoresStructure $structure)
    {
        assert(!$structure->getUseTeams());

        $scores = array();
        foreach ($structure->getPlayerScores() as $playerScore)
            $scores[$playerScore->getPlayer()->login] = $playerScore->getRank();

        return $scores;
    }

    private static function trackmaniaToSimple_Team(OnScoresStructure $structure)
    {
        assert($structure->getUseTeams());

        $scores = array();
        $winnerTeamId = $structure->getWinnerTeamId();
        foreach ($structure->getTeamScores() as $teamScore)
        {
            $teamId = $teamScore->getTeamId();
            // check draw, then win or loss
            $scores[$teamId] = ($winnerTeamId == -1 ? 0 : ($winnerTeamId == $teamId ? 1 : 2));
        }

        return $scores;
    }

    public static function trackmaniaToToornament(OnScoresStructure $structure, MatchDataManager $matchDataManager)
    {
        $results = array();
        if ($structure->getUseTeams())
            $results = self::trackmaniaToSimple_Team($structure);
        else
            $results = self::trackmaniaToSimple_Player($structure);

        $matchType = $matchDataManager->getMatch()->type;
        switch ($matchType)
        {
            case Models\Match\Type::DUEL:
                return self::simpleToToornament_Duel($results, $matchDataManager);
            case Models\Match\Type::FFA:
                return self::simpleToToornament_FFA($results, $matchDataManager);
            default:
                assert(false);
        }

        assert(false);
    }
}
