<?php

namespace TMMasters\ToornamentMania;

use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\Callbacks;
use ManiaControl\Callbacks\Structures\TrackMania\OnScoresStructure;
use ManiaControl\Callbacks\TimerListener;
use ManiaControl\Configurator\ScriptSettings;
use ManiaControl\Files\FileUtil;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerActions;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use ManiaControl\Utils\SystemUtil;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;
use \TMMasters\ChatMode;
use \TMMasters\TMMUtils;
use \TMMasters\ToornamentMania;
use \TMMasters\Toornament\Models;

/**
 * ToornamentMania\Manager is an modular class to synchronize structures between ManiaPlanet-/ManiaControl and Toornament.com
 *
 * @author axelalex2
 */
final class MatchManager implements CallbackListener, TimerListener
{
    const SETTING_TOORNAMENTMANIA_LOAD_SCRIPT_DELAY = 'Load Script Delay (in ms)';
    const SETTING_TOORNAMENTMANIA_START_MATCH_DELAY = 'Start Match Delay (in s)';

    private $accessManager      = null;
    private $maniaControl       = null;
    private $matchDataManager   = null;
    private $playerReady        = null;
    private $scriptsDir         = null;
    private $toornamentMania    = null;
    private $validRound         = true;

    public function getAccessManager   () { return $this->accessManager   ; }
    public function getMatchDataManager() { return $this->matchDataManager; }

    public function getLoadScriptDelay() { return (int) $this->maniaControl->getSettingManager()->getSettingValue($this->toornamentMania, self::SETTING_TOORNAMENTMANIA_LOAD_SCRIPT_DELAY); }
    public function getStartMatchDelay() { return (int) $this->maniaControl->getSettingManager()->getSettingValue($this->toornamentMania, self::SETTING_TOORNAMENTMANIA_START_MATCH_DELAY); }

    public function __construct(ToornamentMania $toornamentMania)
    {
        $this->accessManager    = new AccessManager($toornamentMania);
        $this->maniaControl     = $toornamentMania->getManiaControl();
        $this->matchDataManager = new MatchDataManager($toornamentMania);
        $this->playerReady      = array();
        $this->toornamentMania  = $toornamentMania;

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(Callbacks::TM_SCORES, $this, 'handleEndRoundCallback');
        $this->maniaControl->getCallbackManager()->registerCallbackListener(PlayerManager::CB_PLAYERCONNECT, $this, 'handlePlayerConnectCallback');
        $this->maniaControl->getCallbackManager()->registerCallbackListener(PlayerManager::CB_PLAYERDISCONNECT, $this, 'handlePlayerDisconnectCallback');
        $this->maniaControl->getCallbackManager()->registerCallbackListener(PlayerManager::CB_PLAYERINFOCHANGED, $this, 'handlePlayerInfoChangedCallback');
        $this->maniaControl->getCallbackManager()->registerCallbackListener(ScriptSettings::CB_SCRIPTSETTING_CHANGED, $this, 'handleUpdateScriptSettingCallback');
        $this->maniaControl->getCallbackManager()->registerCallbackListener(SettingManager::CB_SETTING_CHANGED, $this, 'handleUpdateSettingsCallback');

        // Settings
        $this->maniaControl->getSettingManager()->initSetting($this->toornamentMania, self::SETTING_TOORNAMENTMANIA_LOAD_SCRIPT_DELAY, 1000);
        $this->maniaControl->getSettingManager()->initSetting($this->toornamentMania, self::SETTING_TOORNAMENTMANIA_START_MATCH_DELAY, 5);
    }

    private function checkAllPlayersReady()
    {
        $players = $this->maniaControl->getPlayerManager()->getPlayers();

        foreach ($players as $player)
        {
            if ($player->isSpectator)
                continue;

            if (!$this->isPlayerReady($player))
                return false;
        }

        return true;
    }

    private function checkValidRound(OnScoresStructure $structure)
    {
        if (!$this->isMatchRunning())
            return null;

        foreach ($structure->getPlayerScores() as $playerScore)
        {
            $player = $playerScore->getPlayer();
            if (!$player->isConnected)
                continue;

            $response = $this->matchDataManager->getPlayerMode($player);
            assert(is_array($response));
            list($playerMode, $teamIndex) = $response;
            if ($playerMode === PlayerActions::SPECTATOR_SPECTATOR && !$player->isSpectator)
            {
                $this->toornamentMania->chat(
                    ChatMode::ERROR,
                    'There are spectators in player mode, round invalid!'
                );
                $this->validRound = false;
                break;
            }

            if ($structure->getUseTeams())
            {
                if (($teamIndex == 0 || $teamIndex == 1) && $player->teamId != $teamIndex)
                {
                    $this->toornamentMania->chat(
                        ChatMode::ERROR,
                        'There are players in the wrong team, round invalid!'
                    );
                    $this->validRound = false;
                    break;
                }
            }
        }

        if (!$this->validRound)
            $this->setScoresFromToornamentOnCallback(Callbacks::MP_ENDROUNDEND);
    }

    private function completeGame(array $results, OnScoresStructure $structure)
    {
        $currentGameNumber = $this->matchDataManager->getCurrentGameNumber();

        $this->matchDataManager->completeGame($results);
        $this->toornamentMania->chat(
            ChatMode::INFORMATION,
            'Game '.$currentGameNumber.' completed!'
        );

        $matchUpdates = MatchResultParser::afterGameComplete($structure, $this->matchDataManager);
        // match could be over, so don't throw errors
        $this->matchDataManager->updateMatch($matchUpdates, true, false);

        if ($this->matchDataManager->allGamesCompleted() === true)
        {
            $results = MatchResultParser::completeMatch($this->matchDataManager);
            $this->completeMatch($results);
        }
        else
        {
            $this->enableReadyLobby();
        }
    }

    private function completeMatch(array $results)
    {
        $this->matchDataManager->completeMatch($results);
        $this->toornamentMania->chat(
            ChatMode::INFORMATION,
            'Match completed!'
        );
        
        $this->disableReadyLobby();
        $this->unloadMatch();
    }

    private function disableReadyLobby()
    {
        $this->toornamentMania->buttonReadyHide();
        $this->playerReady = array();
    }

    private function enableReadyLobby()
    {
        if (empty($this->playerReady))
        {
            $players = $this->maniaControl->getPlayerManager()->getPlayers();
            foreach ($players as $player)
            {
                if (!$player->isSpectator)
                    $this->setPlayerReady($player, false);
            }
        }
    }

    private function forcePlayer(Player $player)
    {
        if (!$this->isMatchRunning())
            return null;

        if (!$player->isConnected)
            return null;

        $playerProperties = $this->matchDataManager->getPlayerMode($player);
        assert(is_array($playerProperties));
        list($playerMode, $teamIndex) = $playerProperties;

        try {
            $this->maniaControl->getClient()->forceSpectator(
                $player->login,
                $playerMode
            );

            if ($this->maniaControl->getServer()->isTeamMode())
            {
                $this->maniaControl->getClient()->forcePlayerTeam(
                    $player->login,
                    $teamIndex
                );
            }
        } catch (Exception $e) {
            $message = TMMUtils::chatFormatPlayer($player).' did not get forced into correct position!';

            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $message
            );

            return false;
        }

        if ($playerMode === PlayerActions::SPECTATOR_SPECTATOR)
            $this->unsetPlayerReady($player);
        elseif ($this->isPlayerReady($player) === null)
            $this->setPlayerReady($player, false, false);

        return true;
    }

    private function freeAllPlayers()
    {
        $players = $this->maniaControl->getPlayerManager()->getPlayers();

        $result = true;
        foreach ($players as $player)
        {
            try {
                $this->maniaControl->getClient()->forceSpectator($player->login, PlayerActions::SPECTATOR_USER_SELECTABLE);
            } catch (Exception $e) {
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_EXCEPTION,
                    $e
                );
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    TMMUtils::chatFormatPlayer($player).' did not get freed!'
                );
                $result = false;
            }
        }

        return $result;
    }

    public function handleEndRoundCallback(OnScoresStructure $structure)
    {
        if (!$this->isMatchRunning())
            return;

        $section = $structure->getSection();
        switch ($section)
        {
            case 'PreEndRound':
                $this->checkValidRound($structure);
                break;
            case 'EndRound':
                $this->setScoresOnEndRound($structure);
                break;
            case 'EndMatch':
                $this->setScoresOnEndMatch($structure);
                break;
            default: break;
        }
    }

    public function handlePlayerConnectCallback(Player $player)
    {
        if ($this->isMatchRunning())
        {
            $this->forcePlayer($player);

            $this->toornamentMania->chat(
                ChatMode::INFORMATION,
                'A match is running!',
                $player->login
            );
        }
    }

    public function handlePlayerDisconnectCallback(Player $player)
    {
        $this->unsetPlayerReady($player);
    }

    public function handlePlayerInfoChangedCallback(Player $player)
    {
        if ($this->isMatchRunning())
        {
            if (!$player->isConnected)
                return;

            $this->forcePlayer($player);
        }
    }

    public function handleUpdateScriptSettingCallback(string $setting, $newValue)
    {
        if (!$this->isRunning())
            return true;

        if (!$this->matchDataManager->hasScriptSettings())
            return true;

        $forcedScriptSettings = $this->matchDataManager->getScriptSettings();
        if (!array_key_exists($setting, $forcedScriptSettings))
            return true;

        $forcedValue = $forcedScriptSettings[$setting];
        if ($forcedValue != $newValue)
        {
            try {
                $this->maniaControl->getClient()->setModeScriptSettings(array(
                    $setting => $forcedValue
                ));

                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    'Cannot change forced script settings when a match is running!'
                );
            } catch (Exception $e) {
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_EXCEPTION,
                    $e
                );
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not force back script settings!'
                );

                return false;
            }
        }

        return true;
    }

    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this))
            return true;
    }

    public function isGameRunning()
    {
        return $this->matchDataManager->hasCurrentGame();
    }

    public function isMatchRunning()
    {
        return $this->matchDataManager->isMatchRunning();
    }

    public function isPlayerReady(Player $player)
    {
        if (!array_key_exists($player->login, $this->playerReady))
            return null;

        return $this->playerReady[$player->login];
    }

    public function isRunning()
    {
        return $this->matchDataManager->hasGames(false) === true
             ? $this->isGameRunning()
             : $this->isMatchRunning();
    }

    public function loadMatch(string $url = '')
    {
        if ($this->isMatchRunning())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'A match is already running!'
            );
            return false;
        }

        if (!$this->accessManager->hasAllKeys())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Not all Keys given to connect to Toornament API!'
            );
            return false;
        }

        $client = $this->accessManager->connectOrganizer();
        if ($client === null)
            return false;

        $this->matchDataManager->setOrganizerClient($client);

        if ($url != '')
            $this->accessManager->setUrl($url);

        if (!$this->accessManager->hasUrl())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'No URL given to load match from!'
            );
            return false;
        }

        $url = $this->accessManager->getUrl();
        list($tournamentId, $matchId) = Models\Match::parseUrl($url);
        if (!$this->matchDataManager->load($tournamentId, $matchId))
            return false;

        $this->toornamentMania->chat(
            ChatMode::INFORMATION,
            'Match loaded!'
        );

        if ($this->matchDataManager->hasGames())
        {
            if (!$this->startMatch())
            {
                $this->unloadMatch();
                return false;
            }
        }

        $players = $this->maniaControl->getPlayerManager()->getPlayers();
        foreach ($players as $player)
            $this->forcePlayer($player);

        $this->enableReadyLobby();
        return true;
    }

    public function loadScript($scriptSettings = null, $scriptName = null, $scriptText = null)
    {
        $response = $this->prepareLoadScript($scriptSettings, $scriptName, $scriptText);
        if ($response === false)
            return false;

        list($scriptSettings, $scriptName, $scriptText) = $response;

        try {
            TMMUtils::loadScript(
                $this->maniaControl,
                $this->getLoadScriptDelay(),
                $scriptName,
                $scriptSettings,
                $scriptText
            );
        } catch (Exception $e) {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not load script!'
            );

            return false;
        }

        return true;
    }

    public function log(string $message)
    {
        if (!$this->matchDataManager->isMatchLoaded())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'No match loaded to log something in!'
            );
            return false;
        }

        $timestamp = '['.date('c').']';
        $prevPrivateNote = $this->matchDataManager->getMatch()->private_note;
        if ($prevPrivateNote != '')
            $prevPrivateNote = $prevPrivateNote."\n";

        $matchUpdates = array(
            Models\Match::PRIVATE_NOTE => $prevPrivateNote.$timestamp.' '.$message
        );

        return $this->matchDataManager->updateMatch($matchUpdates);
    }

    private function prepareLoadScript($scriptSettings = null, $scriptName = null, $scriptText = null)
    {
        $scriptSettings  = $scriptSettings
                        ?? $this->matchDataManager->getScriptSettings()
                        ?? $this->maniaControl->getClient()->getModeScriptSettings();
        if (!is_array($scriptSettings))
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Script Settings not given in array!'
            );
            return false;
        }

        $scriptName  = $scriptName
                    ?? $this->matchDataManager->getScriptName()
                    ?? $this->maniaControl->getClient()->getScriptName();
        if (!is_string($scriptName))
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'No Script Name given!'
            );
            return false;
        }

        if ($scriptText === null)
        {
            $scriptPath = $this->scriptsDir.$scriptName;
            if (!file_exists($scriptPath))
            {
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    'Script not found ('.$scriptPath.').'
                );

                return false;
            }

            $scriptText = file_get_contents($scriptPath);
        }

        return array($scriptSettings, $scriptName, $scriptText);
    }

    public function setPlayerReady(Player $player, bool $ready, bool $playerReadyMessage = true)
    {
        $prevAllPlayersReady = $this->checkAllPlayersReady();

        $this->playerReady[$player->login] = $ready;

        if ($playerReadyMessage)
        {
            if ($this->playerReady[$player->login])
            {
                $this->toornamentMania->chat(
                    ChatMode::SUCCESS,
                    TMMUtils::chatFormatPlayer($player).' is ready!'
                );
            }
            else
            {
                $this->toornamentMania->chat(
                    ChatMode::ERROR,
                    TMMUtils::chatFormatPlayer($player).' is not ready!'
                );
            }
        }
        $this->toornamentMania->buttonReadyUpdate($player);

        $allPlayersReady = $this->checkAllPlayersReady();
        if ($allPlayersReady)
        {
            $startMatchDelay = $this->getStartMatchDelay();
            $this->toornamentMania->chat(
                ChatMode::INFORMATION,
                'All players ready, starting in '.$startMatchDelay.' seconds!'
            );
            $this->maniaControl->getTimerManager()->registerOneTimeListening(
                $this,
                'start',
                $startMatchDelay*1000
            );
        }
        elseif ($prevAllPlayersReady)
        {
            $success = $this->maniaControl->getTimerManager()->unregisterTimerListening(
                $this,
                'start'
            );
            if ($success)
            {
                $this->toornamentMania->chat(
                    ChatMode::INFORMATION,
                    'Start aborted!'
                );
            }
            else
            {
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not abort start!'
                );
            }
        }
    }

    public function setScoresFromToornament()
    {
        if (!$this->isRunning())
            return;

        $scores = MatchScoreParser::toornamentToTrackmania($this->matchDataManager, $this->maniaControl);

        if ($this->maniaControl->getServer()->isTeamMode())
        {
            foreach ($scores as $teamId => $score)
                $this->maniaControl->getModeScriptEventManager()->setTrackmaniaTeamPoints($teamId, $score, $score, $score);
        }
        else
        {
            foreach ($scores as $login => $score)
            {
                $player = $this->maniaControl->getPlayerManager()->getPlayer($login);
                if ($player === null)
                    continue;

                assert($player instanceof Player);
                $this->maniaControl->getModeScriptEventManager()->setTrackmaniaPlayerPoints($player, $score, $score, $score);
            }
        }
    }

    private function setScoresFromToornamentOnCallback($callback)
    {
        assert($callback != null);

        $this->maniaControl->getCallbackManager()->registerCallbackListener(
            $callback,
            $this,
            function () use ($callback) {
                $this->maniaControl->getCallbackManager()->unregisterCallbackListening(
                    $callback,
                    $this
                );
                $this->setScoresFromToornament();
            }
        );
    }

    public function setScoresOnEndRound(OnScoresStructure $structure)
    {
        if (!$this->isRunning())
            return null;

        if (!$this->validRound)
        {
            $this->validRound = true;
            return false;
        }

        $scores = MatchScoreParser::trackmaniaToToornament($structure, $this->matchDataManager);

        if ($this->matchDataManager->hasGames())
            $this->matchDataManager->updateGame($scores);
        else
            $this->matchDataManager->updateMatch($scores);

        return true;
    }

    public function setScoresOnEndMatch(OnScoresStructure $structure)
    {
        if (!$this->isRunning())
            return null;

        $results = MatchResultParser::trackmaniaToToornament($structure, $this->matchDataManager);

        if ($this->matchDataManager->hasGames())
            $this->completeGame($results, $structure);
        else
            $this->completeMatch($results);

        return true;
    }

    public function start()
    {
        $success = $this->matchDataManager->hasGames()
                 ? $this->startGame()
                 : $this->startMatch();

        if ($success)
        {
            $this->disableReadyLobby();
            $this->setScoresFromToornamentOnCallback(Callbacks::MP_STARTROUNDSTART);
        }

        return $success;
    }

    private function startGame()
    {
        if (!$this->isMatchRunning())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot start game, no match running!'
            );
            return false;
        }

        return $this->matchDataManager->startGame() && $this->loadScript();
    }

    private function startMatch()
    {
        return $this->matchDataManager->startMatch() && $this->loadScript();
    }

    public function togglePlayerReady(Player $player)
    {
        if (!array_key_exists($player->login, $this->playerReady))
            return;

        $this->setPlayerReady($player, !$this->isPlayerReady($player));
    }

    public function unloadMatch(bool $removeUrl = true)
    {
        if ($removeUrl)
            $this->accessManager->removeUrl();

        $this->disableReadyLobby();
        $this->matchDataManager->unload();
        $this->freeAllPlayers();
    }

    public function unsetPlayerReady(Player $player)
    {
        unset($this->playerReady[$player->login]);
        $this->toornamentMania->buttonReadyHide($player);
    }
}
