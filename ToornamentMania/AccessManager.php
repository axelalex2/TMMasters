<?php

namespace TMMasters\ToornamentMania;

use Maniaplanet\DedicatedServer\Xmlrpc\Exception;
use TMMasters\ChatMode;
use TMMasters\Toornament\V2\OrganizerClient;
use TMMasters\Toornament\V2\ViewerClient;
use TMMasters\ToornamentMania;

final class AccessManager
{
    /**
     * Constants
     */
    const TABLE_TOORNAMENTMANIA_KEYS              = '`mc_toornamentmania_keys`';
    const TABLE_TOORNAMENTMANIA_KEYS_APIKEY       = '`api_key`';
    const TABLE_TOORNAMENTMANIA_KEYS_CLIENTID     = '`client_id`';
    const TABLE_TOORNAMENTMANIA_KEYS_CLIENTSECRET = '`client_secret`';
    const TABLE_TOORNAMENTMANIA_KEYS_PASSWORD     = '`password`';
    const TABLE_TOORNAMENTMANIA_URLS              = '`mc_toornamentmania_urls`';
    const TABLE_TOORNAMENTMANIA_URLS_SERVERINDEX  = '`serverIndex`';
    const TABLE_TOORNAMENTMANIA_URLS_URL          = '`url`';

    /**
     * Private properties
     */
    private $mysqli          = null;
    private $serverIndex     = null;
    private $toornamentMania = null;

    public function __construct(ToornamentMania $toornamentMania)
    {
        $this->toornamentMania = $toornamentMania;
        $maniaControl = $this->toornamentMania->getManiaControl();
        $this->mysqli = $maniaControl->getDatabase()->getMysqli();
        $this->serverIndex = $maniaControl->getServer()->index;
        $this->initTables();
    }

    public function checkKeyPassword(string $password)
    {
        if (!$this->hasPassword())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'No password set to check against!'
            );
            return null;
        }

        $keys = $this->getKeys();
        $query = "SELECT PASSWORD('".$password."') AS `password`;";
        $result = $this->mysqli->query($query);
        if ($this->mysqli->error || !$result)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $this->mysqli->error
            );
            return null;
        }

        $passwordEncrypted = $result->fetch_object()->password;
        $result->free();

        return ($passwordEncrypted === $keys->password);
    }

    public function connectOrganizer()
    {
        $keys = $this->getKeys();
        if ($keys === null)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'No keys set to connect to toornament.com!'
            );
            return null;
        }

        try {
            return new OrganizerClient(
                $keys->api_key,
                $keys->client_id,
                $keys->client_secret
            );
        } catch (\Exception $e) {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not connect to toornament.com!'
            );

            return null;
        }
    }

    public function connectViewer()
    {
        $keys = $this->getKeys();
        if ($keys === null)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'No keys set to connect to toornament.com!'
            );
            return null;
        }

        try {
            return new ViewerClient(
                $keys->api_key
            );
        } catch (\Exception $e) {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not connect to toornament.com!'
            );

            return null;
        }
    }

    public function getKeys()
    {
        $query = "SELECT * FROM ".self::TABLE_TOORNAMENTMANIA_KEYS.";";
        $result = $this->mysqli->query($query);
        if ($this->mysqli->error || !$result)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $this->mysqli->error
            );
            return null;
        }

        $rows = array();
        while ($row = $result->fetch_object())
            array_push($rows, $row);

        $result->free();
        $numRows = count($rows);
        assert($numRows <= 1);
        return $rows[0] ?? null;
    }

    public function getUrl()
    {
        $query = "SELECT ".self::TABLE_TOORNAMENTMANIA_URLS_URL."
            FROM ".self::TABLE_TOORNAMENTMANIA_URLS."
            WHERE ".self::TABLE_TOORNAMENTMANIA_URLS_SERVERINDEX." = ".$this->serverIndex.";";
        $result = $this->mysqli->query($query);
        if ($this->mysqli->error || !$result)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $this->mysqli->error
            );
            return null;
        }

        $rows = array();
        while ($row = $result->fetch_object())
            array_push($rows, $row);

        $result->free();
        $numRows = count($rows);
        assert($numRows <= 1);
        return $rows[0]->url ?? null;
    }

    public function hasAllKeys()
    {
        $keys = $this->getKeys();
        if (!is_object($keys))
            return false;

        $allKeys = ($keys->api_key != '' && $keys->client_id != '' && $keys->client_secret != '');
        return $allKeys;
    }

    public function hasAnyKey()
    {
        $keys = $this->getKeys();
        if (!is_object($keys))
            return false;

        $anyKey = ($keys->api_key != '' || $keys->client_id != '' || $keys->client_secret != '');
        return $anyKey;
    }

    public function hasPassword()
    {
        $keys = $this->getKeys();
        if (!is_object($keys))
            return false;

        $hasPassword = (is_string($keys->password) && $keys->password != '');
        if ($hasPassword)
            assert($this->hasAnyKey());

        return $hasPassword;
    }

    public function hasUrl()
    {
        $url = $this->getUrl();
        return is_string($url) && $url != '';
    }

    private function initTables()
    {
        $query = "CREATE TABLE IF NOT EXISTS ".self::TABLE_TOORNAMENTMANIA_KEYS." (
            ".self::TABLE_TOORNAMENTMANIA_KEYS_APIKEY      ." varchar(43) NOT NULL,
            ".self::TABLE_TOORNAMENTMANIA_KEYS_CLIENTID    ." varchar(74) NOT NULL,
            ".self::TABLE_TOORNAMENTMANIA_KEYS_CLIENTSECRET." varchar(50) NOT NULL,
            ".self::TABLE_TOORNAMENTMANIA_KEYS_PASSWORD    ." varchar(41) NOT NULL
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";

        $this->mysqli->query($query);
        if ($this->mysqli->error)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $this->mysqli->error
            );
            return false;
        }

        $query = "CREATE TABLE IF NOT EXISTS ".self::TABLE_TOORNAMENTMANIA_URLS." (
            ".self::TABLE_TOORNAMENTMANIA_URLS_SERVERINDEX." int(11)     NOT NULL,
            ".self::TABLE_TOORNAMENTMANIA_URLS_URL        ." varchar(86) NOT NULL,
            UNIQUE KEY (".self::TABLE_TOORNAMENTMANIA_URLS_SERVERINDEX.")
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;";

        $this->mysqli->query($query);
        if ($this->mysqli->error)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $this->mysqli->error
            );
            return false;
        }

        return true;
    }

    public function removeKeys()
    {
        if ($this->hasUrl())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot remove keys when a URL is stored!'
            );
            return false;
        }

        $query = "TRUNCATE TABLE ".self::TABLE_TOORNAMENTMANIA_KEYS.";";

        $this->mysqli->query($query);
        if ($this->mysqli->error)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $this->mysqli->error
            );
            return false;
        }

        return true;
    }

    public function removeUrl()
    {
        $query = "DELETE FROM ".self::TABLE_TOORNAMENTMANIA_URLS."
            WHERE ".self::TABLE_TOORNAMENTMANIA_URLS_SERVERINDEX." = ".$this->serverIndex.";";
        
        $this->mysqli->query($query);
        if ($this->mysqli->error)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $this->mysqli->error
            );
            return false;
        }

        return true;
    }

    public function setKeys(string $apiKey, string $clientId, string $clientSecret, string $password)
    {
        if (($apiKey != '' || $clientId != '' || $clientSecret != '') && $password === '')
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Keys need to be secured by a password!'
            );
            return false;
        }

        if ($this->hasAnyKey())
            $this->removeKeys();

        $query = "INSERT INTO ".self::TABLE_TOORNAMENTMANIA_KEYS." (
            ".self::TABLE_TOORNAMENTMANIA_KEYS_APIKEY.",
            ".self::TABLE_TOORNAMENTMANIA_KEYS_CLIENTID.",
            ".self::TABLE_TOORNAMENTMANIA_KEYS_CLIENTSECRET.",
            ".self::TABLE_TOORNAMENTMANIA_KEYS_PASSWORD.")
            VALUES (
                '".$apiKey."',
                '".$clientId."',
                '".$clientSecret."',
                ".($password == '' ? "''" : "PASSWORD('".$password."')")."
            );";
        
        $this->mysqli->query($query);
        if ($this->mysqli->error)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $this->mysqli->error
            );
            return false;
        }

        return true;
    }

    public function setUrl(string $url)
    {
        if (substr($url, -1) == '/')
            $url = substr($url, 0, -1);

        $query = "INSERT INTO ".self::TABLE_TOORNAMENTMANIA_URLS." (
            ".self::TABLE_TOORNAMENTMANIA_URLS_SERVERINDEX.",
            ".self::TABLE_TOORNAMENTMANIA_URLS_URL.")
            VALUES (
                ".$this->serverIndex.",
                '".$url."'
            ) ON DUPLICATE KEY UPDATE
            ".self::TABLE_TOORNAMENTMANIA_URLS_URL." = VALUES(".self::TABLE_TOORNAMENTMANIA_URLS_URL.");";
        
        $this->mysqli->query($query);
        if ($this->mysqli->error)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                $this->mysqli->error
            );
            return false;
        }

        return true;
    }
}
