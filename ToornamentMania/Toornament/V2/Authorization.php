<?php

namespace TMMasters\Toornament\V2;

final class Authorization
{
    const CLIENT_ID     = 'client_id';
    const CLIENT_SECRET = 'client_secret';
    const GRANT_TYPE    = 'grant_type';
    const SCOPE         = 'scope';
}
