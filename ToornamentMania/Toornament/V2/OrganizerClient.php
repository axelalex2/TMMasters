<?php

namespace TMMasters\Toornament\V2;

use \TMMasters\Toornament\ApiCalls;
use \TMMasters\Toornament\ApiUser;
use \TMMasters\Toornament\RestRequest;

class OrganizerClient extends Client
{
    public function __construct(string $apiKey, string $clientID, string $clientSecret)
    {
        if ($apiKey === '' || $clientID === '' || $clientSecret === '')
            throw new \Exception('Cannot create toornament organizer client without API Key, Client ID or Client secret!');

        parent::__construct($apiKey, ApiUser::ORGANIZER, $clientID, $clientSecret);
        // TODO throwaway call to check correctness of apiKey
    }

    public function getTourmaments(array $queryParams)
    {
        return parent::send(
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS),
            Authorization\Scope::ORGANIZER_VIEW,
            $queryParams
        );
    }

    public function createTournament(array $requestBody)
    {
        return parent::send(
            RestRequest::POST,
            array(ApiCalls::TOURNAMENTS),
            Authorization\Scope::ORGANIZER_ADMIN,
            $requestBody
        );
    }

    public function getTournamentById(string $tournamentId)
    {
        return parent::send(
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS, $tournamentId),
            Authorization\Scope::ORGANIZER_VIEW
        );
    }

    public function updateTournament(string $tournamentId, array $requestBody)
    {
        return parent::send(
            RestRequest::PATCH,
            array(ApiCalls::TOURNAMENTS, $tournamentId),
            Authorization\Scope::ORGANIZER_ADMIN,
            $requestBody
        );
    }

    public function deleteTournament(string $tournamentId)
    {
        return parent::send(
            RestRequest::DELETE,
            array(ApiCalls::TOURNAMENTS, $tournamentId),
            Authorization\Scope::ORGANIZER_DELETE
        );
    }

    public function getParticipantById(string $tournamentId, string $stageId)
    {
        return parent::send(
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::PARTICIPANTS, $stageId),
            Authorization\Scope::ORGANIZER_PARTICIPANT
        );
    }

    public function getMatches(string $tournamentId, array $queryParams)
    {
        return parent::send(
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::MATCHES),
            Authorization\Scope::ORGANIZER_RESULT,
            $queryParams
        );
    }

    public function getMatchById(string $tournamentId, string $matchId)
    {
        return parent::send(
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::MATCHES, $matchId),
            Authorization\Scope::ORGANIZER_RESULT
        );
    }

    public function updateMatch(string $tournamentId, string $matchId, array $requestBody)
    {
        return parent::send(
            RestRequest::PATCH,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::MATCHES, $matchId),
            Authorization\Scope::ORGANIZER_RESULT,
            $requestBody
        );
    }

    public function getGames(string $tournamentId, string $matchId)
    {
        return parent::pagination(
            ApiCalls::GAMES,
            50,
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::MATCHES, $matchId, ApiCalls::GAMES),
            Authorization\Scope::ORGANIZER_RESULT
        );
    }

    public function getGameByNumber(string $tournamentId, string $matchId, $gameNumber)
    {
        return parent::send(
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::MATCHES, $matchId, ApiCalls::GAMES, $gameNumber),
            Authorization\Scope::ORGANIZER_RESULT
        );
    }

    public function updateGame(string $tournamentId, string $matchId, string $gameNumber, array $requestBody)
    {
        return parent::send(
            RestRequest::PATCH,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::MATCHES, $matchId, ApiCalls::GAMES, $gameNumber),
            Authorization\Scope::ORGANIZER_RESULT,
            $requestBody
        );
    }
}
