<?php

namespace TMMasters\Toornament\V2;

use \TMMasters\Toornament\ApiCalls;
use \TMMasters\Toornament\ApiUser;
use \TMMasters\Toornament\RestRequest;

final class ViewerClient extends Client
{
    public function __construct(string $apiKey)
    {
        if ($apiKey === '')
            throw new \Exception('Cannot create toornament viewer client without API Key!');

        parent::__construct($apiKey, ApiUser::VIEWER, '', '');
        // TODO throwaway call to check correctness of apiKey
    }

    public function getStageById(string $tournamentId, string $stageId)
    {
        return parent::send(
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::STAGES, $stageId)
        );
    }

    public function getGroupById(string $tournamentId, string $groupId)
    {
        return parent::send(
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::GROUPS, $groupId)
        );
    }

    public function getRoundById(string $tournamentId, string $roundId)
    {
        return parent::send(
            RestRequest::GET,
            array(ApiCalls::TOURNAMENTS, $tournamentId, ApiCalls::ROUNDS, $roundId)
        );
    }
}
