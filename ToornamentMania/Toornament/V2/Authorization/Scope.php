<?php

namespace TMMasters\Toornament\V2\Authorization;

final class Scope
{
    const ORGANIZER_ADMIN       = 'organizer:admin';
    const ORGANIZER_DELETE      = 'organizer:delete';
    const ORGANIZER_PARTICIPANT = 'organizer:participant';
    const ORGANIZER_RESULT      = 'organizer:result';
    const ORGANIZER_VIEW        = 'organizer:view';
}
