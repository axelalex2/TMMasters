<?php

namespace TMMasters\Toornament\V2\Authorization;

final class GrantType
{
    const CLIENT_CREDENTIALS = 'client_credentials';
}
