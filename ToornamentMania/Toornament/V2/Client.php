<?php

namespace TMMasters\Toornament\V2;

use \TMMasters\Toornament\RestRequest;
use \TMMasters\Toornament\RestResponse;

function addHttpHeader(array &$httpHeader, string $key, string $value)
{
    array_push($httpHeader, $key.': '.$value);
}

function parseHttpHeaderProperty(string $httpHeaderProperty)
{
    $pos = strpos($httpHeaderProperty, ': ');
    if ($pos === false)
        throw new \Exception('Invalid Http-Header property "'.$httpHeaderProperty.'"!');
    $key = substr($httpHeaderProperty, 0, $pos);
    $value = substr($httpHeaderProperty, $pos+2);
    return array($key, $value);
}

function parseHttpHeaderProperties(string $httpHeaders)
{   
    $httpHeaderProperties = array_filter(explode("\r\n", $httpHeaders));
    // remove the first header, as it is not a property
    array_shift($httpHeaderProperties);

    $associative = array();
    foreach ($httpHeaderProperties as $httpHeaderProperty)
    {
        list($key, $value) = parseHttpHeaderProperty($httpHeaderProperty);
        $associative[$key] = $value;
    }
    return $associative;
}

abstract class Client
{
    const API_URL = 'https://api.toornament.com';
    const API_VERSION = 'v2';

    const HTTP_HEADER_APIKEY        = 'X-Api-Key';
    const HTTP_HEADER_AUTHORIZATION = 'Authorization';
    const HTTP_HEADER_CONTENT_RANGE = 'Content-Range';
    const HTTP_HEADER_RANGE         = 'Range';

    const OAUTH_URL = self::API_URL.'/oauth/'.self::API_VERSION.'/token';

    private $apiKey       = null;
    private $apiUser      = null;
    private $clientID     = null;
    private $clientSecret = null;
    private $oauth        = null;
    private $range        = null;

    protected function __construct(string $apiKey, string $apiUser, string $clientID, string $clientSecret)
    {
        $this->apiKey       = $apiKey;
        $this->apiUser      = $apiUser;
        $this->clientID     = $clientID;
        $this->clientSecret = $clientSecret;
        $this->oauth        = array();
    }

    private function authorizeClientCredentials(string $scope)
    {
        // if there is a access_token, only refresh if expired
        if (array_key_exists($scope, $this->oauth)
         && $this->oauth[$scope]->expires_in >= time())
            return;

        $requestBody = array(
            Authorization::GRANT_TYPE    => Authorization\GrantType::CLIENT_CREDENTIALS,
            Authorization::CLIENT_ID     => $this->clientID,
            Authorization::CLIENT_SECRET => $this->clientSecret,
            Authorization::SCOPE         => $scope
        );
        $response = $this->send(RestRequest::POST, self::OAUTH_URL, '', http_build_query($requestBody));

        if (isset($response->error))
        {
            var_dump($response);
            throw new \Exception('Authorization Error "'.$response->error.'": '.$response->message.' ('.$response->hint.')');
        }

        $response->expires_in += time();
        $this->oauth[$scope] = $response;
    }

    protected function pagination(string $rangeDesc, int $rangeMax, string $request, $url, string $scope = '', $requestBody = null)
    {
        $response = array();
        $from = 0;
        $this->range = '';
        while ($this->range !== null)
        {
            $to = $from + $rangeMax - 1;
            $this->range = $rangeDesc.'='.$from.'-'.$to;

            $pageResponse = $this->send($request, $url, $scope, $requestBody);
            $response = array_merge($response, $pageResponse);

            $from += $rangeMax;
        }

        return $response;
    }

    protected function send(string $request, $url, string $scope = '', $requestBody = null)
    {
        if (is_array($url))
        {
            array_unshift($url, self::API_URL, $this->apiUser, self::API_VERSION);
            $url = implode('/', $url);
        }

        $httpHeader = array();
        addHttpHeader($httpHeader, self::HTTP_HEADER_APIKEY, $this->apiKey);
        
        if ($scope != '')
        {
            $this->authorizeClientCredentials($scope);
            $oauth = $this->oauth[$scope];
            $oauthStr = $oauth->token_type.' '.$oauth->access_token;
            addHttpHeader($httpHeader, self::HTTP_HEADER_AUTHORIZATION, $oauthStr);
        }

        if ($this->range !== null && $this->range !== '' && $url !== self::OAUTH_URL)
            addHttpHeader($httpHeader, self::HTTP_HEADER_RANGE, $this->range);

        if (is_array($requestBody))
            $requestBody = json_encode($requestBody);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $request);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        
        $response = curl_exec($curl);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = parseHttpHeaderProperties(substr($response, 0, $header_size));
        $body = substr($response, $header_size);
        $info = curl_getinfo($curl);

        if ($body === false || $body === '')
        {
            var_dump($info);
            var_dump(curl_error($curl));
            throw new \Exception('Request Error ('.$info['http_code'].'), see Logs for details!');
        }
        curl_close($curl);

        $body = json_decode($body);
        if (isset($info->status) && $info->status === RestResponse::ERROR)
        {
            var_dump($header);
            var_dump($body);
            throw new \Exception('Response Error ('.$info['http_code'].'): "' . $info->errormessage . '", see Logs for details!');
        }

        if ($this->range !== null && $this->range !== '')
        {
            $contentRange = explode(' ', $header[self::HTTP_HEADER_CONTENT_RANGE]);
            assert(count($contentRange) == 2);

            $contentRange = explode('/', $contentRange[1]);
            assert(count($contentRange) == 2);
            $total = intval($contentRange[1]);
            if ($total === 0)
            {
                $this->range = null;
                assert($contentRange[0] == '*');
            }
            else
            {
                $contentRange = explode('-', $contentRange[0]);
                assert(count($contentRange) == 2);
                $to = intval($contentRange[1]);

                if ($to === $total-1)
                    $this->range = null;
            }
        }

        return $body;
    }
}
