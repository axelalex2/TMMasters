<?php

namespace TMMasters\Toornament;

final class RestRequest
{
    const DELETE = 'DELETE';
    const GET    = 'GET';
    const PATCH  = 'PATCH';
    const POST   = 'POST';
}
