<?php

namespace TMMasters\Toornament;

final class ApiCalls
{
    const GAMES        = 'games';
    const GROUPS       = 'groups';
    const MATCHES      = 'matches';
    const PARTICIPANTS = 'participants';
    const ROUNDS       = 'rounds';
    const STAGES       = 'stages';
    const TOURNAMENTS  = 'tournaments';
}
