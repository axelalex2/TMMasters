<?php

namespace TMMasters\Toornament;

final class ApiUser
{
    const ACCOUNT     = 'account';
    const ORGANIZER   = 'organizer';
    const PARTICIPANT = 'participant';
    const VIEWER      = 'viewer';
}
