<?php

namespace TMMasters\Toornament\Models;

final class Tournament
{
    public static function isSingleTournament(object $tournament)
    {
        return $tournament->participant_type == Tournament\ParticipantType::SINGLE;
    }

    public static function isTeamTournament(object $tournament)
    {
        return $tournament->participant_type == Tournament\ParticipantType::TEAM;
    }
}
