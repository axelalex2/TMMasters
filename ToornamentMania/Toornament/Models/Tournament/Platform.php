<?php

namespace TMMasters\Toornament\Models\Tournament;

final class Platform
{
    const PC              = 'pc';
    const PLAYSTATION4    = 'playstation4';
    const XBOX_ONE        = 'xbox_one';
    const NINTENDO_SWITCH = 'nintendo_switch';
    const MOBILE          = 'mobile';
    const PLAYSTATION3    = 'playstation3';
    const PLAYSTATION2    = 'playstation2';
    const PLAYSTATION1    = 'playstation1';
    const PS_VITA         = 'ps_vita';
    const PSP             = 'psp';
    const XBOX360         = 'xbox360';
    const XBOX            = 'xbox';
    const WII_U           = 'wii_u';
    const WII             = 'wii';
    const GAMECUBE        = 'gamecube';
    const NINTENDO64      = 'nintendo64';
    const SNES            = 'snes';
    const NES             = 'nes';
    const DREAMCAST       = 'dreamcast';
    const SATURN          = 'saturn';
    const MEGADRIVE       = 'megadrive';
    const MASTER_SYSTEM   = 'master_system';
    const _3DS            = '3ds';
    const DS              = 'ds';
    const GAME_BOY        = 'game_boy';
    const NEO_GEO         = 'neo_geo';
    const OTHER_PLATFORM  = 'other_platform';
    const NOT_VIDEO_GAME  = 'not_video_game';
}
