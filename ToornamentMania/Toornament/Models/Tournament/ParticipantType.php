<?php

namespace TMMasters\Toornament\Models\Tournament;

final class ParticipantType
{
    const SINGLE = 'single';
    const TEAM   = 'team';
}
