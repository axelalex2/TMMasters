<?php

namespace TMMasters\Toornament\Models;

final class Match
{
    const GROUP_ID            = 'group_id';
    const ID                  = 'id';
    const NUMBER              = 'number';
    const PLAYED_AT           = 'played_at';
    const PRIVATE_NOTE        = 'private_note';
    const PUBLIC_NOTE         = 'public_note';
    const ROUND_ID            = 'round_id';
    const SCHEDULED_DATE_TIME = 'scheduled_date_time';
    const SETTINGS            = 'settings';
    const STAGE_ID            = 'stage_id';
    const TYPE                = 'type';

    public static function parseUrl(string $url)
    {
        if (substr($url, -1) == '/')
            $url = substr($url, 0, -1);

        $urlExplode = explode('/', $url);
        if (count($urlExplode) != 7
         || $urlExplode[0] != 'https:'
         || $urlExplode[1] != ''
         || $urlExplode[2] != 'www.toornament.com'
         || $urlExplode[3] != 'tournaments'
         || $urlExplode[5] != 'matches')
        {
            throw new \Exception('Weblink of toornament match is incomplete!');
        }

        return array($urlExplode[4], $urlExplode[6]);
    }
}
