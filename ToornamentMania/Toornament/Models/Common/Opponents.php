<?php

namespace TMMasters\Toornament\Models\Common;

final class Opponents
{
    const NUMBER = 'number';
    const RANK   = 'rank';
    const RESULT = 'result';
    const SCORE  = 'score';
}
