<?php

namespace TMMasters\Toornament\Models\Common;

final class Result
{
    const DRAW = 'draw';
    const LOSS = 'loss';
    const WIN  = 'win';
}
