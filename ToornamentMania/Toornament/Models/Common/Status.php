<?php

namespace TMMasters\Toornament\Models\Common;

final class Status
{
    const PENDING   = 'pending';
    const RUNNING   = 'running';
    const COMPLETED = 'completed';
}
