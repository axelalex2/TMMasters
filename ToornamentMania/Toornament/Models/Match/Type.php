<?php

namespace TMMasters\Toornament\Models\Match;

final class Type
{
    const BYE  = 'bye';
    const DUEL = 'duel';
    const FFA  = 'ffa';
}
