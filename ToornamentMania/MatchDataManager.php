<?php

namespace TMMasters\ToornamentMania;

use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerActions;
use \TMMasters\ChatMode;
use \TMMasters\Toornament\Models;
use \TMMasters\Toornament\V2\OrganizerClient;
use \TMMasters\Toornament\V2\ViewerClient;
use \TMMasters\ToornamentMania;

final class MatchDataManager
{
    private $client            = null;
    private $currentGameNumber = null;
    private $games             = null;
    private $match             = null;
    private $participants      = null;
    private $scriptName        = null;
    private $scriptSettings    = null;
    private $toornamentMania   = null;
    private $tournament        = null;

    public function __construct(ToornamentMania $toornamentMania)
    {
        $this->toornamentMania = $toornamentMania;
    }

    public function allGamesCompleted()
    {
        if (!$this->hasGames())
            return null;

        foreach ($this->games as $game)
        {
            if ($game->status != Models\Common\Status::COMPLETED)
                return false;
        }

        return true;
    }

    private function checkMatch()
    {
        assert($this->match != null);

        if ($this->match->type === Models\Match\Type::BYE)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot play a bye!'
            );
            return false;
        }

        if ($this->match->status === Models\Common\Status::COMPLETED)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Match is already completed!'
            );
            return false;
        }

        return true;
    }

    private function checkParticipants()
    {
        assert(is_array($this->participants));

        $result = true;
        foreach ($this->participants as $participantId => $participant)
        {
            $lineup = $participant->lineup ?? array($participant);
            foreach ($lineup as $player)
            {
                if (!isset($player->custom_fields->maniaplanet_id))
                {
                    $this->toornamentMania->chat(
                        ChatMode::ADMIN_ERROR,
                        'Maniaplanet ID of player "'.$player->name.'" not available!'
                    );
                    $result = false;
                    continue;
                }

                if (!is_string($player->custom_fields->maniaplanet_id))
                {
                    $this->toornamentMania->chat(
                        ChatMode::ADMIN_ERROR,
                        'Maniaplanet ID of player "'.$player->name.'" not encoded correctly!'
                    );
                    $result = false;
                    continue;
                }

                if ($player->custom_fields->maniaplanet_id === '')
                {
                    $this->toornamentMania->chat(
                        ChatMode::ADMIN_ERROR,
                        'Maniaplanet ID of player "'.$player->name.'" is empty!'
                    );
                    $result = false;
                    continue;
                }
            }
        }

        return $result;
    }

    private function checkTournament()
    {
        assert($this->tournament != null);

        if ($this->tournament->status == Models\Common\Status::COMPLETED)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Tournament is already completed!'
            );
            return false;
        }

        $valid_disciplines = array(
            'trackmania2canyon',
            'trackmania2lagoon',
            'trackmania2stadium',
            'trackmania2valley'
        );
        if (!in_array($this->tournament->discipline, $valid_disciplines))
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Invalid discipline "'.$this->tournament->discipline.'"!'
            );
            return false;
        }

        if ($this->tournament->platforms == Models\Tournament\Platform::PC)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Platform "PC" should be the only platform of the tournament!'
            );
            return false;
        }

        if ($this->tournament->archived)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Tournament is archived!'
            );
            return false;
        }

        return true;
    }

    public function completeGame(array $gameUpdates)
    {
        $gameUpdates[Models\Common::STATUS] = Models\Common\Status::COMPLETED;

        $success = $this->updateGame($gameUpdates);
        $this->currentGameNumber = null;
        return $success;
    }

    public function completeMatch(array $matchUpdates)
    {
        $matchUpdates[Models\Match::PLAYED_AT] = date('c');
        $matchUpdates[Models\Common::STATUS] = Models\Common\Status::COMPLETED;

        $success = $this->updateMatch($matchUpdates, false, false);
        $this->unload();
        return $success;
    }

    public function getCurrentGame()
    {
        if (!is_array($this->games))
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot get current game, no games loaded!'
            );
            return null;
        }

        if (empty($this->games))
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot get current game, match has no games!'
            );
            return null;
        }

        if (!is_int($this->currentGameNumber))
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot get current game, no game loaded!'
            );
            return null;
        }

        assert(array_key_exists($this->currentGameNumber, $this->games));
        return $this->games[$this->currentGameNumber];
    }

    public function getCurrentGameNumber()
    {
        return $this->currentGameNumber;
    }

    public function getMatch()
    {
        return $this->match;
    }

    public function getNbParticipants()
    {
        $nbParticipants = count($this->participants);
        assert(count($this->match->opponents) == $nbParticipants);
        return $nbParticipants;
    }

    public function getParticipants()
    {
        return $this->participants;
    }

    public function getPlayerMode(Player $player)
    {
        if (!$this->isMatchLoaded())
            return null;

        foreach ($this->match->opponents as $opponent)
        {
            if ($opponent->participant === null)
                continue;

            assert(array_key_exists($opponent->participant->id, $this->participants));

            $teamIndex = $opponent->number-1;
            $participant = $this->participants[$opponent->participant->id];

            $lineup = $participant->lineup ?? array($participant);
            foreach ($lineup as $lineupPlayer)
            {
                assert(isset($lineupPlayer->custom_fields->maniaplanet_id));
                
                if ($lineupPlayer->custom_fields->maniaplanet_id == $player->login)
                {
                    return array(
                        PlayerActions::SPECTATOR_USER_SELECTABLE,
                        $teamIndex
                    );
                }
            }
        }

        return array(
            PlayerActions::SPECTATOR_SPECTATOR,
            -1
        );
    }

    public function getScriptName()
    {
        return $this->scriptName;
    }

    public function getScriptSettings()
    {
        return $this->scriptSettings;
    }

    public function getTournament()
    {
        return $this->tournament;
    }

    public function hasCurrentGame(bool $enableErrors = false)
    {
        return $this->hasGames($enableErrors) && $this->currentGameNumber != null;
    }

    public function hasGames(bool $enableErrors = false)
    {
        if (!is_array($this->games))
        {
            if ($enableErrors)
            {
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    'Cannot determine if match has games, no games loaded!'
                );
            }
            
            return null;
        }

        return !empty($this->games);
    }

    public function hasMatch()
    {
        return $this->match != null;
    }

    public function hasScriptSettings()
    {
        return is_array($this->scriptSettings) && !empty($this->scriptSettings);
    }

    public function isMatchCompleted()
    {
        if ($this->match === null)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot determine if match is completed, no match loaded!'
            );
            return null;
        }

        return $this->match->status === Models\Common\Status::COMPLETED;
    }

    public function isMatchLoaded()
    {
        return $this->tournament != null
            && $this->match      != null
            && is_array($this->participants)
            && is_array($this->games);
    }

    public function isMatchRunning(bool $enableErrors = false)
    {
        if ($this->match === null)
        {
            if ($enableErrors)
            {
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    'Cannot determine if match is running, no match loaded!'
                );
            }

            return null;
        }

        return $this->match->status === Models\Common\Status::RUNNING;
    }

    public function load(string $tournamentId, string $matchId)
    {
        $this->unload(false);

        $success = $this->loadTournament($tournamentId)
                && $this->loadMatch($matchId)
                && $this->loadMatchSettings()
                && $this->loadParticipants()
                && $this->loadGames();

        if ($success === true)
            return true;

        $this->unload();
        return false;
    }

    public function loadGame()
    {
        if (!$this->hasGames())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot load game, match has no games!'
            );
            return false;
        }

        if ($this->hasCurrentGame())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot load game, a game is already running!'
            );
            return false;
        }

        foreach ($this->games as $game)
        {
            if ($game->status != Models\Common\Status::COMPLETED)
            {
                $this->currentGameNumber = $game->number;
                return true;
            }
        }

        $this->toornamentMania->chat(
            ChatMode::ADMIN_ERROR,
            'Cannot load game, all games are completed!'
        );
        return false;
    }

    private function loadGames(bool $errorOnComplete = true)
    {
        assert($this->client     != null);
        assert($this->tournament != null);
        assert($this->match      != null);

        try {
            $games = $this->client->getGames(
                $this->tournament->id,
                $this->match->id
            );
        } catch (\Exception $e) {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not retrieve game from Toornament!'
            );

            return false;
        }

        $this->games = array();
        foreach ($games as $game)
            $this->games[$game->number] = $game;

        if ($errorOnComplete && $this->allGamesCompleted() === true)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'All games are already completed!'
            );
            return false;
        }

        return true;
    }

    private function loadMatch(string $matchId)
    {
        assert($this->client     != null);
        assert($this->tournament != null);

        try {
            $this->match = $this->client->getMatchById(
                $this->tournament->id,
                $matchId
            );
        } catch (\Exception $e) {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not retrieve match from Toornament!'
            );

            return false;
        }

        return $this->checkMatch();
    }

    private function loadMatchSettings()
    {
        assert($this->match != null);

        if (!is_string($this->match->public_note) || $this->match->public_note === '')
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'No script settings in public note of match!'
            );
            return false;
        }

        if (substr_count($this->match->public_note, "\n") != 1)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot parse script settings from public note!'
            );
            return false;
        }

        list($scriptName, $scriptSettings) = explode("\n", $this->match->public_note);
        $this->scriptName = trim($scriptName);
        $this->scriptSettings = json_decode(trim($scriptSettings), true);

        return true;
    }

    private function loadParticipants()
    {
        assert($this->client     != null);
        assert($this->tournament != null);
        assert($this->match      != null);

        $this->participants = array();
        foreach ($this->match->opponents as $opponent)
        {
            $participant = $opponent->participant;
            if ($participant === null)
                continue;

            try {
                $participant = $this->client->getParticipantById(
                    $this->tournament->id,
                    $participant->id
                );
            } catch (\Exception $e) {
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_EXCEPTION,
                    $e
                );
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not retrieve all participants from Toornament!'
                );

                return false;
            }

            $this->participants[$participant->id] = $participant;
        }

        assert(count($this->match->opponents) == count($this->participants));

        if ($this->match->type === Models\Match\Type::DUEL && count($this->participants) != 2)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Duel Match has to have exactly 2 participants!'
            );
            return false;
        }

        return $this->checkParticipants();
    }

    private function loadTournament(string $tournamentId)
    {
        assert($this->client != null);

        try {
            $this->tournament = $this->client->getTournamentById($tournamentId);
        } catch (\Exception $e) {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not retrieve tournament from Toornament!'
            );
            return false;
        }

        return $this->checkTournament();
    }

    public function prepareMatchDetails(ViewerClient $client)
    {
        if (!$this->isMatchLoaded())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not prepare match details, no match loaded!'
            );
            return false;
        }

        $stage = null;
        $group = null;
        $round = null;

        try {
            $stage = $client->getStageById(
                $this->tournament->id,
                $this->match->stage_id
            );

            $group = $client->getGroupById(
                $this->tournament->id,
                $this->match->group_id
            );

            $round = $client->getRoundById(
                $this->tournament->id,
                $this->match->round_id
            );
        } catch (\Exception $e) {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not prepare match details!'
            );

            return false;
        }

        $contacts = array();
        if (isset($this->tournament->contact) && $this->tournament->contact != '')
            $contacts['eMail'  ] = 'mailto:'.$this->tournament->contact;
        if (isset($this->tournament->discord) && $this->tournament->discord != '')
            $contacts['Discord'] = $this->tournament->discord;
        if (isset($this->tournament->website) && $this->tournament->website != '')
            $contacts['Website'] = $this->tournament->website;

        $contactsStr = array();
        foreach ($contacts as $desc => $link)
            array_push($contactsStr, '$L['.$link.']'.$desc.'$L');
        $contactsStr = implode(', ', $contactsStr);

        $matchStr = array();
        foreach ($this->participants as $participant)
            array_push($matchStr, $participant->name);
        $matchStr = implode(' vs ', $matchStr);
        $matchStr = $matchStr.' (# '.implode('.', array(
            $stage->number,
            $group->number,
            $round->number,
            $this->match->number
        )).')';

        $messages = array();
        $messages['Tournament'] = $this->tournament->name;
        $messages['Organizer' ] = $this->tournament->organization;
        $messages['Contacts'  ] = $contactsStr;
        $messages['Stage'     ] = $stage->name;
        $messages['Group'     ] = $group->name;
        $messages['Round'     ] = $round->name;
        $messages['Match'     ] = $matchStr;
        if (isset($this->match->settings->match_format) && $this->match->settings->match_format != '')
            $messages['Match Format'] = $this->match->settings->match_format;

        return $messages;
    }

    public function setOrganizerClient(OrganizerClient $client)
    {
        $this->client = $client;
    }

    public function startGame()
    {
        if ($this->isMatchCompleted())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot start game, match is completed!'
            );
            return false;
        }

        $success = $this->loadGame();
        if (!$success)
            return false;

        $prevStatus = $this->getCurrentGame()->status;
        if ($prevStatus === Models\Common\Status::PENDING)
        {
            $startScores = array();
            foreach ($this->getCurrentGame()->opponents as $opponent)
            {
                array_push($startScores, array(
                    Models\Common\Opponents::NUMBER => $opponent->number,
                    Models\Common\Opponents::SCORE => 0
                ));
            }

            $gameUpdates = array(
                Models\Common::OPPONENTS => $startScores,
                Models\Common::STATUS => Models\Common\Status::RUNNING
            );

            if (!$this->updateGame($gameUpdates))
            {
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not start game!'
                );
                return false;
            }

            $this->toornamentMania->chat(
                ChatMode::INFORMATION,
                'Game '.$this->getCurrentGameNumber().' started!'
            );
        }
        elseif ($prevStatus === Models\Common\Status::RUNNING)
        {
            $this->toornamentMania->chat(
                ChatMode::INFORMATION,
                'Game '.$this->getCurrentGameNumber().' continued!'
            );
        }
        else
            assert(false);

        return true;
    }

    public function startMatch()
    {
        if (!$this->isMatchLoaded())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot start match, no match loaded!'
            );
            return false;
        }

        if ($this->isMatchCompleted())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot start match, match is completed!'
            );
            return false;
        }

        $prevStatus = $this->match->status;
        if ($prevStatus === Models\Common\Status::PENDING)
        {
            $startScores = array();
            foreach ($this->getMatch()->opponents as $opponent)
            {
                $scores = array(
                    Models\Common\Opponents::NUMBER => $opponent->number,
                    Models\Common\Opponents::SCORE => 0
                );
                if ($this->match->type === Models\Match\Type::FFA && ($opponent === null || $opponent->participant === null))
                    $scores[Models\Common\Opponents::RANK] = $this->getNbParticipants() + 1;

                array_push($startScores, $scores);
            }

            $matchUpdates = array(
                Models\Common::OPPONENTS => $startScores,
                Models\Common::STATUS => Models\Common\Status::RUNNING
            );

            if (!$this->updateMatch($matchUpdates))
            {
                $this->toornamentMania->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not start match!'
                );
                return false;
            }

            $this->toornamentMania->chat(
                ChatMode::INFORMATION,
                'Match started!'
            );
        }
        elseif ($prevStatus === Models\Common\Status::RUNNING)
        {
            $this->toornamentMania->chat(
                ChatMode::INFORMATION,
                'Match continued!'
            );
        }
        else
            assert(false);

        return true;
    }

    public function unload($resetClient = true)
    {
        if ($resetClient)
            $this->client = null;

        $this->currentGameNumber = null;
        $this->games             = null;
        $this->match             = null;
        $this->participants      = null;
        $this->scriptName        = null;
        $this->scriptSettings    = null;
        $this->tournament        = null;
    }

    public function updateGame(array $gameUpdates)
    {
        if ($this->client === null)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot update game, no client loaded!'
            );
            return false;
        }

        if ($this->tournament === null)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot update game, no tournament loaded!'
            );
            return false;
        }

        if ($this->match === null)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot update game, no match loaded!'
            );
            return false;
        }

        if (!$this->hasCurrentGame())
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot update game, no game running!'
            );
            return false;
        }

        try {
            $this->games[$this->currentGameNumber] = $this->client->updateGame(
                $this->tournament->id,
                $this->match->id,
                $this->games[$this->currentGameNumber]->number,
                $gameUpdates
            );
        } catch (\Exception $e) {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not update game on Toornament!'
            );

            return false;
        }

        return true;
    }

    public function updateMatch(array $matchUpdates, bool $loadGames = true, bool $errorOnComplete = true)
    {
        if ($this->client === null)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot update match, no client loaded!'
            );
            return false;
        }

        if ($this->tournament === null)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot update match, no tournament loaded!'
            );
            return false;
        }

        if ($this->match === null)
        {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Cannot update match, no match loaded!'
            );
            return false;
        }

        try {
            $this->match = $this->client->updateMatch(
                $this->tournament->id,
                $this->match->id,
                $matchUpdates
            );
        } catch (\Exception $e) {
            $this->toornamentMania->chat(
                ChatMode::ADMIN_EXCEPTION,
                $e
            );
            $this->toornamentMania->chat(
                ChatMode::ADMIN_ERROR,
                'Could not update match on Toornament!'
            );

            return false;
        }

        if ($this->hasGames() && $loadGames)
            return $this->loadGames($errorOnComplete);

        return true;
    }
}
