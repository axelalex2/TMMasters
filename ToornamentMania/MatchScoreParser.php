<?php

namespace TMMasters\ToornamentMania;

use ManiaControl\Callbacks\Structures\TrackMania\OnScoresStructure;
use ManiaControl\ManiaControl;
use \TMMasters\Toornament\Models;

abstract class MatchScoreParser
{
    private static function simpleToToornament_Player(array $scores, MatchDataManager $matchDataManager)
    {
        $toornamentScores = array();
        $participants = $matchDataManager->getParticipants();
        foreach ($matchDataManager->getMatch()->opponents as $opponent)
        {
            if ($opponent === null || $opponent->participant === null)
                continue;

            if (!array_key_exists($opponent->participant->id, $participants))
                continue;

            $participant = $participants[$opponent->participant->id];
            assert($participant != null);
            assert(!isset($participant->lineup));
            assert(isset($participant->custom_fields->maniaplanet_id));
            $login = $participant->custom_fields->maniaplanet_id;

            array_push($toornamentScores, array(
                Models\Common\Opponents::NUMBER => $opponent->number,
                Models\Common\Opponents::SCORE  => $scores[$login] ?? 0
            ));
        }

        return array(
            Models\Common::OPPONENTS => $toornamentScores
        );
    }

    private static function simpleToToornament_Team(array $scores, MatchDataManager $matchDataManager)
    {
        // TODO check $teamId with $opponent->number

        $toornamentScores = array();
        foreach ($scores as $teamId => $teamScore)
        {
            array_push($toornamentScores, array(
                Models\Common\Opponents::NUMBER => $teamId+1,
                Models\Common\Opponents::SCORE  => $teamScore
            ));
        }

        return array(
            Models\Common::OPPONENTS => $toornamentScores
        );
    }

    public static function toornamentToTrackmania(MatchDataManager $matchDataManager, ManiaControl $maniaControl)
    {
        $matchOpponents = $matchDataManager->getMatch()->opponents;
        $opponents = array();
        if ($matchDataManager->hasCurrentGame())
            $opponents = $matchDataManager->getCurrentGame()->opponents;
        else
            $opponents = $matchDataManager->getMatch()->opponents;

        $nbOpponents = count($opponents);
        assert($nbOpponents === count($matchOpponents));

        $scores = array();
        $participants = $matchDataManager->getParticipants();
        for ($i = 0; $i < $nbOpponents; $i++)
        {
            $opponent = $opponents[$i];
            $matchOpponent = $matchOpponents[$i];

            assert($opponent->number === $matchOpponent->number);

            if ($matchOpponent->participant === null)
                continue;

            assert(array_key_exists($matchOpponent->participant->id, $participants));

            $scoreKey = $opponent->number-1; // $teamId
            $participant = $participants[$matchOpponent->participant->id];
            if (!isset($participant->lineup))
            {
                assert(isset($participant->custom_fields->maniaplanet_id));
                $scoreKey = $participant->custom_fields->maniaplanet_id;
            }

            $scores[$scoreKey] = $opponent->score;
        }

        return $scores;
    }

    private static function trackmaniaToSimple_Player(OnScoresStructure $structure)
    {
        assert(!$structure->getUseTeams());

        $scores = array();
        foreach ($structure->getPlayerScores() as $playerScore)
            $scores[$playerScore->getPlayer()->login] = $playerScore->getMatchPoints();

        return $scores;
    }

    private static function trackmaniaToSimple_Team(OnScoresStructure $structure)
    {
        assert($structure->getUseTeams());

        $scores = array();
        foreach ($structure->getTeamScores() as $teamScore)
            $scores[$teamScore->getTeamId()] = $teamScore->getMatchPoints();

        return $scores;
    }

    public static function trackmaniaToToornament(OnScoresStructure $structure, MatchDataManager $matchDataManager)
    {
        if ($structure->getUseTeams())
        {
            $scores = self::trackmaniaToSimple_Team($structure);
            return self::simpleToToornament_Team($scores, $matchDataManager);
        }
        else
        {
            $scores = self::trackmaniaToSimple_Player($structure);
            return self::simpleToToornament_Player($scores, $matchDataManager);
        }

        assert(false);
    }
}
