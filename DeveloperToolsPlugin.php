<?php

namespace TMMasters;

use ManiaControl\Admin\AuthenticationManager;
use ManiaControl\Commands\CommandListener;
use ManiaControl\ManiaControl;
use ManiaControl\Players\Player;
use ManiaControl\Plugins\Plugin;

/**
 * DeveloperToolsPlugin provides a simple plugin for developers to have additional actions over their servers and plugins.
 *
 * @author axelalex2
 */
class DeveloperToolsPlugin implements CommandListener, Plugin
{
    /**
     * Constants
     */
    const PLUGIN_ID      = 999;
    const PLUGIN_VERSION = 20.05;
    const PLUGIN_NAME    = 'DeveloperToolsPlugin';
    const PLUGIN_AUTHOR  = 'axelalex2';

    /**
     * Private properties
     */
    /** @var    ManiaControl $maniaControl */
    private $maniaControl = null;

    /**
     * @see \ManiaControl\Plugins\Plugin::prepare()
     * @param ManiaControl $maniaControl
     */
    public static function prepare(ManiaControl $maniaControl)
    { }

    public static function getAuthor     () { return self::PLUGIN_AUTHOR ; }
    public static function getId         () { return self::PLUGIN_ID     ; }
    public static function getName       () { return self::PLUGIN_NAME   ; }
    public static function getVersion    () { return self::PLUGIN_VERSION; }
    public static function getDescription() { return 'Plugin offers additional Tools for Developers'; }

    /**
     * Load the plugin
     *
     * @param \ManiaControl\ManiaControl $maniaControl
     */
    public function load(ManiaControl $maniaControl)
    {
        $this->maniaControl = $maniaControl;

        // Commands
        $this->maniaControl->getCommandManager()->registerCommandListener('query', $this, 'onCommandQuery', true, 'Executes Server-Query (only available to MasterAdmin).');
    }

    public function onCommandQuery(array $chatCallback, Player $player)
    {
        if (!$this->maniaControl->getAuthenticationManager()->checkRight($player, AuthenticationManager::AUTH_LEVEL_MASTERADMIN))
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($player);
            return false;
        }
        
        $commandParts = explode(' ', $chatCallback[1][2]);
        if (!empty($commandParts))
            $command = array_shift($commandParts);
        
        if (empty($commandParts))
        {
            $this->maniaControl->getChat()->sendError('No query specified!', $player);
            return false;
        }

        $query = array_shift($commandParts);
        if (!$query)
        {
            $this->maniaControl->getChat()->sendError('No query specified!', $player);
            return false;
        }

        try
        {
            $this->maniaControl->getChat()->sendInformation('Executing Query "' . $query . '" with parameters: ' . implode(', ', $commandParts), $player);
            $result = $this->maniaControl->getClient()->execute($query, $commandParts);
            $result = var_export($result, true);
            $this->maniaControl->getChat()->sendInformation($result, $player);
        }
        catch (\Exception $e)
        {
            $this->maniaControl->getChat()->sendException($e, $player);
            return false;
        }

        return true;
    }

    /**
     * Unload the plugin and its resources
     * @see \ManiaControl\Plugins\Plugin::unload()
     */
    public function unload()
    {
        $this->maniaControl = null;
    }
}
