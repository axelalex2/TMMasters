<?php

namespace TMMasters;

use ManiaControl\Callbacks\TimerListener;
use ManiaControl\Files\FileUtil;
use ManiaControl\Logger;
use ManiaControl\ManiaControl;
use ManiaControl\Players\Player;
use ManiaControl\Utils\Formatter;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;

class DummyTimerListener implements TimerListener
{ }

class TMMUtils
{
    static public function chat(ManiaControl $maniaControl, $prefix, int $mode, $messages, $loginsOrAuthLevel = null)
    {
        if (empty($messages))
            return;

        if (!is_array($messages))
            $messages = array($messages);

        foreach ($messages as $message)
        {
            switch ($mode)
            {
                case ChatMode::ADMIN_SUCCESS:
                    $maniaControl->getChat()->sendSuccessToAdmins(
                        $message,
                        $loginsOrAuthLevel,
                        $prefix
                    );
                    break;

                case ChatMode::ADMIN_INFORMATION:
                    $maniaControl->getChat()->sendInformationToAdmins(
                        $message,
                        $loginsOrAuthLevel,
                        $prefix
                    );
                    Logger::logInfo($message);
                    break;

                case ChatMode::ADMIN_MESSAGE:
                    $maniaControl->getChat()->sendMessageToAdmins(
                        $message,
                        $loginsOrAuthLevel,
                        $prefix
                    );
                    break;

                case ChatMode::ADMIN_ERROR:
                    $maniaControl->getChat()->sendErrorToAdmins(
                        $message,
                        $loginsOrAuthLevel,
                        $prefix
                    );
                    Logger::logError($message);
                    break;

                case ChatMode::ADMIN_EXCEPTION:
                    assert($message instanceof Exception || $message instanceof \Exception);
                    $maniaControl->getChat()->sendExceptionToAdmins(
                        $message,
                        $loginsOrAuthLevel,
                        $prefix
                    );
                    Logger::logError($message);
                    break;

                case ChatMode::SUCCESS:
                    $maniaControl->getChat()->sendSuccess(
                        $message,
                        $loginsOrAuthLevel,
                        $prefix
                    );
                    break;

                case ChatMode::INFORMATION:
                    $maniaControl->getChat()->sendInformation(
                        $message,
                        $loginsOrAuthLevel,
                        $prefix
                    );
                    if (empty($logins))
                        Logger::logInfo($message);
                    break;

                case ChatMode::ERROR:
                    $maniaControl->getChat()->sendError(
                        $message,
                        $loginsOrAuthLevel,
                        $prefix
                    );
                    if (empty($logins))
                        Logger::logError($message);
                    break;

                case ChatMode::EXCEPTION:
                    assert($message instanceof Exception || $message instanceof \Exception);
                    $maniaControl->getChat()->sendException(
                        $message,
                        $loginsOrAuthLevel,
                        $prefix
                    );
                    if (empty($logins))
                        Logger::logError($message);
                    break;

                default:
                    assert(false);
            }
        }
    }

    static public function chatFormatPlayer($player)
    {
        if (is_array($player))
        {
            $players = array();
            foreach ($player as $p)
                array_push($players, self::chatFormatPlayer($p));
            
            return implode(', ', $players);
        }
        elseif ($player instanceof Player)
            return $player->getEscapedNickname().' ('.$player->login.')';
        
        return '';
    }

    static public function dump($mixed)
    {
        if (!is_array($mixed) && !is_object($mixed))
            return $mixed;

        $arrayClone = (array) $mixed;
        $arrayDump = array();

        foreach ($arrayClone as $key => $value)
        {
            $newKey = null;
            if (is_object($key))
                $newKey = 'Instance of "'.get_class($key).'"';
            else
            {
                $keyList = explode("\0", $key);
                $newKey = $keyList[count($keyList)-1];
            }

            if (is_object($value))
                $value = 'Instance of "'.get_class($value).'"';
            else
                $value = self::dump($value);
            
            $arrayDump[$newKey] = $value;
        }

        return $arrayDump;
    }

    /**
     * Formats message to input variables with white color.
     * @param string $message
     * @param ...$inputs
     */
    static public function formatMessage(string $message, ...$inputs)
    {
        $formattedInputs = array();
        foreach ($inputs as $input)
        {
            $strInput = null;
            
            if (is_array($input))
                $strInput = var_export($input, true);
            elseif (is_bool($input))
                $strInput = $input ? 'true' : 'false';
            elseif ($input instanceof Player)
                $strInput = self::chatFormatPlayer($input);
            elseif (is_object($input))
                continue; // can't print objects
            else
                $strInput = (string) $input;

            array_push($formattedInputs, "\$<\$fff{$strInput}\$>");
        }

        array_unshift($formattedInputs, $message);
        return sprintf(...$formattedInputs);
    }

    static public function loadScript(ManiaControl $maniaControl, int $delay, string $scriptName, array $scriptSettings, $scriptText = null)
    {
        static $scriptsDir = null;
        if ($scriptsDir === null)
        {
            $scriptsDataDir = FileUtil::shortenPath($maniaControl->getServer()->getDirectory()->getScriptsFolder());
            if ($maniaControl->getServer()->checkAccess($scriptsDataDir))
            {
                $gameShort = $maniaControl->getMapManager()->getCurrentMap()->getGame();
                $game = '';
                switch ($gameShort)
                {
                    case 'qm': $game = 'QuestMania'; break;
                    case 'sm': $game = 'ShootMania'; break;
                    case 'tm': $game = 'TrackMania'; break;
                }

                if ($game != '')
                {
                    $scriptsDir = $scriptsDataDir.DIRECTORY_SEPARATOR.'Modes'.DIRECTORY_SEPARATOR.$game.DIRECTORY_SEPARATOR;
                    if (!$maniaControl->getServer()->checkAccess($scriptsDir))
                        $scriptsDir = null;
                }
            }

            if ($scriptsDir === null)
                throw new Exception('Scripts directory not found, unable to load different scripts!');
        }

        if ($scriptText === null)
        {
            $scriptPath = $scriptsDir.$scriptName;
            if (!file_exists($scriptPath))
                throw new Exception('Script not found ('.$scriptPath.').');

            $scriptText = file_get_contents($scriptPath);
        }

        $maniaControl->getClient()->setModeScriptText($scriptText);
        $maniaControl->getClient()->setScriptName($scriptName);
        $maniaControl->getClient()->setModeScriptSettings($scriptSettings);

        if ($delay >= 0)
        {
            $maniaControl->getTimerManager()->registerOneTimeListening(
                new DummyTimerListener(),
                function () use ($maniaControl) {
                    $maniaControl->getMapManager()->getMapActions()->skipMap();
                },
                $delay
            );
        }
    }

    static public function loginsToPlayers(ManiaControl $maniaControl, array $logins, bool $onlyConnectedPlayers = false)
    {
        $players = array();
        foreach ($logins as $login)
        {
            if (empty($login))
                continue;

            if (!preg_match('/[\-\.0-9\_a-z]+/', $login))
                throw new Exception("Login '{$login}' invalid!");

            $player = $maniaControl->getPlayerManager()->getPlayer($login, $onlyConnectedPlayers);
            if ($player === null)
                throw new Exception("Player with login '{$login}' not found!");

            $players[$login] = $player;
        }
        return $players;
    }

    static public function playersToLogins(array $players)
    {
        $logins = array();
        foreach ($players as $player)
        {
            assert($player instanceof Player);
            array_push($logins, $player->login);
        }
        return $logins;
    }

    static public function replaceSpecialCharacters(string $s)
    {
        // a simple set of characters (printable ASCII + small extensions)
        static $ascii = '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~§°€';
        // TODO lookup nickedit in ManiaPlanet
        static $replaceTable = array(
            '`' => '\'', '´' => '\'', '΄' => '\'', '΅' => '\'', '‘' => '\'', '’' => '\'',
            '“' => '"', '”' => '"',
            '¸' => ',', '͵' => ',',
            'ͺ' => '.',
            '…' => '...',
            ';' => ';',
            '¹' => '1',
            '²' => '2',
            '³' => '3', 'З' => '3', 'з' => '3',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ă' => 'A', 'Ą' => 'A', 'Ǎ' => 'A', 'Ǟ' => 'A', 'Ǻ' => 'A', 'А' => 'A', 'Ά' => 'A', 'Α' => 'A',
            'Æ' => 'AE', 'Ǣ' => 'AE', 'Ǽ' => 'AE',
            'В' => 'B', 'в' => 'B', 'Β' => 'B', '฿' => 'B', 'Ƀ' => 'B',
            'Ç' => 'C', 'Ć' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Č' => 'C', 'С' => 'C', 'Ҫ' => 'C',
            'Ð' => 'D', 'Ď' => 'D', 'Đ' => 'D',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ę' => 'E', 'Ě' => 'E', 'Ё' => 'E', 'Е' => 'E', 'Έ' => 'E', 'Ε' => 'E', 'Ξ' => 'E', 'Σ' => 'E', 'ミ' => 'E', 'ョ' => 'E', 'ヨ' => 'E',
            'Ŧ' => 'F', 'Ƒ' => 'F', 'Ғ' => 'F', 'ғ' => 'F', 'キ' => 'F', 'ギ' => 'F', 'チ' => 'F', 'ヂ' => 'F',
            'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G', 'Ǥ' => 'G', 'Ǧ' => 'G',
            'Ĥ' => 'H', 'Н' => 'H', 'н' => 'H', 'Ң' => 'H', 'ң' => 'H', 'Ҥ' => 'H', 'ҥ' => 'H', 'Ӈ' => 'H', 'ӈ' => 'H', 'Ή' => 'H', 'Η' => 'H',
            'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ĩ' => 'I', 'Ī' => 'I', 'Ĭ' => 'I', 'Į' => 'I', 'İ' => 'I', 'Ǐ' => 'I', 'І' => 'I', 'Ї' => 'I', 'Ί' => 'I', 'Ι' => 'I', 'Ϊ' => 'I', 'ェ' => 'I', 'エ' => 'I',
            'Ĳ' => 'IJ',
            'ĴЈ' => 'J', 'Ј' => 'J',
            'Ķ' => 'K', 'ĸ' => 'K', 'Ǩ' => 'K', 'Ќ' => 'K', 'К' => 'K', 'к' => 'K', 'ќ' => 'K', 'Қ' => 'K', 'қ' => 'K', 'Ҝ' => 'K', 'ҝ' => 'K', 'Ҟ' => 'K', 'Ҡ' => 'K', 'ҡ' => 'K', 'Ӄ' => 'K', 'ӄ' => 'K', 'Κ' => 'K', 'κ' => 'K',
            'Ĺ' => 'L', 'Ļ' => 'L', 'Ľ' => 'L', 'Ŀ' => 'L', 'Ł' => 'L', 'ム' => 'L',
            'М' => 'M', 'м' => 'M', 'Μ' => 'M',
            'Ñ' => 'N', 'Ń' => 'N', 'Ņ' => 'N', 'Ň' => 'N', 'Ŋ' => 'N', 'И' => 'N', 'Й' => 'N', 'и' => 'N', 'й' => 'N', 'Ν' => 'N',
            'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ō' => 'O', 'Ŏ' => 'O', 'Ő' => 'O', 'Ơ' => 'O', 'Ǒ' => 'O', 'Ǫ' => 'O', 'Ǭ' => 'O', 'Ǿ' => 'O', 'О' => 'O', 'Ѳ' => 'O', 'Ό' => 'O', 'Θ' => 'O', 'Ο' => 'O',
            'Œ' => 'OE',
            'Ѹ' => 'Oy',
            'Р' => 'P', 'Ρ' => 'P', 'ァ' => 'P', 'ア' => 'P', 'ャ' => 'P', 'ヤ' => 'P',
            'Ŕ' => 'R', 'Ŗ' => 'R', 'Ř' => 'R', 'Я' => 'R', 'я' => 'R', '々' => 'R', 'マ' => 'R',
            'Ś' => 'S', 'Ŝ' => 'S', 'Ş' => 'S', 'Š' => 'S', 'Ѕ' => 'S',
            'Ţ' => 'T', 'Ť' => 'T', 'Ʈ' => 'T', 'Т' => 'T', 'т' => 'T', 'Ҭ' => 'T', 'ҭ' => 'T', 'Τ' => 'T', 'τ' => 'T', '〒' => 'T', 'ィ' => 'T', 'イ' => 'T',
            'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ũ' => 'U', 'Ū' => 'U', 'Ŭ' => 'U', 'Ů' => 'U', 'Ű' => 'U', 'Ų' => 'U', 'Ư' => 'U', 'Ǔ' => 'U', 'Ǖ' => 'U', 'Ǘ' => 'U', 'Ǚ' => 'U', 'Ǜ' => 'U',
            'Ѵ' => 'V', 'Ѷ' => 'V',
            'Ŵ' => 'W',
            'Х' => 'X', 'Ҳ' => 'X', 'Χ' => 'X', 'χ' => 'X', '〤' => 'X', 'メ' => 'X',
            '¥' => 'Y', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ў' => 'Y', 'У' => 'Y', 'Ү' => 'Y', 'Ұ' => 'Y', 'Ύ' => 'Y', 'Υ' => 'Y', 'Ϋ' => 'Y', 'ϒ' => 'Y', 'ϓ' => 'Y', 'ϔ' => 'Y',
            'Ź' => 'Z', 'Ż' => 'Z', 'Ž' => 'Z', 'Ƶ' => 'Z', 'Ζ' => 'Z',
            'ª' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'ā' => 'a', 'ă' => 'a', 'ą' => 'a', 'ǎ' => 'a', 'ǟ' => 'a', 'ǻ' => 'a', 'а' => 'a', 'ά' => 'a', 'α' => 'a', 'λ' => 'a', '〆' => 'a', 'ค' => 'a', 'ด' => 'a', 'ล' => 'a', 'ศ' => 'a', 'ส' => 'a',
            'æ' => 'ae', 'ǣ' => 'ae', 'ǽ' => 'ae',
            'ƀ' => 'b', 'β' => 'b',
            '¢' => 'c', 'ç' => 'c', 'ć' => 'c', 'ĉ' => 'c', 'ċ' => 'c', 'č' => 'c', 'с' => 'c', 'ҫ' => 'c',
            'ð' => 'd', 'ď' => 'd', 'đ' => 'd', 'δ' => 'd', '๔' => 'd', '๕' => 'd',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ē' => 'e', 'ĕ' => 'e', 'ė' => 'e', 'ę' => 'e', 'ě' => 'e', 'е' => 'e', 'ё' => 'e', 'Ҽ' => 'e', 'ҽ' => 'e', 'Ҿ' => 'e', 'ҿ' => 'e',
            'ƒ' => 'f',
            'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ǥ' => 'g', 'ǧ' => 'g', 'ǵ' => 'g',
            'ĥ' => 'h', 'ħ' => 'h', 'ћ' => 'h', 'Һ' => 'h', 'һ' => 'h',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ĩ' => 'i', 'ī' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ǐ' => 'i', 'і' => 'i', 'ї' => 'i', 'ו' => 'i', 'ΐ' => 'i', 'ί' => 'i', 'ι' => 'i', 'ϊ' => 'i',
            'ĳ' => 'ij',
            'ĵ' => 'j', 'ǰ' => 'j', 'ј' => 'j', 'נ' => 'j',
            'ķ' => 'k', 'ǩ' => 'k', 'ҟ' => 'k', 'ห' => 'k',
            'ĺ' => 'l', 'ļ' => 'l', 'ľ' => 'l', 'ŀ' => 'l', 'ł' => 'l',
            'ฅ' => 'm', 'ต' => 'm', '๓' => 'm', '๗' => 'm',
            'ñ' => 'n', 'ń' => 'n', 'ņ' => 'n', 'ň' => 'n', 'ŉ' => 'n', 'ŋ' => 'n', 'ה' => 'n', 'ח' => 'n', 'מ' => 'n', 'ת' => 'n', 'η' => 'n', 'ก' => 'n', 'ฑ' => 'n', 'ท' => 'n',
            'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ō' => 'o', 'ŏ' => 'o', 'ő' => 'o', 'ơ' => 'o', 'ǒ' => 'o', 'ǫ' => 'o', 'ǭ' => 'o', 'ǿ' => 'o', 'о' => 'o', 'ѳ' => 'o', 'ס' => 'o', 'ο' => 'o', 'σ' => 'o', 'ό' => 'o', '๏' => 'o', '๐' => 'o',
            'œ' => 'oe',
            'ѹ' => 'oy',
            'р' => 'p', 'ק' => 'p', 'ρ' => 'p',
            'ŕ' => 'r', 'ŗ' => 'r', 'ř' => 'r',
            'ś' => 's', 'ŝ' => 's', 'ş' => 's', 'š' => 's', 'ѕ' => 's', 'ธ' => 's', 'ร' => 's',
            'ţ' => 't', 'ť' => 't', 'ŧ' => 't', 'ƫ' => 't', 'ナ' => 't', 'ヒ' => 't', 'ビ' => 't', 'ピ' => 't',
            'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ũ' => 'u', 'ū' => 'u', 'ŭ' => 'u', 'ů' => 'u', 'ű' => 'u', 'ų' => 'u', 'ư' => 'u', 'ǔ' => 'u', 'ǖ' => 'u', 'ǘ' => 'u', 'ǚ' => 'u', 'ǜ' => 'u', 'ט' => 'u', 'ΰ' => 'u', 'υ' => 'u', 'ϋ' => 'u', 'ύ' => 'u', 'น' => 'u', 'บ' => 'u', 'ม' => 'u',
            'ѵ' => 'v', 'ѷ' => 'v', 'ν' => 'v', 'ϑ' => 'v',
            'ŵ' => 'w', 'ѡ' => 'w', 'ω' => 'w', 'ώ' => 'w', 'ผ' => 'w', 'ฝ' => 'w', 'พ' => 'w', 'ฟ' => 'w', 'ฬ' => 'w',
            '×' => 'x', 'х' => 'x', 'ҳ' => 'x',
            'ý' => 'y', 'ÿ' => 'y', 'ŷ' => 'y', 'у' => 'y', 'ў' => 'y', 'ү' => 'y', 'ұ' => 'y', 'ע' => 'y', 'γ' => 'y',
            'ź' => 'z', 'ż' => 'z', 'ž' => 'z', 'ƶ' => 'z',
            '¦' => '|',
            '' => 'twitch',
        );

        $result = '';

        $s = Formatter::stripCodes($s);
        $s = mb_strtolower($s, 'UTF-8');
        foreach (preg_split('/(?<!^)(?!$)/u', $s) as $c)
        {
            if (strpos($ascii, $c) !== false)
                $result .= $c;

            if (array_key_exists($c, $replaceTable))
                $result .= $replaceTable[$c];

            // everything else will be discarded
        }

        $result = mb_strtolower($result, 'UTF-8');
        return $result;
    }
}
