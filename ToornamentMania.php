<?php

namespace TMMasters;

use FML\Controls\Entry;
use FML\Controls\Frame;
use FML\Controls\Labels\Label_Button;
use FML\Controls\Labels\Label_Text;
use FML\Controls\Quad;
use FML\Controls\Quads\Quad_Icons64x64_1;
use FML\ManiaLink;
use ManiaControl\Admin\AuthenticationManager;
use ManiaControl\Callbacks\CallbackListener;
use ManiaControl\Callbacks\CallbackManager;
use ManiaControl\Callbacks\Callbacks;
use ManiaControl\Callbacks\Structures\TrackMania\OnScoresStructure;
use ManiaControl\Callbacks\TimerListener;
use ManiaControl\Commands\CommandListener;
use ManiaControl\Configurator\ScriptSettings;
use ManiaControl\Logger;
use ManiaControl\ManiaControl;
use ManiaControl\Manialinks\ManialinkManager;
use ManiaControl\Manialinks\ManialinkPageAnswerListener;
use ManiaControl\Players\Player;
use ManiaControl\Players\PlayerActions;
use ManiaControl\Players\PlayerManager;
use ManiaControl\Plugins\InstallMenu;
use ManiaControl\Plugins\Plugin;
use ManiaControl\Settings\Setting;
use ManiaControl\Settings\SettingManager;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;

/**
 * ToornamentMania provides a plugin for matches to be reported on Toornament.
 *
 * @author axelalex2
 */
class ToornamentMania implements CallbackListener, CommandListener, ManialinkPageAnswerListener, Plugin, TimerListener
{
    /**
     * Constants
     */
    const PLUGIN_ID      = 999;
    const PLUGIN_VERSION = 19.01;
    const PLUGIN_NAME    = 'ToornamentMania';
    const PLUGIN_AUTHOR  = 'axelalex2';

    const CONST_SETTING_TOORNAMENTMANIA_CHAT_PREFIX                    = '$<$o$ffft$09fo$e23o$fffrnament$> » ';
    const CONST_SETTING_TOORNAMENTMANIA_MESSAGE_MORE_INFORMATION       = '...';
    const CONST_SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_SIZE_MULT = 0.1;

    const MLID_TOORNAMENTMANIA_BUTTON_READY = self::PLUGIN_NAME.'.Button.Ready';
    const MLID_TOORNAMENTMANIA_BUTTON_READY_ACTION = self::MLID_TOORNAMENTMANIA_BUTTON_READY.'.Action';

    const MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT           = self::PLUGIN_NAME.'.Widget.Advertisement';
    const MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_HEIGHT    = 105 * self::CONST_SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_SIZE_MULT;
    const MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_IMAGE_URL = 'https://blog.toornament.com/wp-content/uploads/2017/10/PoweredbyToor_White.png';
    const MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_WIDTH     = 450 * self::CONST_SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_SIZE_MULT;

    const MLID_TOORNAMENTMANIA_WINDOW_KEYS              = ManialinkManager::MAIN_MLID;
    const MLID_TOORNAMENTMANIA_WINDOW_KEYS_ACTION_CLOSE = self::PLUGIN_NAME.'.Window.Keys.Action.Close';
    const MLID_TOORNAMENTMANIA_WINDOW_KEYS_ACTION_SAVE  = self::PLUGIN_NAME.'.Window.Keys.Action.Save';
    const MLID_TOORNAMENTMANIA_WINDOW_KEYS_APIKEY       = self::PLUGIN_NAME.'.Window.Keys.ApiKey';
    const MLID_TOORNAMENTMANIA_WINDOW_KEYS_CLIENTID     = self::PLUGIN_NAME.'.Window.Keys.ClientId';
    const MLID_TOORNAMENTMANIA_WINDOW_KEYS_CLIENTSECRET = self::PLUGIN_NAME.'.Window.Keys.ClientSecret';
    const MLID_TOORNAMENTMANIA_WINDOW_KEYS_PASSWORD     = self::PLUGIN_NAME.'.Window.Keys.Password';

    const SETTING_TOORNAMENTMANIA_AUTHENTICATION_LEVEL       = 'Authentication Level for too*-Commands';
    const SETTING_TOORNAMENTMANIA_BUTTON_READY_HEIGHT        = 'Button Ready Height';
    const SETTING_TOORNAMENTMANIA_BUTTON_READY_POS_X         = 'Button Ready Pos X';
    const SETTING_TOORNAMENTMANIA_BUTTON_READY_POS_Y         = 'Button Ready Pos Y';
    const SETTING_TOORNAMENTMANIA_BUTTON_READY_WIDTH         = 'Button Ready Width';
    const SETTING_TOORNAMENTMANIA_CHAT_INFORMATION_FORMAT    = 'Chat Information Format';
    const SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_POS_X = 'Widget Advertisement Pos X';
    const SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_POS_Y = 'Widget Advertisement Pos Y';

    /*
     * Private properties
     */
    private $maniaControl           = null;
    private $matchManager           = null;
    private $windowKeysPressedClose = false;

    /**
     * @see \ManiaControl\Plugins\Plugin::prepare()
     * @param ManiaControl $maniaControl
     */
    public static function prepare(ManiaControl $maniaControl)
    { }

    public static function getAuthor     () { return self::PLUGIN_AUTHOR ; }
    public static function getId         () { return self::PLUGIN_ID     ; }
    public static function getName       () { return self::PLUGIN_NAME   ; }
    public static function getVersion    () { return self::PLUGIN_VERSION; }
    public static function getDescription() { return 'Plugin lets you report matches on '.Toornament\Toornament::URL; }

    public function getManiaControl() { return $this->maniaControl; }
    public function getMatchManager() { return $this->matchManager; }

    public function getAuthenticationLevel    () { return AuthenticationManager::getAuthLevel($this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_TOORNAMENTMANIA_AUTHENTICATION_LEVEL)); }
    public function getButtonReadyHeight      () { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_TOORNAMENTMANIA_BUTTON_READY_HEIGHT       ); }
    public function getButtonReadyPosX        () { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_TOORNAMENTMANIA_BUTTON_READY_POS_X        ); }
    public function getButtonReadyPosY        () { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_TOORNAMENTMANIA_BUTTON_READY_POS_Y        ); }
    public function getButtonReadyWidth       () { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_TOORNAMENTMANIA_BUTTON_READY_WIDTH        ); }
    public function getChatInformationFormat  () { return (string) $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_TOORNAMENTMANIA_CHAT_INFORMATION_FORMAT   ); }
    public function getWidgetAdvertisementPosX() { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_POS_X); }
    public function getWidgetAdvertisementPosY() { return (float)  $this->maniaControl->getSettingManager()->getSettingValue($this, self::SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_POS_Y); }

    public function load(ManiaControl $maniaControl)
    {
        $this->maniaControl = $maniaControl;

        $this->matchManager = new ToornamentMania\MatchManager($this);

        // Callbacks
        $this->maniaControl->getCallbackManager()->registerCallbackListener(PlayerManager::CB_PLAYERCONNECT, $this, 'handlePlayerConnectCallback');
        $this->maniaControl->getCallbackManager()->registerCallbackListener(SettingManager::CB_SETTING_CHANGED, $this, 'handleUpdateSettingsCallback');

        // Commands
        $this->maniaControl->getCommandManager()->registerCommandListener('tookeys'  , $this, 'commandToo',  true, 'Opens a window to enter your keys.');
        $this->maniaControl->getCommandManager()->registerCommandListener('tooload'  , $this, 'commandToo',  true, 'Loads a match by the given match URL.');
        $this->maniaControl->getCommandManager()->registerCommandListener('toounload', $this, 'commandToo',  true, 'Unloads the match.');

        // Manialinks
        $this->maniaControl->getManialinkManager()->registerManialinkPageAnswerListener(self::MLID_TOORNAMENTMANIA_BUTTON_READY_ACTION, $this, 'handleButtonReadyAction');
        $this->maniaControl->getManialinkManager()->registerManialinkPageAnswerListener(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_ACTION_CLOSE, $this, 'handleWindowKeysActionClose');
        $this->maniaControl->getManialinkManager()->registerManialinkPageAnswerListener(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_ACTION_SAVE , $this, 'handleWindowKeysActionSave' );

        // Settings
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_TOORNAMENTMANIA_AUTHENTICATION_LEVEL, AuthenticationManager::getPermissionLevelNameArray(AuthenticationManager::AUTH_LEVEL_MODERATOR));
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_TOORNAMENTMANIA_BUTTON_READY_HEIGHT, 10.5);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_TOORNAMENTMANIA_BUTTON_READY_POS_X, 32);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_TOORNAMENTMANIA_BUTTON_READY_POS_Y, 84.75);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_TOORNAMENTMANIA_BUTTON_READY_WIDTH, 20);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_TOORNAMENTMANIA_CHAT_INFORMATION_FORMAT, '$o$9cf');
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_POS_X, 0);
        $this->maniaControl->getSettingManager()->initSetting($this, self::SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_POS_Y, 84.75);

        // final build up steps in timer listening, so MC can finish startup first
        $this->maniaControl->getTimerManager()->registerOneTimeListening(
            $this,
            function () {
                $this->buildUpFinish();
            },
            1000
        );
    }

    public function buildUpFinish()
    {
        $this->widgetAdvertisementDisplay();

        if (!$this->matchManager->getAccessManager()->hasAllKeys())
        {
            $this->chat(
                ChatMode::ADMIN_INFORMATION,
                'Welcome to '.self::PLUGIN_NAME.' v'.self::PLUGIN_VERSION.' (by '.self::PLUGIN_AUTHOR.')!'
            );
            $this->chat(
                ChatMode::ADMIN_INFORMATION,
                'To fully activate '.self::PLUGIN_NAME.', enter //tookeys and insert your personal keys!'
            );
            $this->chat(
                ChatMode::ADMIN_INFORMATION,
                'For more information, visit '.self::CONST_SETTING_TOORNAMENTMANIA_MESSAGE_MORE_INFORMATION
            );
        }
        elseif ($this->matchManager->getAccessManager()->hasUrl())
        {
            if ($this->matchManager->loadMatch())
            {
                $this->chat(
                    ChatMode::ADMIN_SUCCESS,
                    'Match successfully recovered!'
                );
            }
            else
            {
                $this->chat(
                    ChatMode::ADMIN_ERROR,
                    'Could not recover match!'
                );
            }
        }
    }

    public function buttonReadyHide($player = null)
    {
        $this->maniaControl->getManialinkManager()->hideManialink(self::MLID_TOORNAMENTMANIA_BUTTON_READY, $player);
    }

    private function buttonReadyMove()
    {
        $borderX = 160 - $this->getButtonReadyWidth () / 2;
        $borderY =  90 - $this->getButtonReadyHeight() / 2;
        
        $posX = $this->getButtonReadyPosX();
        if (abs($posX) > $borderX)
        {
            // sign the border pos
            $borderX *= ($posX <=> 0);

            $this->chat(
                ChatMode::ADMIN_INFORMATION,
                'Button Ready would be off the screen, setting position X to '.$borderX
            );

            $this->maniaControl->getSettingManager()->setSetting(
                $this,
                self::SETTING_TOORNAMENTMANIA_BUTTON_READY_POS_X,
                $borderX
            );
        }

        $posY = $this->getButtonReadyPosY();
        if (abs($posY) > $borderY)
        {
            // sign the border pos
            $borderY *= ($posY <=> 0);

            $this->chat(
                ChatMode::ADMIN_INFORMATION,
                'Button Ready would be off the screen, setting position Y to '.$borderY
            );

            $this->maniaControl->getSettingManager()->setSetting(
                $this,
                self::SETTING_TOORNAMENTMANIA_BUTTON_READY_POS_Y,
                $borderY
            );
        }

        $this->buttonReadyUpdate();
    }

    public function buttonReadyUpdate($player = null)
    {
        if ($player === null)
        {
            $players = $this->maniaControl->getPlayerManager()->getPlayers();
            foreach ($players as $player)
                $this->buttonReadyUpdate($player);

            return true;
        }
        assert($player instanceof Player);

        if ($this->matchManager->isRunning() || !$this->matchManager->getMatchDataManager()->isMatchLoaded() || $player->isSpectator)
        {
            $this->buttonReadyHide($player);
            return true;
        }

        $height = $this->getButtonReadyHeight();
        $posX   = $this->getButtonReadyPosX();
        $posY   = $this->getButtonReadyPosY();
        $width  = $this->getButtonReadyWidth();

        $quadStyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadStyle();
        $quadSubstyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadSubstyle();

        $manialink = new ManiaLink(self::MLID_TOORNAMENTMANIA_BUTTON_READY);

        $frame = new Frame();
        $manialink->addChild($frame);
        $frame->setPosition($posX, $posY);
        $frame->setSize($width, $height);

        $borderQuad = new Quad();
        $frame->addChild($borderQuad);
        $borderQuad->setAction(self::MLID_TOORNAMENTMANIA_BUTTON_READY_ACTION);
        $borderQuad->setSize($width, $height);
        $borderQuad->setStyles($quadStyle, $quadSubstyle);

        $label = new Label_Text();
        $frame->addChild($label);
        $label->setTextSize(2);

        $ready = $this->matchManager->isPlayerReady($player);
        assert($ready != null);
        if ($ready) {
            $label->setText("Ready!");
            $label->setTextColor('0f0');
        } else {
            $label->setText("Ready?");
            $label->setTextColor('f00');
        }

        $this->maniaControl->getManialinkManager()->sendManialink($manialink, $player->login);
    }

    public function chat(int $mode, $messages, $logins = null)
    {
        if (empty($messages))
            return;

        if (is_string($messages))
            $messages = array($messages);

        assert(is_array($messages));

        $loginsOrAuthLevel = $logins;
        switch ($mode)
        {
            case ChatMode::ADMIN_SUCCESS:
            case ChatMode::ADMIN_INFORMATION:
            case ChatMode::ADMIN_ERROR:
            case ChatMode::ADMIN_EXCEPTION:
                $loginsOrAuthLevel = $this->getAuthenticationLevel();
                break;
            case ChatMode::INFORMATION:
                array_unshift($messages, $this->getChatInformationFormat());
                break;
            default: break;
        }

        TMMUtils::chat(
            $this->maniaControl,
            self::CONST_SETTING_TOORNAMENTMANIA_CHAT_PREFIX,
            $mode,
            $messages,
            $loginsOrAuthLevel
        );
    }

    public function commandToo(array $chatCallback, Player $admin)
    {
        $authLevel = $this->getAuthenticationLevel();
        if (!$this->maniaControl->getAuthenticationManager()->checkRight($admin, $authLevel))
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($admin);
            return false;
        }
        
        $params = explode(' ', $chatCallback[1][2]);
        $command = array_shift($params);

        if (!$this->matchManager->getAccessManager()->hasAllKeys() && $command != '//tookeys')
        {
            $this->chat(
                ChatMode::ERROR,
                array(
                    'Cannot use '.self::PLUGIN_NAME.' without Toornament API Key!',
                    'Enter //tookeys and insert your personal keys!'
                ),
                $admin->login
            );

            return false;
        }

        switch ($command)
        {
            case '//tookeys'  : $this->windowKeysPrepare($admin, array_shift($params)); break;
            case '//tooload'  : $this->loadMatch($admin, array_shift($params)); break;
            case '//toounload': $this->unloadMatch($admin); break;
            default: $this->chat(
                ChatMode::ERROR,
                'too*-Command "'.$command.'" not implemented yet!',
                $admin->login
            );
        }
    }

    private function displayMatchDetailsInChat()
    {
        $viewer = $this->matchManager->getAccessManager()->connectViewer();
        if ($viewer === null)
            return false;

        $messages = $this->matchManager->getMatchDataManager()->prepareMatchDetails($viewer);
        if ($messages === false)
            return false;

        assert(is_array($messages));
        foreach ($messages as $key => $info)
            $this->chat(
                ChatMode::INFORMATION,
                $key.': '.$info
            );

        return true;
    }

    public function handleButtonReadyAction(array $callback, Player $player)
    {
        $this->matchManager->togglePlayerReady($player);
    }

    public function handlePlayerConnectCallback(Player $player)
    {
        $this->widgetAdvertisementDisplay($player);
    }

    public function handleUpdateSettingsCallback(Setting $setting)
    {
        if (!$setting->belongsToClass($this))
            return true;

        $this->buttonReadyMove();
        $this->widgetAdvertisementMove();
    }

    public function handleWindowKeysActionClose(array $callback, Player $admin)
    {
        $data = $this->windowKeysExtractDataFromCallback($callback);
        $windowApiKey       = $data[self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_APIKEY];
        $windowClientId     = $data[self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_CLIENTID];
        $windowClientSecret = $data[self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_CLIENTSECRET];
        $windowPassword     = $data[self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_PASSWORD];

        $keys = $this->matchManager->getAccessManager()->getKeys();

        $storedApiKey       = $keys->api_key       ?? '';
        $storedClientId     = $keys->client_id     ?? '';
        $storedClientSecret = $keys->client_secret ?? '';

        $dataChanged = false;
        if ($windowApiKey       != $storedApiKey
         || $windowClientId     != $storedClientId
         || $windowClientSecret != $storedClientSecret
         || $this->matchManager->getAccessManager()->checkKeyPassword($windowPassword) != true)
        {
            $dataChanged = true;
        }

        if ($this->windowKeysPressedClose === true || !$dataChanged)
        {
            $this->windowKeysHide();
            $this->windowKeysPressedClose = false;
            return;
        }

        if ($dataChanged)
        {
            $this->windowKeysPressedClose = true;
            $this->chat(
                ChatMode::ERROR,
                'You have unsaved changes!',
                $admin->login
            );
        }
    }

    public function handleWindowKeysActionSave(array $callback, Player $admin)
    {
        $data = $this->windowKeysExtractDataFromCallback($callback);
        $windowApiKey       = $data[self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_APIKEY      ] ?? '';
        $windowClientId     = $data[self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_CLIENTID    ] ?? '';
        $windowClientSecret = $data[self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_CLIENTSECRET] ?? '';
        $windowPassword     = $data[self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_PASSWORD    ] ?? '';

        $success = $this->matchManager->getAccessManager()->setKeys(
            $windowApiKey      ,
            $windowClientId    ,
            $windowClientSecret,
            $windowPassword
        );

        if ($success === true)
        {
            $this->chat(
                ChatMode::SUCCESS,
                'Saved Keys!',
                $admin->login
            );
        }
        else
        {
            $this->chat(
                ChatMode::SUCCESS,
                'Could not save Keys!',
                $admin->login
            );
        }

        return $success;
    }

    private function loadMatch(Player $admin, string $url)
    {
        $success = $this->matchManager->loadMatch($url);
        if (!$success)
            return false;

        $this->chat(
            ChatMode::ADMIN_SUCCESS,
            $admin->getEscapedNickname().' loaded match!'
        );

        if (!$this->displayMatchDetailsInChat())
        {
            $this->chat(
                ChatMode::ADMIN_ERROR,
                'Could not display Match Details!'
            );
        }

        return true;
    }

    private function unloadMatch(Player $admin)
    {
        if (!$this->matchManager->getMatchDataManager()->isMatchLoaded())
        {
            $this->chat(
                ChatMode::ERROR,
                'No match loaded to unload!',
                $admin->login
            );
            return null;
        }

        $this->matchManager->unloadMatch();
        $this->chat(
            ChatMode::ADMIN_SUCCESS,
            $admin->getEscapedNickname().' unloaded the match!'
        );

        return true;
    }

    public function widgetAdvertisementDisplay($player = null)
    {
        $posX = $this->getWidgetAdvertisementPosX();
        $posY = $this->getWidgetAdvertisementPosY();

        $quadStyle    = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadStyle();
        $quadSubstyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultQuadSubstyle();

        $manialink = new ManiaLink(self::MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT);
        $frame = new Frame();
        $manialink->addChild($frame);
        $frame->setPosition($posX, $posY);

        $borderQuad = new Quad();
        $frame->addChild($borderQuad);
        $borderQuad->setSize(self::MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_WIDTH,
                             self::MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_HEIGHT);
        $borderQuad->setStyles($quadStyle, $quadSubstyle);

        $imageQuad = new Quad();
        $frame->addChild($imageQuad);
        $imageQuad->setImageUrl(self::MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_IMAGE_URL);
        $imageQuad->setSize(self::MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_WIDTH,
                            self::MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_HEIGHT);
        $imageQuad->setUrl(Toornament\Toornament::URL);

        $this->maniaControl->getManialinkManager()->sendManialink($manialink, $player);
    }
    
    private function widgetAdvertisementHide()
    {
        $this->maniaControl->getManialinkManager()->hideManialink(self::MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT);
    }

    private function widgetAdvertisementMove()
    {
        $borderX = 160 - self::MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_WIDTH  / 2;
        $borderY =  90 - self::MLID_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_HEIGHT / 2;
        
        $posX = $this->getWidgetAdvertisementPosX();
        if (abs($posX) > $borderX)
        {
            // sign the border pos
            $borderX *= ($posX <=> 0);

            $this->chat(
                ChatMode::ADMIN_INFORMATION,
                'Advertisement would be off the screen, setting position X to '.$borderX
            );

            $this->maniaControl->getSettingManager()->setSetting(
                $this,
                self::SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_POS_X,
                $borderX
            );
        }

        $posY = $this->getWidgetAdvertisementPosY();
        if (abs($posY) > $borderY)
        {
            // sign the border pos
            $borderY *= ($posY <=> 0);

            $this->chat(
                ChatMode::ADMIN_INFORMATION,
                'Advertisement would be off the screen, setting position Y to '.$borderY
            );

            $this->maniaControl->getSettingManager()->setSetting(
                $this,
                self::SETTING_TOORNAMENTMANIA_WIDGET_ADVERTISEMENT_POS_Y,
                $borderY
            );
        }

        $this->widgetAdvertisementDisplay();
    }

    private function windowKeysDisplay(Player $admin, string $apiKey, string $clientId, string $clientSecret, string $password)
    {
        $width        = 110;
        $height       = 40;
        $quadStyle    = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultMainWindowStyle();
        $quadSubstyle = $this->maniaControl->getManialinkManager()->getStyleManager()->getDefaultMainWindowSubStyle();

        $manialink = new ManiaLink(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS);
        $frame = new Frame();
        $manialink->addChild($frame);
        $frame->setZ(ManialinkManager::MAIN_MANIALINK_Z_VALUE);

        // background
        $backgroundQuad = new Quad();
        $frame->addChild($backgroundQuad);
        $backgroundQuad->setSize($width, $height);
        $backgroundQuad->setStyles($quadStyle, $quadSubstyle);
        $backgroundQuad->setZ(-1);

        $posY = $height/2 - 5;
        // headline
        $headLabel = new Label_Text();
        $frame->addChild($headLabel);
        $headLabel->setPosition(0, $posY);
        $headLabel->setStyle($headLabel::STYLE_TextCardMedium);
        $headLabel->setText(self::PLUGIN_NAME.' v'.self::PLUGIN_VERSION);
        $headLabel->setTextSize(3);

        $entryHeight = 5.;
        $posY -= 1.5*$entryHeight;

        // key inputs
        // api key
        $apiKeyLabel = new Label_Text();
        $frame->addChild($apiKeyLabel);
        $apiKeyLabel->setHorizontalAlign($apiKeyLabel::LEFT);
        $apiKeyLabel->setPosition($width * -0.45, $posY);
        $apiKeyLabel->setSize($width * 0.3, $entryHeight);
        $apiKeyLabel->setStyle($apiKeyLabel::STYLE_TextCardSmall);
        $apiKeyLabel->setText('API Key');

        $apiKeyEntry = new Entry();
        $frame->addChild($apiKeyEntry);
        $apiKeyEntry->setDefault($apiKey);
        $apiKeyEntry->setName(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_APIKEY);
        $apiKeyEntry->setPosition($width * 0.15, $posY);
        $apiKeyEntry->setSize($width * 0.6, $entryHeight * 0.9);
        $apiKeyEntry->setStyle(Label_Text::STYLE_TextValueSmall);
        $apiKeyEntry->setTextSize(1);

        $posY -= $entryHeight;

        // client id
        $clientIdLabel = new Label_Text();
        $frame->addChild($clientIdLabel);
        $clientIdLabel->setHorizontalAlign($clientIdLabel::LEFT);
        $clientIdLabel->setPosition($width * -0.45, $posY);
        $clientIdLabel->setSize($width * 0.3, $entryHeight);
        $clientIdLabel->setStyle($clientIdLabel::STYLE_TextCardSmall);
        $clientIdLabel->setText('Client ID');

        $clientIdEntry = new Entry();
        $frame->addChild($clientIdEntry);
        $clientIdEntry->setDefault($clientId);
        $clientIdEntry->setName(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_CLIENTID);
        $clientIdEntry->setPosition($width * 0.15, $posY);
        $clientIdEntry->setSize($width * 0.6, $entryHeight * 0.9);
        $clientIdEntry->setStyle(Label_Text::STYLE_TextValueSmall);
        $clientIdEntry->setTextSize(1);

        $posY -= $entryHeight;

        // client secret
        $clientSecretLabel = new Label_Text();
        $frame->addChild($clientSecretLabel);
        $clientSecretLabel->setHorizontalAlign($clientSecretLabel::LEFT);
        $clientSecretLabel->setPosition($width * -0.45, $posY);
        $clientSecretLabel->setSize($width * 0.3, $entryHeight);
        $clientSecretLabel->setStyle($clientSecretLabel::STYLE_TextCardSmall);
        $clientSecretLabel->setText('Client Secret');

        $clientSecretEntry = new Entry();
        $frame->addChild($clientSecretEntry);
        $clientSecretEntry->setDefault($clientSecret);
        $clientSecretEntry->setName(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_CLIENTSECRET);
        $clientSecretEntry->setPosition($width * 0.15, $posY);
        $clientSecretEntry->setSize($width * 0.6, $entryHeight * 0.9);
        $clientSecretEntry->setStyle(Label_Text::STYLE_TextValueSmall);
        $clientSecretEntry->setTextSize(1);

        $posY -= $entryHeight;

        // password
        $passwordLabel = new Label_Text();
        $frame->addChild($passwordLabel);
        $passwordLabel->setHorizontalAlign($passwordLabel::LEFT);
        $passwordLabel->setPosition($width * -0.45, $posY);
        $passwordLabel->setSize($width * 0.3, $entryHeight);
        $passwordLabel->setStyle($passwordLabel::STYLE_TextCardSmall);
        $passwordLabel->setText('Password');

        $passwordEntry = new Entry();
        $frame->addChild($passwordEntry);
        $passwordEntry->setDefault($password);
        $passwordEntry->setName(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_PASSWORD);
        $passwordEntry->setPosition($width * 0.15, $posY);
        $passwordEntry->setSize($width * 0.6, $entryHeight * 0.9);
        $passwordEntry->setStyle(Label_Text::STYLE_TextValueSmall);
        $passwordEntry->setTextSize(1);

        $posY -= $entryHeight;

        // close button
        $closeButton = new Label_Text();
        $frame->addChild($closeButton);
        $closeButton->setAction(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_ACTION_CLOSE);
        $closeButton->setPosition($width * -0.2, $height * -0.4);
        $closeButton->setSize($width * 0.3, 7);
        $closeButton->setStyle($closeButton::STYLE_TextButtonNavBack);
        $closeButton->setText('Close');
        $closeButton->setTextColor('999');

        // save button
        $saveButton = new Label_Text();
        $frame->addChild($saveButton);
        $saveButton->setAction(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS_ACTION_SAVE);
        $saveButton->setPosition($width * 0.2, $height * -0.4);
        $saveButton->setSize($width * 0.3, 7);
        $saveButton->setStyle($saveButton::STYLE_TextButtonNavBack);
        $saveButton->setText('Save');
        $saveButton->setTextColor('2af');

        // Display manialink
        $this->maniaControl->getManialinkManager()->sendManialink($manialink, $admin->login);
    }

    private function windowKeysExtractDataFromCallback(array $callback)
    {
        $windowKeysData = array();
        foreach ($callback[1][3] as $manialinkData)
            $windowKeysData[$manialinkData['Name']] = $manialinkData['Value'];

        return $windowKeysData;
    }

    private function windowKeysHide()
    {
        $this->maniaControl->getManialinkManager()->hideManialink(self::MLID_TOORNAMENTMANIA_WINDOW_KEYS);
    }

    private function windowKeysPrepare(Player $admin, $password)
    {
        if (!$this->maniaControl->getAuthenticationManager()->checkPermission($admin, InstallMenu::SETTING_PERMISSION_INSTALL_PLUGINS))
        {
            $this->maniaControl->getAuthenticationManager()->sendNotAllowed($admin);
            return;
        }

        if ($this->matchManager->getAccessManager()->hasPassword())
        {
            if (!is_string($password) || $password == '')
            {
                $this->chat(
                    ChatMode::ERROR,
                    'Missing password!',
                    $admin->login
                );

                return false;
            }

            if ($this->matchManager->getAccessManager()->checkKeyPassword($password) != true)
            {
                $this->chat(
                    ChatMode::ERROR,
                    'Wrong password!',
                    $admin->login
                );

                return false;
            }
        }
        else
            $password = '';

        $keys = $this->matchManager->getAccessManager()->getKeys();

        $apiKey       = $keys->api_key       ?? '';
        $clientId     = $keys->client_id     ?? '';
        $clientSecret = $keys->client_secret ?? '';

        $this->windowKeysDisplay(
            $admin,
            $apiKey,
            $clientId,
            $clientSecret,
            $password
        );
    }

    /**
     * Unload the plugin and its resources
     * @see \ManiaControl\Plugins\Plugin::unload()
     */
    public function unload()
    {
        if ($this->matchManager->isMatchRunning())
        {
            $this->chat(
                ChatMode::INFORMATION,
                'Match paused, continue with //toogame !'
            );
        }
        $this->matchManager->unloadMatch(false);

        $this->widgetAdvertisementHide();

        $this->windowKeysHide();
    }
}
